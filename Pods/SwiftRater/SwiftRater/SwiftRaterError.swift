//
//  SwiftRaterError.swift
//  SwiftRater
//
//  Created by Fujiki Takeshi on 2017/03/29.
//  Copyright © 2022年 com.takecian. All rights reserved.
//

import UIKit

enum SwiftRaterError: Error {
    case malformedURL
    case missingBundleIdOrAppId
}
