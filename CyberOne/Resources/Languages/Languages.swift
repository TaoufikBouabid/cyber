//
//  Language.swift
//  Language
//
//  Created by Karizma Technology on 2/27/17.
//  Copyright © 2022 Karizma Technology. All rights reserved.
//

import Foundation

final class Languages {


    class func currentLanguage() -> String{
        let def = UserDefaults.standard
        
        let langs = def.object(forKey: "AppleLanguages") as! NSArray
        let firstLang = langs.firstObject as! String
        
        return firstLang.components(separatedBy: "-").first ?? firstLang
    }


    class func setAppLanguage(_ lang: String){
        let def = UserDefaults.standard
        def.set([lang, currentLanguage()], forKey: "AppleLanguages")
        def.synchronize()
    }


    class func isRightToLeft() -> Bool {
        return UIView.userInterfaceLayoutDirection(for: UIView.appearance().semanticContentAttribute) == .rightToLeft
    }
}
