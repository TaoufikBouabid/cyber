//
//  Localizer.swift
//  Language
//
//  Created by Karizma Technology on 2/27/17.
//  Copyright © 2022 Karizma Technology. All rights reserved.
//

import UIKit

class Localizer {
    
    class func DoTheExchange(){
        ExchangeMethodsForClass(className: Bundle.self, originalSelector: #selector(Bundle.localizedString(forKey:value:table:)), overrideSelector: #selector(Bundle.customLocalizedStringForKey(key:value:table:)))
        
        ExchangeMethodsForClass(className: UIApplication.self, originalSelector: #selector(getter: UIApplication.userInterfaceLayoutDirection), overrideSelector: #selector(getter: UIApplication.custom_userInterfaceLayoutDirection))
    }
}

extension Bundle {
    @objc func customLocalizedStringForKey(key: String, value: String?, table tableName: String) -> String {
        
        let currentLanguage = Languages.currentLanguage()
        var bundle = Bundle()
        
        if let path = Bundle.main.path(forResource: currentLanguage, ofType: "lproj") {
            bundle = Bundle(path: path)!
        } else{
            let path = Bundle.main.path(forResource: "Base", ofType: "lproj")
            bundle = Bundle(path: path!)!
        }
        
        return bundle.customLocalizedStringForKey(key: key, value: value, table: tableName)
    }
}

extension UIApplication {
    @objc var custom_userInterfaceLayoutDirection: UIUserInterfaceLayoutDirection{
        get {
            var direction = UIUserInterfaceLayoutDirection.leftToRight
            if Languages.currentLanguage().starts(with: "ar") {
                direction = .rightToLeft
            }
            
            return direction
        }
    }
}

func ExchangeMethodsForClass(className: AnyClass, originalSelector: Selector, overrideSelector:  Selector){

    let originalMethod = class_getInstanceMethod(className, originalSelector)

    let overrideMethod = class_getInstanceMethod(className, overrideSelector)

    if class_addMethod(className, originalSelector, method_getImplementation(overrideMethod!), method_getTypeEncoding(overrideMethod!)){
        class_replaceMethod(className, overrideSelector, method_getImplementation(originalMethod!), method_getTypeEncoding(originalMethod!))
    } else{
        method_exchangeImplementations(originalMethod!, overrideMethod!)

    }

//    func ExtchangeMethodsForClass(className:AnyClass,
//                                  originalSelector:Selector,
//                                  overrideSelector:Selector)
//    {
//        let originalMethod = class_getInstanceMethod(className, originalSelector)
//        let overrideMethod = class_getInstanceMethod(className, overrideSelector)
//
//        if class_addMethod(className, originalSelector, method_getImplementation(overrideMethod!), method_getTypeEncoding(overrideMethod!))
//        {
//            class_replaceMethod(className, overrideSelector, method_getImplementation(originalMethod!), method_getTypeEncoding(originalMethod!))
//
//        }else{
//            method_exchangeImplementations(originalMethod!, overrideMethod!)
//        }
}
