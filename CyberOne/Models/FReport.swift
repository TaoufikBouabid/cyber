//
//  FReport.swift
//  CyberOne
//
//  Created by Taoufik on 09/01/2019.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import Foundation
import FirebaseDatabase



final class FReport: FObject {
    static let path = "Reports"

    enum Keys: String {
        case attachments
        case videos
        case statusVoiceRecording
        case status
        case thumbnails
    }

    var fullName: String! = ""
    var address: String! = ""
    var dateOfBirth: String! = ""
    var email: String! = ""
    var phoneNumber: String! = ""
    var notes: String! = ""
    var hasFacebookAccount: Bool = false
    var fakeAccount: Bool = false
    var fakeAccountCreateDate: String = ""
    var usesPersonalPhotos: Bool = false
    var fakeAccountPhoto: String! = ""
    var statusVoiceRecording: String! = ""
    var attachments: String! = ""
    var accountHackedDate: String! = ""
    var someoneKnowsPassword: Bool = false
    var hackedAccountEmail: String! = ""
    var hackedAccountEmailPassword: String! = ""
    var hackedAccountLastPassword: String! = ""
    var phoneType: String! = ""
    var isLegalCopyrightOwner: Bool = false
    var copyrightsViolationLink: String! = ""
    var copyrightsPublishedWithoutPermission: Bool = false
    var someoneUnknownHarassesYou: Bool = false
    var doYouKnowWhoExtortsYou: Bool = false
    var olderThan18: Bool = false
    var isContentReal: Bool = false
    var socialNetwork: String! = ""
    var objectId: String! = ""
    var status: Int = 0
    var userId: String! = ""
    var serviceId: String! = ""
    var videos: String! = ""
    var isClosed: Bool = false
    var remarks: String! = ""
    var creationDate: String! = ""
    var type: String! = ""
    var subscription: String! = ""
    var subscriptionLogo: String! = ""
    var serviceType: Int = 0
    var socialNetworkLogo: String! = ""
    var accountNameOrLink: String! = ""
    var firstHarrassement: Bool = false
    var sexualHarrassement: Bool = false
    var doubtPerson: Bool = false
    var privateOrWork: Bool = false
    var createdAt: Double = 0.0
    var lastMessageDateSent: Date = Date()

    convenience init(
        fullName: String,
        address: String,
        dateOfBirth: String,
        email: String,
        phoneNumber: String,
        notes: String,
        hasFacebookAccount: Bool ,
        fakeAccount: Bool ,
        fakeAccountCreateDate: String = "",
        usesPersonalPhotos: Bool,
        fakeAccountPhoto: String,
        statusVoiceRecording: String,
        attachments: String,
        accountHackedDate: String,
        someoneKnowsPassword: Bool ,
        hackedAccountEmail: String,
        hackedAccountEmailPassword: String,
        hackedAccountLastPassword: String,
        phoneType: String,
        isLegalCopyrightOwner: Bool ,
        copyrightsViolationLink: String,
        copyrightsPublishedWithoutPermission: Bool ,
        someoneUnknownHarassesYou: Bool ,
        doYouKnowWhoExtortsYou: Bool ,
        olderThan18: Bool ,
        isContentReal: Bool ,
        socialNetwork: String,
        objectId: String,
        status: Int,
        userId: String,
        serviceId: String,
        videos: String,
        isClosed: Bool,
        remarks: String,
        creationDate: String,
        type: String,
        subscription: String,
        subscriptionLogo: String,
        serviceType: Int,
        socialNetworkLogo: String,
        accountNameOrLink: String,
        firstHarrassement: Bool,
        sexualHarrassement: Bool,
        doubtPerson: Bool,
        privateOrWork: Bool
    ) {
        let objId = randomString(length: 10)
        self.init(
            path: "Reports",
            subpath: objId,
            dictionary: [
                "fullName" : fullName,
                "address" : address,
                "dateOfBirth" : dateOfBirth,
                "email" : email,
                "phoneNumber" : phoneNumber,
                "notes" : notes,
                "hasFacebookAccount" : hasFacebookAccount ,
                "fakeAccount" : fakeAccount ,
                "fakeAccountCreateDate" : fakeAccountCreateDate,
                "usesPersonalPhotos" :  usesPersonalPhotos,
                "fakeAccountPhoto" :  fakeAccountPhoto,
                "statusVoiceRecording" :  statusVoiceRecording,
                "attachments" :  attachments,
                "accountHackedDate" :  accountHackedDate,
                "someoneKnowsPassword" :  someoneKnowsPassword ,
                "hackedAccountEmail" :  hackedAccountEmail,
                "hackedAccountEmailPassword" :  hackedAccountEmailPassword,
                "hackedAccountLastPassword" :  hackedAccountLastPassword,
                "phoneType" :  phoneType,
                "isLegalCopyrightOwner" :  isLegalCopyrightOwner ,
                "copyrightsViolationLink" :  copyrightsViolationLink,
                "copyrightsPublishedWithoutPermission" :  copyrightsPublishedWithoutPermission,
                "someoneUnknownHarassesYou" :  someoneUnknownHarassesYou ,
                "doYouKnowWhoExtortsYou" :  doYouKnowWhoExtortsYou ,
                "olderThan18" :  olderThan18 ,
                "isContentReal" :  isContentReal ,
                "socialNetwork" :  socialNetwork,
                "objectId" :  objectId,
                "status" :  status,
                "userId" :  userId,
                "id" :  serviceId,
                "videos" :  videos,
                "isClosed" :  isClosed,
                "remarks" : remarks,
                "creationDate" : creationDate,
                "type" : type,
                "subscription" : subscription,
                "subscriptionLogo": subscriptionLogo,
                "serviceType" : serviceType,
                "socialNetworkLogo" : socialNetworkLogo,
                "accountNameOrLink" : accountNameOrLink,
                "firstHarrassement": firstHarrassement,
                "sexualHarrassement": sexualHarrassement,
                "doubtPerson": doubtPerson,
                "privateOrWork": privateOrWork
            ]
        )
        self.fullName = fullName
        self.address = address
        self.dateOfBirth = dateOfBirth
        self.email = email
        self.phoneNumber = phoneNumber
        self.notes = notes
        self.hasFacebookAccount = hasFacebookAccount
        self.fakeAccount = fakeAccount
        self.fakeAccountCreateDate = fakeAccountCreateDate
        self.usesPersonalPhotos =  usesPersonalPhotos
        self.fakeAccountPhoto =  fakeAccountPhoto
        self.statusVoiceRecording =  statusVoiceRecording
        self.attachments =  attachments
        self.accountHackedDate =  accountHackedDate
        self.someoneKnowsPassword =  someoneKnowsPassword
        self.hackedAccountEmail =  hackedAccountEmail
        self.hackedAccountEmailPassword =  hackedAccountEmailPassword
        self.hackedAccountLastPassword =  hackedAccountLastPassword
        self.phoneType =  phoneType
        self.isLegalCopyrightOwner =  isLegalCopyrightOwner
        self.copyrightsViolationLink =  copyrightsViolationLink
        self.copyrightsPublishedWithoutPermission =  copyrightsPublishedWithoutPermission
        self.someoneUnknownHarassesYou =  someoneUnknownHarassesYou
        self.doYouKnowWhoExtortsYou =  doYouKnowWhoExtortsYou
        self.olderThan18 =  olderThan18
        self.isContentReal =  isContentReal
        self.socialNetwork =  socialNetwork
        self.objectId =  objectId
        self.status =  status
        self.userId =  userId
        self.serviceId =  serviceId
        self.videos =  videos
        self.isClosed = isClosed
        self.remarks = remarks
        self.creationDate = creationDate
        self.type = type
        self.subscription = subscription
        self.subscriptionLogo = subscriptionLogo
        self.serviceType = serviceType
        self.socialNetworkLogo = socialNetworkLogo
        self.accountNameOrLink = accountNameOrLink
        self.firstHarrassement = firstHarrassement
        self.sexualHarrassement = sexualHarrassement
        self.doubtPerson = doubtPerson
        self.privateOrWork = privateOrWork
    }


    convenience init(value: [String: Any]) {
        let fullName = value["fullName"] as? String ?? ""
        let address = value["address"] as? String ?? ""
        let dateOfBirth = value["dateOfBirth"] as? String ?? ""
        let email = value["email"] as? String ?? ""
        let phoneNumber = value["phoneNumber"] as? String ?? ""
        let notes = value["notes"] as? String ?? ""
        let hasFacebookAccount = value["hasFacebookAccount"] as? Bool ?? false
        let fakeAccount = value["fakeAccount"] as? Bool ?? false
        let fakeAccountCreateDate = value["fakeAccountCreateDate"] as? String ?? ""
        let usesPersonalPhotos =  value["usesPersonalPhotos"] as? Bool ?? false
        let fakeAccountPhoto =  value["fakeAccountPhoto"] as? String ?? ""
        let statusVoiceRecording =  value["statusVoiceRecording"] as? String ?? ""
        let attachments =  value["attachments"] as? String ?? ""
        let accountHackedDate =  value["accountHackedDate"] as? String ?? ""
        let someoneKnowsPassword =  value["someoneKnowsPassword"] as? Bool ?? false
        let hackedAccountEmail =  value["hackedAccountEmail"] as? String ?? ""
        let hackedAccountEmailPassword =  value["hackedAccountEmailPassword"] as? String ?? ""
        let hackedAccountLastPassword =  value["hackedAccountLastPassword"] as? String ?? ""
        let phoneType =  value["phoneType"] as? String ?? ""
        let isLegalCopyrightOwner =  value["isLegalCopyrightOwner"] as? Bool ?? false
        let copyrightsViolationLink =  value["copyrightsViolationLink"] as? String ?? ""
        let copyrightsPublishedWithoutPermission =  value["copyrightsPublishedWithoutPermission"] as? Bool ?? false
        let someoneUnknownHarassesYou =  value["someoneUnknownHarassesYou"] as? Bool ?? false
        let doYouKnowWhoExtortsYou =  value["doYouKnowWhoExtortsYou"] as? Bool ?? false
        let olderThan18 =  value["olderThan18"] as? Bool ?? false
        let isContentReal =  value["isContentReal"] as? Bool ?? false
        let socialNetwork =  value["socialNetwork"] as? String ?? ""
        let objectId =  value["objectId"] as? String ?? ""
        let status =  value["status"] as? Int ?? 0
        let userId =  value["userId"] as? String ?? ""
        let serviceId =  value["id"] as? String ?? ""
        let videos =  value["videos"] as? String ?? ""
        let isClosed =  value["isClosed"] as? Bool ?? false
        let remarks = value["remarks"] as? String ?? ""
        let creationDate = value["creationDate"] as? String ?? ""
        let type = value["type"] as? String ?? ""
        let subscription = value["subscription"] as? String ?? ""
        let subscriptionLogo = value["subscriptionLogo"] as? String ?? ""
        let serviceType = value["serviceType"] as? Int ?? 0
        let socialNetworkLogo = value["socialNetworkLogo"] as? String ?? ""
        let accountNameOrLink = value["accountNameOrLink"] as? String ?? ""

        let firstHarrassement  =  value["firstHarrassement "] as? Bool ?? false
        let sexualHarrassement =  value["sexualHarrassement"] as? Bool ?? false
        let doubtPerson =  value["doubtPerson"] as? Bool ?? false
        let privateOrWork =  value["privateOrWork"] as? Bool ?? false
        let createdAt = value["createdAt"] as? Double ?? 0.0

        self.init(
            path: "Reports",
            subpath: objectId ,
            dictionary: [
                "fullName" : fullName,
                "address" : address,
                "dateOfBirth" : dateOfBirth,
                "email" : email,
                "phoneNumber" : phoneNumber,
                "notes" : notes,
                "hasFacebookAccount" : hasFacebookAccount ,
                "fakeAccount" : fakeAccount ,
                "fakeAccountCreateDate" : fakeAccountCreateDate,
                "usesPersonalPhotos" :  usesPersonalPhotos,
                "fakeAccountPhoto" :  fakeAccountPhoto,
                "statusVoiceRecording" :  statusVoiceRecording,
                "attachments" :  attachments,
                "accountHackedDate" :  accountHackedDate,
                "someoneKnowsPassword" :  someoneKnowsPassword ,
                "hackedAccountEmail" :  hackedAccountEmail,
                "hackedAccountEmailPassword" :  hackedAccountEmailPassword,
                "hackedAccountLastPassword" :  hackedAccountLastPassword,
                "phoneType" :  phoneType,
                "isLegalCopyrightOwner" :  isLegalCopyrightOwner ,
                "copyrightsViolationLink" :  copyrightsViolationLink,
                "copyrightsPublishedWithoutPermission" :  copyrightsPublishedWithoutPermission,
                "someoneUnknownHarassesYou" :  someoneUnknownHarassesYou ,
                "doYouKnowWhoExtortsYou" :  doYouKnowWhoExtortsYou ,
                "olderThan18" :  olderThan18 ,
                "isContentReal" :  isContentReal ,
                "socialNetwork" :  socialNetwork,
                "objectId" :  objectId,
                "status" :  status,
                "userId" :  userId,
                "id" :  serviceId,
                "videos" :  videos,
                "isClosed" : isClosed,
                "remarks" : remarks,
                "creationDate" : creationDate,
                "type" : type,
                "subscription" : subscription,
                "subscriptionLogo": subscriptionLogo,
                "serviceType" : serviceType,
                "socialNetworkLogo" : socialNetworkLogo,
                "accountNameOrLink" : accountNameOrLink,
                "firstHarrassement" : firstHarrassement,
                "sexualHarrassement" : sexualHarrassement,
                "doubtPerson" : doubtPerson,
                "privateOrWork" : privateOrWork,
                "createdAt": createdAt
            ]
        )
        self.fullName = fullName
        self.address = address
        self.dateOfBirth = dateOfBirth
        self.email = email
        self.phoneNumber = phoneNumber
        self.notes = notes
        self.hasFacebookAccount = hasFacebookAccount
        self.fakeAccount = fakeAccount
        self.fakeAccountCreateDate = fakeAccountCreateDate
        self.usesPersonalPhotos =  usesPersonalPhotos
        self.fakeAccountPhoto =  fakeAccountPhoto
        self.statusVoiceRecording =  statusVoiceRecording
        self.attachments =  attachments
        self.accountHackedDate =  accountHackedDate
        self.someoneKnowsPassword =  someoneKnowsPassword
        self.hackedAccountEmail =  hackedAccountEmail
        self.hackedAccountEmailPassword =  hackedAccountEmailPassword
        self.hackedAccountLastPassword =  hackedAccountLastPassword
        self.phoneType =  phoneType
        self.isLegalCopyrightOwner =  isLegalCopyrightOwner
        self.copyrightsViolationLink =  copyrightsViolationLink
        self.copyrightsPublishedWithoutPermission =  copyrightsPublishedWithoutPermission
        self.someoneUnknownHarassesYou =  someoneUnknownHarassesYou
        self.doYouKnowWhoExtortsYou =  doYouKnowWhoExtortsYou
        self.olderThan18 =  olderThan18
        self.isContentReal =  isContentReal
        self.socialNetwork =  socialNetwork
        self.objectId =  objectId
        self.status =  status
        self.userId =  userId
        self.serviceId =  serviceId
        self.videos =  videos
        self.isClosed = isClosed
        self.remarks = remarks
        self.creationDate = creationDate
        self.type = type
        self.subscription = subscription
        self.subscriptionLogo = subscriptionLogo
        self.serviceType = serviceType
        self.socialNetworkLogo = socialNetworkLogo
        self.accountNameOrLink = accountNameOrLink
        self.firstHarrassement = firstHarrassement
        self.sexualHarrassement = sexualHarrassement
        self.doubtPerson = doubtPerson
        self.privateOrWork = privateOrWork
        self.createdAt = createdAt
    }
}



func randomString(length: Int) -> String {
    let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    let len = UInt32(letters.length)
    var randomString = ""
    for _ in 0 ..< length {
        let rand = arc4random_uniform(len)
        var nextChar = letters.character(at: Int(rand))
        randomString += NSString(characters: &nextChar, length: 1) as String
    }
    return randomString
}
