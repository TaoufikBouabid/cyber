//
//  Tutorial.swift
//  CyberOne
//
//  Created by Taoufik on 17/12/2018.
//  Copyright © 2018 Cyber One. All rights reserved.
//

import Foundation



final class Tutorial: NSObject {
    static let path = "Tutorial"

    var uniqueId: String!
    var desc: String!
    var image: String!
    var order: Int!


    init(dict: [String: Any]) {

        let languages = UserDefaults.standard.dictionary(forKey: "Languages")

        if let uniqueId = dict["uniqueId"] as? String {
            self.uniqueId = uniqueId
        }

        if let tutoText = dict["desc"] as? String {
            if let languages = languages, let langTitle = languages[tutoText] as? [String: Any], let translatedTitle = langTitle[Languages.currentLanguage()] as? String {
                self.desc = translatedTitle
            } else {
                self.desc = tutoText
            }
        }

        if let tutoImage = dict["image"] as? String {
            self.image = tutoImage
        }

        if let tutoOrder = dict["order"] as? Int {
            self.order = tutoOrder
        }
    }

}
