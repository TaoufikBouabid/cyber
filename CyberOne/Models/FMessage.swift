//
//  Messages.swift
//  Tawsil
//
//  Created by Taoufik on 09/09/2022.
//  Copyright © 2022 CyberOne All rights reserved.
//

import Foundation



final class FMessage: NSObject {
    static let path = "Messages"

    enum keys: String {
        case dateSent
        case message
        case senderId
        case type
        case url
        case audio
        case audioDuration
    }

    var objectId: String = ""
    var dateSent: String = ""
    var message: String = ""
    var senderId: String = ""
    var type: String = ""
    var url: String = ""
    var audio: String = ""
    var audioDuration: String = ""
    
    override var description: String { return "<Message; objectId: \(objectId); dateSent: \(dateSent); message; \(message);senderId: \(senderId);type: \(type)>" }
    
    func initWithDictionary(msgDict: [String:Any]) -> FMessage {
        let msg = FMessage()
        
        if (msgDict["dateSent"] != nil) {
            msg.dateSent = msgDict["dateSent"] as! String
        }
        if (msgDict["message"] != nil) {
            msg.message = msgDict["message"] as! String
        }
        if (msgDict["senderId"] != nil) {
            msg.senderId = msgDict["senderId"] as! String
        }
        if (msgDict["type"] != nil) {
            msg.type = msgDict["type"] as! String
        }
        if (msgDict["url"] != nil) {
            msg.url = msgDict["url"] as! String
        }
        if (msgDict["audio"] != nil) {
            msg.audio = msgDict["audio"] as! String
        }
        if (msgDict["audio_duration"] != nil) {
            msg.audioDuration = "\(msgDict["audio_duration"]!)"
        }
        return msg
    }
}

