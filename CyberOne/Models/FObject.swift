//
//  Service.swift
//  CyberOne
//
//  Created by Taoufik on 06/01/2019.
//  Copyright © 2022 Cyber One. All rights reserved.
//


import Foundation
import FirebaseDatabase
import Reachability



class FObject: NSObject {

    struct Const {
        static let objectId = "objectId"
        static let createdAt = "createdAt"
        static let updatedAt = "updatedAt"
    }

	private var pathX: String!
	var subpathX: String?
	var values: [String: Any] = [:]


	init(path: String, subpath: String?) {
		super.init()
        self.pathX = path
        self.subpathX = subpath
	}


    convenience init(path: String) {
		self.init(path: path, subpath: nil)
	}


    convenience init(path: String, dictionary: [String: Any]) {
		self.init(path: path, subpath: nil, dictionary: dictionary)
	}


    convenience init(path: String, subpath: String?, dictionary: [String: Any]) {
		self.init(path: path, subpath: subpath)
		for (key, value) in dictionary {
			values[key] = value
		}
	}


	// MARK: - Accessors
	subscript(key: String) -> Any? {
		get { return values[key] }
		set { values[key] = newValue }
	}


    func objectId() -> String? {
        return values[Const.objectId] as? String
	}


	// MARK: - Save methods

    func saveInBackground() {
        self.saveInBackground(block: { error in })
	}


    func saveInBackground(block: @escaping (_ error: Error?) -> Void) {
		let reference = databaseReference()
        if values[Const.objectId] == nil {
			values[Const.objectId] = reference.key
		}

        if values[Const.createdAt] == nil {
            values[Const.createdAt] = Date().timeIntervalSince1970
		}

		values[Const.updatedAt] = Date().timeIntervalSince1970

        reference.updateChildValues(values, withCompletionBlock: { error, ref in
            block(error)
        })
	}


	// MARK: - Update methods

    func updateInBackground() {
        self.updateInBackground(block: { error in })
	}


    func updateInBackground(block: @escaping (_ error: Error?) -> Void) {
		var reference = databaseReference()
        if let objectId = values[Const.objectId] as? String {
            reference = Database.database()
                .reference(withPath: pathX)
                .child(objectId)
            values[Const.updatedAt] = Date().timeIntervalSince1970
            reference.updateChildValues(
                values,
                withCompletionBlock: { error, ref in
                block(error)
            })
		} else {
			block(NSError.description("Object cannot be updated.", code: 101))
		}
	}


	// MARK: - Delete methods

	func deleteInBackground() {
        self.deleteInBackground(block: { error in })
	}


    func deleteInBackground(block: @escaping (_ error: Error?) -> Void) {
		let reference = databaseReference()
        if values[Const.objectId] != nil {
            reference.removeValue(completionBlock: { error, ref in
                block(error)
            })
		} else {
			block(NSError.description("Object cannot be deleted.", code: 102))
		}
	}


	// MARK: - Fetch methods

	func fetchInBackground() {
        self.fetchInBackground(block: { error in })
	}


    func fetchInBackground(block: @escaping (_ error: Error?) -> Void) {
        do {
            if try Reachability().connection == .unavailable {
                block(NSError.description("no internet connection", code: 999))
            }
        } catch {
        }
		let reference = databaseReference()
		reference.observeSingleEvent(of: .value, with: { snapshot in
			if snapshot.exists(), let values = snapshot.value as? [String: Any] {
				self.values = values
                block(nil)
			} else {
                var desc = "Object \(self.pathX ?? "") not found."
                if let subpathX = self.subpathX {
                    desc = "Object \(self.pathX ?? "").\(subpathX) not found."
                }
                block(NSError.description(desc,code: 103))
			}
		})
	}


	// MARK: - Private methods

	func databaseReference() -> DatabaseReference {
        if let subPath = self.subpathX {
            if let objectId = values[Const.objectId] as? String  {
                return Database.database()
                    .reference(withPath: self.pathX)
                    .child(objectId)
            } else {
                return Database.database()
                    .reference(withPath: self.pathX)
                    .child(subPath)
            }
        }
        return Database.database().reference(withPath: self.pathX)
	}
}
