//
//  Chat.swift
//  Tawsil
//
//  Created by Taoufik on 09/09/2022.
//  Copyright © 2022 CyberOne All rights reserved.
//

import Foundation


final class FChat: NSObject {
    static let path = "Chats"

    enum keys: String {
        case objectId
        case createdAt
        case lastMessage
        case lastMessageDateSent
        case lastMessageUserId
        case typingIndicator
        case unreadMessages
        case opponentKey
        case isClosed
        case values
        case serviceId
    }

    var objectId: String = ""
    var createdAt: Double = 0.0
    var lastMessage: String = ""
    var lastMessageDateSent: Date = Date()
    var lastMessageUserId: String = ""
    var typingIndicator = [String: Any]()
    var unreadMessages = [String: Any]()
    var opponentKey: String = ""
    var isClosed = false
    var values = [String: Any]()
    var serviceId: Int = 0

    var report: FReport!
    var serviceImage: String!


    init(chatDict: [String: Any]) {

        if let objId = chatDict[FChat.keys.objectId.rawValue] as? String {
            self.objectId = objId
        }

        if let createdAt = chatDict[FChat.keys.createdAt.rawValue] as? Double {
            self.createdAt = createdAt
        }

        if let lastMessage = chatDict[FChat.keys.lastMessage.rawValue] as? String {
            self.lastMessage = lastMessage
        }

        if let lastMessageDateSent = chatDict[FChat.keys.lastMessageDateSent.rawValue] as? String {
            let dateFormatter = DateFormatterProvider.dateFormatterFromGMT
            if let date = dateFormatter.date(from: lastMessageDateSent) {
                self.lastMessageDateSent = date
            }
            else {
                self.lastMessageDateSent = Date()
            }
        }

        if let lastMessageUserId = chatDict[FChat.keys.lastMessageUserId.rawValue] as? String {
            self.lastMessageUserId = lastMessageUserId
        }

        if let typingIndicator = chatDict[FChat.keys.typingIndicator.rawValue] as? [String : Any] {
            self.typingIndicator = typingIndicator
        }

        if let unreadMessages = chatDict[FChat.keys.unreadMessages.rawValue] as? [String : Any] {
            self.unreadMessages = unreadMessages
        }

        if let isClosed = chatDict[FChat.keys.isClosed.rawValue] as? Bool {
            self.isClosed = isClosed
        }
    }


    func update(from chatDict: [String: Any]) {
        
        if let objId = chatDict[FChat.keys.objectId.rawValue] as? String {
            self.objectId = objId
        }

        if let createdAt = chatDict[FChat.keys.createdAt.rawValue] as? Double {
            self.createdAt = createdAt
        }

        if let lastMessage = chatDict[FChat.keys.lastMessage.rawValue] as? String {
            self.lastMessage = lastMessage
        }

        if let lastMessageDateSent = chatDict[FChat.keys.lastMessageDateSent.rawValue] as? String {
            let dateFormatter = DateFormatterProvider.dateFormatterFromGMT
            if let date = dateFormatter.date(from: lastMessageDateSent) {
                self.lastMessageDateSent = date
            }
            else {
                self.lastMessageDateSent = Date()
            }
        }

        if let lastMessageUserId = chatDict[FChat.keys.lastMessageUserId.rawValue] as? String {
            self.lastMessageUserId = lastMessageUserId
        }

        if let typingIndicator = chatDict[FChat.keys.typingIndicator.rawValue] as? [String : Any] {
            self.typingIndicator = typingIndicator
        }

        if let unreadMessages = chatDict[FChat.keys.unreadMessages.rawValue] as? [String : Any] {
            self.unreadMessages = unreadMessages
        }

        if let isClosed = chatDict[FChat.keys.isClosed.rawValue] as? Bool {
            self.isClosed = isClosed
        }
    }


    subscript(key: String) -> Any {
        return self.values[key]!
    }

}
