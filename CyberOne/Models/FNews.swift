//
//  FNews.swift
//  CyberOne
//
//  Created by Taoufik on 9/28/19.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import Foundation

class FNews: NSObject {
    
    var newsID: String!
    var categoryID: Int!
    var content: String!
    var date: String!
    var image: String!
    var likes: Int!
    var title: String!
    var views: Int!
    var direction: String!

    init(dict: [String:Any]) {
        
        if let newsID = dict["objectId"] as? String {
            self.newsID = newsID
        }

        if let categoryID = dict["categoryId"] as? Int {
            
            self.categoryID = categoryID
        }
        
        if let content = dict["content"] as? String {
            
            self.content = content
        }
        
        if let date = dict["date"] as? String {
            
            self.date = date
        }
        
        if let image = dict["image"] as? String {
            
            self.image = image
        }
        
        if let likes = dict["likes"] as? Int {
            
            self.likes = likes
        }
        
        if let title = dict["title"] as? String {
            
            self.title = title
        }
        
        if let views = dict["views"] as? Int {
            
            self.views = views
        }
        
        if let direction = dict["direction"] as? String {
            
            self.direction = direction
        }
    }
}
