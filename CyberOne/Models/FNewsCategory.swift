//
//  FNewsCategory.swift
//  CyberOne
//
//  Created by Taoufik on 11/30/19.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import Foundation

class FNewsCategory: NSObject {
    
    var objectId: String!
    var title: String!
    var id: Int!
    var order: Int!
    
    init(dict: [String: Any]) {
        
        if let objectId = dict["objectId"] as? String {
            self.objectId = objectId
        }
        
        if let id = dict["id"] as? Int {
            
            self.id = id
        }
        
        if let order = dict["order"] as? Int {
            
            self.order = order
        }
        
        let languages = UserDefaults.standard.dictionary(forKey: "Languages")

        if let title = dict["title"] as? String {
            if let languages = languages, let langTitle = languages[title] as? [String: Any], let translatedTitle = langTitle[Languages.currentLanguage()] as? String {
                self.title = translatedTitle
            } else {
                self.title = title
            }
        }
        
    }
}
