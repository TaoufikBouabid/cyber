//
//  FComments.swift
//  CyberOne
//
//  Created by Taoufik on 9/28/19.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import Foundation
import FirebaseDatabase

class FComments: FObject {

    var commentID: String!
    var newsID: String!
    var replyCommentID: String!
    var dateTime: String!
    var likes: Int!
    var text: String!
    var timeZone: String!
    var userId: String!
    var username: String!
    var userImage: String!
    var user: FUsers?

    
    convenience init(newsID: String,
                     replyCommentID: String?,
                     dateTime: String,
                     text: String,
                     likes: Int = 0,
                     userId: String,
                     username: String,
                     userImage: String) {
        let objectIdKey = "objectId"
        let newsIdKey = "newsId"
        let replyCommentIdKey = "replyCommentId"
        let dateTimeKey = "dateTime"
        let textKey = "text"
        let timeZoneKey = "timeZone"
        let likesKey = "likes"
        let userIdKey = "userId"
        let usernameKey = "username"
        let userImageKey = "userImage"
        let objId = randomString(length: 10)
        self.init(path: "Comments", subpath: objId, dictionary: [newsIdKey : newsID , replyCommentIdKey: replyCommentID, dateTimeKey: dateTime, textKey: text, likesKey: likes, userIdKey: userId, usernameKey: username, userImageKey: userImage])
        self.newsID = newsID
        self.replyCommentID = replyCommentID
        self.dateTime = dateTime
        self.text = text
        self.likes = likes
        self.userId = userId
        self.username = username
        self.userImage = userImage
    }


    convenience init(value: [String:Any]) {
        let objectIdKey = "objectId"
        let newsIdKey = "newsId"
        let replyCommentIdKey = "replyCommentId"
        let dateTimeKey = "dateTime"
        let textKey = "text"
        let timeZoneKey = "timeZone"
        let likesKey = "likes"
        let userIdKey = "userId"
        let usernameKey = "username"
        let userImageKey = "userImage"

        let objectId = value[objectIdKey] as? String ?? ""
        let replyCommentId = value[replyCommentIdKey] as? String
        let dateTime = value[dateTimeKey] as? String ?? ""
        let text = value[textKey] as? String ?? ""
        let likes = value[likesKey] as? Int ?? 0
        let userId = value[userIdKey] as? String ?? ""
        let username = value[usernameKey] as? String ?? ""
        let userImage = value[userImageKey] as? String ?? ""
        self.init(path: "Comments",
                  subpath: objectId ,
                  dictionary: [newsIdKey : objectId , replyCommentIdKey: replyCommentId, dateTimeKey: dateTime, textKey: text, likesKey: likes, userIdKey: userId, usernameKey: username, userImageKey: userImage])
        self.commentID = objectId
        self.replyCommentID = replyCommentId
        self.dateTime = dateTime
        self.text = text
        self.likes = likes
        self.userId = userId
        self.username = username
        self.userImage = userImage
    }

}
