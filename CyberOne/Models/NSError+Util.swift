//
//  Service.swift
//  CyberOne
//
//  Created by Taoufik on 06/01/2019.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import Foundation
import JustLog



extension Error {

    static func description(_ description: String, code: Int) -> Error? {
        guard let domain = Bundle.main.bundleIdentifier else { return nil }
		let userInfo = [NSLocalizedDescriptionKey: description]
        Logger.shared.error("[Error] description, \(description)")
		return NSError(domain: domain, code: code, userInfo: userInfo)
	}
}
