//
//  FService+Type.swift
//  CyberOne
//
//  Created by Taoufik on 13/11/2022.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import Foundation


enum ServiceType: Int {
    case impersonation = 1
    case extortion
    case copyrights
    case harassement
    case hackedAccount
    case other


    static func type(from rawValue: Int) -> String {
        switch rawValue {
        case 1: return localizedString(with: "impersonate me")
        case 2: return localizedString(with: "I get blackmailed")
        case 3: return localizedString(with: "Copyright and privacy rights")
        case 4: return localizedString(with: "I get harassed")
        case 5: return localizedString(with: "My account is hacked")
        case 6: return localizedString(with: "Other problem")
        default: return ""
        }
    }
}


func localizedString(with key: String) -> String {
    let languages = UserDefaults.standard.dictionary(forKey: "Languages")
    if let languages = languages,
        let langDesc = languages[key] as? [String: Any],
        let localizedString = langDesc[Languages.currentLanguage()] as? String {
        return localizedString
    }
    return key
}
