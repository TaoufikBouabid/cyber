//
//  FUsers.swift
//  CyberOne
//
//  Created by Taoufik on 9/28/19.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import Foundation



final class FUsers: NSObject {

    enum keys: String {
        case objectId
        case avatar
        case email
        case name
        case phoneNumber
        case fcmToken
        case country
        case type
        case age
        case createdAt
        case updatedAt
        case password
    }

    var userID: String!
    var avatar: String!
    var email: String!
    var name: String!
    var phoneNumber: String!
    var country: String!
    var type: Int!
    var age: Int!


    init(dict: [String: Any]) {
        
        if let userID = dict["objectId"] as? String {
            self.userID = userID
        }
        
        if let avatar = dict["avatar"] as? String {
            self.avatar = avatar
        }
        
        if let email = dict["email"] as? String {
            self.email = email
        }
        
        if let name = dict["name"] as? String {
            self.name = name
        }
        
        if let phoneNumber = dict["phoneNumber"] as? String {
            self.phoneNumber = phoneNumber
        }

        if let country = dict["country"] as? String {
            self.country = country
        }

        if let type = dict["type"] as? Int {
            self.type = type
        }

        if let age = dict["age"] as? Int {
            self.age = age
        }
    }
}
