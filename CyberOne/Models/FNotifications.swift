//
//  FNotifications.swift
//  CyberOne
//
//  Created by Taoufik on 12/3/19.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import Foundation



final class FNotifications: NSObject {

    var data: [String:Any]!
    var body: String!
    var reportId: String!
    var title: String!
    var date: Int!
    var objectId: String!
    var status: Int!
    var type: Int!
    var userId: String!


    init(dict: [String: Any]) {

        if let data = dict["notification_data"] as? [String: Any] {
            self.data = data
        }
        
        if let body = data["body"] as? String {
            self.body = body
        }

        if let title = data["title"] as? String {
            self.title = title
        }

        if let reportId = data["report_id"] as? String {
            self.reportId  = reportId
        }
        
        if let date = dict["notification_date"] as? Int {
            self.date = date
        }
        
        if let id = dict["notification_id"] as? String {
            self.objectId = id
        }
        
        // 0 Not read, 1 read
        if let status = dict["notification_status"] as? Int {
            self.status = status
        }
        
        // 0 report
        if let type = dict["notification_type"] as? Int {
            self.type = type
        }
        
        if let userId = dict["user_id"] as? String {
            self.userId = userId
        }
    }

}
