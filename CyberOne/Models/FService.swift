//
//  Service.swift
//  CyberOne
//
//  Created by Taoufik on 06/01/2019.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import Foundation



final class FService: NSObject {
    
    var objectId: String!
    var background: String!
    var icon: String!
    var title: String!
    var desc: String!
    var order: Int!
    var id: Int!


    init(dict: [String: Any]) {
        
        if let objectId = dict["objectId"] as? String {
            self.objectId = objectId
        }

        if let serviceId = dict["id"] as? Int {
            self.id = serviceId
        }

        let languages = UserDefaults.standard.dictionary(forKey: "Languages")

        if let background = dict["background"] as? String {
            self.background = background
        }
        
        if let icon = dict["icon"] as? String {
            self.icon = icon
        }
        
        if let title = dict["title"] as? String {
            if let languages = languages, let langTitle = languages[title] as? [String: Any], let translatedTitle = langTitle[Languages.currentLanguage()] as? String {
                self.title = translatedTitle
            } else {
                self.title = title
            }
        }
        
        if let description = dict["desc"] as? String {
            if let languages = languages, let langDesc = languages[description] as? [String: Any], let translatedDesc = langDesc[Languages.currentLanguage()] as? String {
                self.desc = translatedDesc
            } else {
                self.desc = description
            }
        }
        
        if let order = dict["order"] as? Int {
            self.order = order
        }
    }
}
