//
//  Country.swift
//
//  Created by Taoufik on 12/11/2022
//  Copyright (c) . All rights reserved.
//

import Foundation



struct Country: Codable {

    enum CodingKeys: String, CodingKey {
        case nameAr = "name_ar"
        case code
        case nameEn = "name_en"
        case dialCode = "dial_code"
    }

    var nameAr: String?
    var code: String?
    var nameEn: String?
    var dialCode: String?


    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.nameAr = try container.decodeIfPresent(String.self, forKey: .nameAr)
        self.code = try container.decodeIfPresent(String.self, forKey: .code)
        self.nameEn = try container.decodeIfPresent(String.self, forKey: .nameEn)
        self.dialCode = try container.decodeIfPresent(String.self, forKey: .dialCode)
    }

}
