//
//  FUser+Type.swift
//  CyberOne
//
//  Created by Taoufik on 12/11/2022.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import Foundation


/** User type, 0 is normal user, 1 is admin */
enum FUserType: Int {
    case normal = 0
    case admin
}
