//
//  WPUser.swift
//  CyberOne
//
//  Created by Taoufik on 10/11/2022.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import Foundation



final class WPUser : WPAPIUser {}


class BLPost : Post {
    
    let betterAuthor : WPUser?
    let betterFeaturedImage : Media?
    
    private enum CodingKeys: String, CodingKey {
        
        case better_author = "better_author"
        case better_featured_image = "better_featured_image"
        
    }
    
    required init(from decoder: Decoder) throws {
        
        // Get our container for this subclass' coding keys
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        betterAuthor = try values.decodeIfPresent(WPUser.self, forKey: .better_author)
        betterFeaturedImage = try values.decodeIfPresent(Media.self, forKey: .better_featured_image)
//        betterAuthor = nil
//        betterFeaturedImage = nil
        // Get superDecoder for superclass and call super.init(from:) with it
        //        let superDecoder = try values.superDecoder() // superDecoder is not working, see https://stackoverflow.com/questions/44553934/using-decodable-in-swift-4-with-inheritance#_=_ && https://stackoverflow.com/questions/47518209/decoder-containerkeyedby-throws-decodingerror-typemismatch-error-codable-bug?rq=1
        try super.init(from: decoder)
    }
    
    override init(title: String?, content: String?, featuredMedia: Int?, categories: [Int]?) {
        betterAuthor = nil
        betterFeaturedImage = nil
        super.init(title: title, content: content, featuredMedia: featuredMedia, categories: categories)
    }
    
    override func encode(to encoder: Encoder) throws {
        
//        var container = encoder.container(keyedBy: CodingKeys.self)
        
        // Neither of betterAuthor && betterFeaturedImage should be encoded
        
        // Get superEncoder for superclass and call super.encode(to:) with it
        //        let superEncoder = container.superEncoder() // Not using super encoder, as this will encode parent properties into a 'super' property, which does not work with WordPress.
        try super.encode(to: encoder)
    }
    
}
