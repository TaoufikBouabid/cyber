//
//  Service.swift
//  CyberOne
//
//  Created by Taoufik on 06/01/2019.
//  Copyright © 2022 Cyber One. All rights reserved.
//


import FirebaseAuth
import JustLog



final class FUser: FObject {

    static let path = "User"


	// MARK: - Class methods
    static var currentUser: FUser? = {
		if let dictionary = UserDefaults.standard.object(forKey: "currentUser") as? [String: Any] {
            Logger.shared.debug("[FObject] currentUser : \(dictionary)")
			return FUser(path: "User", dictionary: dictionary)
		}
		return nil
	}()


    class func signIn(
        email: String,
        password: String,
        completion: @escaping (_ user: FUser?, _ error: Error?) -> Void
    ) {
		Auth.auth().signIn(withEmail: email, password: password) { authResult, error in
			if error == nil {
				let firuser = authResult!.user
                FUser.load(firuser: firuser) { user, error in
					if error == nil {
                        completion(user, nil)
					} else {
						try? Auth.auth().signOut()
                        completion(nil, error)
					}
				}
			} else {
				completion(nil, error)
			}
		}
	}


    class func createUser(
        email: String,
        password: String,
        completion: @escaping (_ user: FUser?, _ error: Error?) -> Void
    ) {
		Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
			if error == nil {
				let firuser = authResult!.user
				FUser.create(uid: firuser.uid, email: email) { user, error in
					if (error == nil) {
                        completion(user, nil)
					} else {
						firuser.delete(completion: { error in
							if (error != nil) {
								try? Auth.auth().signOut()
							}
						})
						completion(nil, error)
					}
				}
			} else {
				completion(nil, error)
			}
		}
	}


    class func signIn(
        credential: AuthCredential,
        completion: @escaping (_ user: FUser?, _ error: Error?) -> Void
    ) {
		Auth.auth().signIn(with: credential) { authResult, error in
			if error == nil {
				let firuser = authResult!.user
				FUser.load(firuser: firuser) { user, error in
					if error == nil {
                        completion(user, nil)
					} else {
						try? Auth.auth().signOut()
                        completion(nil, error)
					}
				}
			} else {
				completion(nil, error)
			}
		}
	}


	// MARK: - Logut methods

    class func logOut() -> Bool {
		do {
			try Auth.auth().signOut()
			UserDefaults.standard.removeObject(forKey: "currentUser")
			UserDefaults.standard.synchronize()
			return true
		} catch {
			return false
		}
	}


	// MARK: - Private methods

	class func load(firuser: User, completion: @escaping (_ user: FUser?, _ error: Error?) -> Void) {
        let user = FUser(path: FUser.path)
        user[FUsers.keys.objectId.rawValue] = firuser.uid
		user.fetchInBackground(objId: firuser.uid, block: { error in
			if error != nil {
				self.create(uid: firuser.uid, email: firuser.email, completion: completion)
			} else {
				completion(user, nil)
			}
		})
	}


    class func create(uid: String, email: String?, completion: @escaping (_ user: FUser?, _ error: Error?) -> Void) {

        let user = FUser(path: FUser.path)
        user[FUsers.keys.objectId.rawValue] = uid

		if let email = email {
            user[FUsers.keys.email.rawValue] = email
		}
		user.saveInBackground(block: { error in
			if error == nil {
                completion(user, nil)
			} else {
				completion(nil, error)
			}
		})
	}


	// MARK: - Current user methods

    var isCurrent: Bool {
        if let objectId = self["objectId"] as? String {
            return objectId == Auth.auth().currentUser?.uid
        }
        return false
    }


    func saveLocalIfCurrent() {
        if self.isCurrent {
            UserDefaults.standard.set(self.values, forKey: "currentUser")
            UserDefaults.standard.synchronize()
        }
	}


	// MARK: - Save methods

    override func saveInBackground() {
        self.saveLocalIfCurrent()
		super.saveInBackground()
	}


    override func saveInBackground(block: @escaping (_ error: Error?) -> Void) {
        super.subpathX = Auth.auth().currentUser?.uid
        super.saveInBackground(block: { error in
            if error == nil {
                self.saveLocalIfCurrent()
            }
            block(error)
        })
	}


	// MARK: - Fetch methods

    override func fetchInBackground() {
        super.subpathX = FUser.currentUser?.objectId()
		super.fetchInBackground(block: { error in
			if error == nil {
				self.saveLocalIfCurrent()
			}
		})
	}


    func fetchInBackground(objId: String, block: @escaping (_ error: Error?) -> Void) {
        super.subpathX = objId
		super.fetchInBackground(block: { error in
			if error == nil {
				self.saveLocalIfCurrent()
			}
            block(error)
		})
	}

}



extension FUser {

    private func id() -> String? {
        return self[FUsers.keys.objectId.rawValue] as? String
    }


    class func currentUserId() -> String? {
        return FUser.currentUser?.id()
    }


    private func fcmToken() -> String? {
        return self[FUsers.keys.fcmToken.rawValue] as? String
    }


    class func fcmToken() -> String? {
        return FUser.currentUser?.fcmToken()
    }


    private func name() -> String? {
        return self[FUsers.keys.name.rawValue] as? String
    }


    class func currentUserName() -> String? {
        return FUser.currentUser?.name()
    }


    private func country() -> String? {
        return self[FUsers.keys.country.rawValue] as? String
    }


    class func currentUserCountry() -> String? {
        return FUser.currentUser?.country()
    }


    private func type() -> Int? {
        return self[FUsers.keys.type.rawValue] as? Int
    }


    class func currentUserType() -> Int? {
        return FUser.currentUser?.type()
    }


    private func email() -> String? {
        return self[FUsers.keys.email.rawValue] as? String
    }


    class func currentUserEmail() -> String? {
        return FUser.currentUser?.email()
    }


    private func avatar() -> String? {
        return self[FUsers.keys.avatar.rawValue] as? String
    }


    class func currentUserAvatar() -> String? {
        return FUser.currentUser?.avatar()
    }


    private func age() -> Int? {
        return self[FUsers.keys.age.rawValue] as? Int
    }


    class func currentUserAge() -> Int? {
        return FUser.currentUser?.age()
    }


    private func phoneNumber() -> String? {
        return self[FUsers.keys.phoneNumber.rawValue] as? String
    }


    class func currentUserPhoneNumber() -> String? {
        return FUser.currentUser?.phoneNumber()
    }
}
