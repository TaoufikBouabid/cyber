//
//  FSocialNetworks.swift
//  CyberOne
//
//  Created by Taoufik on 07/01/2019.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import Foundation

class FSocialNetworks: NSObject {
    
    var socialNetworksID: String!
    var logo: String!
    var selectedLogo: String!
    var name: String!
    var order: Int!
    
    init(dict: [String:Any]) {
        
        if let socialNetworksID = dict["objectId"] as? String {
            self.socialNetworksID = socialNetworksID
        }
        
        if let logo = dict["logo"] as? String {
            self.logo = logo
        }
        
        if let selectedLogo = dict["logoSelected"] as? String {
            self.selectedLogo = selectedLogo
        }
        
        if let name = dict["name"] as? String {
            self.name = name
        }
        
        if let order = dict["order"] as? Int {
            self.order = order
        }
        
        let languages = UserDefaults.standard.dictionary(forKey: "Languages")

        if dict["logo"] == nil && dict["logoSelected"] == nil {
            if let name = dict["name"] as? String {
                if let languages = languages, let langTitle = languages[name] as? [String: Any], let translatedTitle = langTitle[Languages.currentLanguage()] as? String {
                    self.name = translatedTitle
                } else {
                    self.name = name
                }
            }
        }
    }
}
