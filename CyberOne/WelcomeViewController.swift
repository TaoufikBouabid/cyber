//
//  WelcomeViewController.swift
//  CyberOne
//
//  Created by Taoufik on 07/11/2018.
//  Copyright © 2018 Cyber One. All rights reserved.
//

import UIKit



final class WelcomeViewController: UIViewController {

    @IBOutlet weak var firstHolderView: UIView!
    @IBOutlet weak var secondHolderView: UIView!
    @IBOutlet weak var englishLangButton: UIButton!
    @IBOutlet weak var frenshLangButton: UIButton!
    @IBOutlet weak var hebrewLangButton: UIButton!
    @IBOutlet weak var arabichLangButton: UIButton!
    @IBOutlet weak var selectLangView: UIView!
    private var language: String!

    override func viewDidLoad() {
        super.viewDidLoad()

        switch Languages.currentLanguage() {
        case "fr":
            self.french()
            break
        case "he":
            self.hebrew()
            break
        case "ar":
            self.arabic()
            break
        default:
            self.english()
            break
        }
    }


    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        self.firstHolderView.roundCorners(corners: [.topLeft,.topRight], cornerRadius: 10)
        self.secondHolderView.roundCorners(corners: [.bottomLeft,.bottomRight], cornerRadius: 10)
    }


    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }


    private func english() {
        self.englishLangButton.backgroundColor = UIColor.darkGray.withAlphaComponent(0.25)
        self.arabichLangButton.backgroundColor = UIColor.darkGray.withAlphaComponent(0)
        self.hebrewLangButton.backgroundColor = UIColor.darkGray.withAlphaComponent(0)
        self.frenshLangButton.backgroundColor = UIColor.darkGray.withAlphaComponent(0)
        self.language = LanguagesCodes.en.rawValue
    }


    @IBAction func englishAction(_ sender: UIButton) {
        self.englishLangButton.backgroundColor = UIColor.darkGray.withAlphaComponent(0.25)
        self.arabichLangButton.backgroundColor = UIColor.darkGray.withAlphaComponent(0)
        self.hebrewLangButton.backgroundColor = UIColor.darkGray.withAlphaComponent(0)
        self.frenshLangButton.backgroundColor = UIColor.darkGray.withAlphaComponent(0)
        self.language = LanguagesCodes.en.rawValue
    }


    private func arabic() {
        self.arabichLangButton.backgroundColor = UIColor.darkGray.withAlphaComponent(0.25)
        self.englishLangButton.backgroundColor = UIColor.darkGray.withAlphaComponent(0)
        self.hebrewLangButton.backgroundColor = UIColor.darkGray.withAlphaComponent(0)
        self.frenshLangButton.backgroundColor = UIColor.darkGray.withAlphaComponent(0)
        self.language = LanguagesCodes.ar.rawValue
    }


    @IBAction func arabicAction(_ sender: UIButton) {
        self.arabic()
    }


    private func hebrew() {
        self.hebrewLangButton.backgroundColor = UIColor.darkGray.withAlphaComponent(0.25)
        self.englishLangButton.backgroundColor = UIColor.darkGray.withAlphaComponent(0)
        self.arabichLangButton.backgroundColor = UIColor.darkGray.withAlphaComponent(0)
        self.frenshLangButton.backgroundColor = UIColor.darkGray.withAlphaComponent(0)
        self.language = LanguagesCodes.he.rawValue
    }


    @IBAction func hebrewAction(_ sender: UIButton) {
        self.hebrew()
    }


    private func french() {
        self.frenshLangButton.backgroundColor = UIColor.darkGray.withAlphaComponent(0.25)
        self.englishLangButton.backgroundColor = UIColor.darkGray.withAlphaComponent(0)
        self.arabichLangButton.backgroundColor = UIColor.darkGray.withAlphaComponent(0)
        self.hebrewLangButton.backgroundColor = UIColor.darkGray.withAlphaComponent(0)
        self.language = LanguagesCodes.fr.rawValue
    }


    @IBAction func frenshAction(_ sender: UIButton) {
        self.french()
    }


    @IBAction func selectLangAction(_ sender: UIButton) {
        switch self.language {
        case LanguagesCodes.en.rawValue:
            self.showDemoViewController(isRightToLeft: false, code: LanguagesCodes.en.rawValue)
            break
        case LanguagesCodes.fr.rawValue:
            self.showDemoViewController(isRightToLeft: false, code: LanguagesCodes.fr.rawValue)
            break
        case LanguagesCodes.ar.rawValue:
            self.showDemoViewController(isRightToLeft: true, code: LanguagesCodes.ar.rawValue)
            break
        case LanguagesCodes.he.rawValue:
            self.showDemoViewController(isRightToLeft: true, code: LanguagesCodes.he.rawValue)
            break
        default:
            let demoVc = StoryboardProvider.instance.storyboard(for: .main).instantiateViewController(
                withIdentifier: "DemoViewController"
            )
            demoVc.modalPresentationStyle = .fullScreen
            self.present(demoVc, animated: true)
        }
    }


    private func showDemoViewController(isRightToLeft: Bool, code: String) {
        Localizer.DoTheExchange()
        Languages.setAppLanguage(code)
        UIView.appearance().semanticContentAttribute = isRightToLeft ? .forceRightToLeft : .forceLeftToRight
        UINavigationBar.appearance().semanticContentAttribute = isRightToLeft ? .forceRightToLeft : .forceLeftToRight
        let demo = StoryboardProvider.instance.storyboard(for: .main).instantiateViewController(
            withIdentifier: "DemoViewController"
        )
        demo.modalPresentationStyle = .fullScreen
        self.present(demo, animated: true)
    }
}



public enum LanguagesCodes: String {
    case ar,en,nl,ja,ko,vi,ru,sv,fr,es,pt,it,de,da,fi,nb,tr,el,id,
    ms,th,hi,hu,pl,cs,sk,uk,hr,ca,ro,he,ur,fa,ku,arc,sl,ml
    case enGB = "en-GB"
    case enAU = "en-AU"
    case enCA = "en-CA"
    case enIN = "en-IN"
    case frCA = "fr-CA"
    case esMX = "es-MX"
    case ptBR = "pt-BR"
    case zhHans = "zh-Hans"
    case zhHant = "zh-Hant"
    case zhHK = "zh-HK"
    case es419 = "es-419"
    case ptPT = "pt-PT"
    case deviceLanguage
}
