//
//  AppDelegate.swift
//  CyberOne
//
//  Created by Taoufik on 07/11/2018.
//  Copyright © 2018 Cyber One. All rights reserved.
//

import UIKit
import Firebase
import IQKeyboardManagerSwift
import FirebaseDatabase
import SwiftRater
import FirebaseMessaging
import UserNotifications
import NotificationView
import JustLog
import FirebaseRemoteConfig



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var WPSharedInstance: WP?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        FirebaseApp.configure()
        self.setupKeyboardManager()
        self.fetchLanguagesInBackground()
        self.setupLogger()
        self.setupNotifications(application)
        self.setupCurrentUser()
        self.setupSwiftRater()
        self.setupAppNavBarAppearance()

        return true
    }


    private func setupAppNavBarAppearance() {
        UIBarButtonItem.appearance().setTitleTextAttributes(
            [NSAttributedStringKey.font: h2Regular],
            for: .normal
        )
        UIBarButtonItem.appearance().setTitleTextAttributes(
            [NSAttributedStringKey.font: h2Regular],
            for: .highlighted
        )
        UIBarButtonItem.appearance().setTitleTextAttributes(
            [NSAttributedStringKey.font: h2Regular],
            for: .focused
        )
        UINavigationBar.appearance().titleTextAttributes = [
            NSAttributedStringKey.font: h2Regular,
            NSAttributedStringKey.foregroundColor: UIColor.white
        ]
    }


    private func fetchLanguagesInBackground() {
        let reference = Database.database().reference(withPath: "Language")
        reference.observeSingleEvent(of: DataEventType.value, with: { snapshot in
            if snapshot.exists(), let value = snapshot.value as? [String: Any] {
                UserDefaults.standard.set(value, forKey: "Languages")
            }
        })
    }


    private func getConfigurations() {
        let configurations = FObject(path: "Configurations")
        configurations.fetchInBackground { error in
            if error == nil {
                if let aboutApp = configurations.values["aboutApp"] as? String {
                    UserDefaults.standard.set(
                        aboutApp,
                        forKey: "aboutApp"
                    )
                }
                if let shareText = configurations.values["shareText"] as? String {
                    UserDefaults.standard.set(
                        shareText,
                        forKey: "shareText"
                    )
                }
                
                if let appStoreURL = configurations.values["appStoreURL"] as? String {
                    UserDefaults.standard.set(
                        appStoreURL,
                        forKey: "appStoreLink"
                    )
                }
            }
        }
    }


    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }


    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        Logger.shared.info("Firebase registration token: \(fcmToken ?? "")")
        let dataDict = ["token": fcmToken]
        UserDefaults.standard.set(dataDict, forKey: "FCMToken")

        if UserDefaults.standard.bool(forKey: "isUserLoggedIn") {
            if let tokenDict = UserDefaults.standard.dictionary(forKey: "FCMToken") as? [String: String],
                let token = tokenDict["token"],
                let userToken = FUser.fcmToken(),
               let id = FUser.currentUserId(),
                userToken.isEmpty {
                let object = FObject(
                    path: FUser.path,
                    subpath: id
                )
                object[FUsers.keys.objectId.rawValue] = id
                object[FUsers.keys.fcmToken.rawValue] = token
                object.updateInBackground()
            }
        }
    }


    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if let data = userInfo as? [String: Any] {
            if data["report_id"] as? String != nil {
                let notificationView = NotificationView.default
                notificationView.title = data["title"] as? String ?? ""
                notificationView.subtitle = ""
                notificationView.body = data["body"] as? String ?? ""
                notificationView.image = UIImage(named: "logo-\(Languages.currentLanguage())") ?? UIImage(named: "logo")
                notificationView.show()
            }
        }
    }


    private func setupLogger() {
        let logger = Logger.shared

        // file destination
        logger.logFilename = "cyberOne.log"

        // logstash destination
        logger.enableLogstashLogging = false
        logger.enableFileLogging = true
        logger.enableConsoleLogging = true

        // default info
        logger.defaultUserInfo = [
            "app": "cyberOne",
            "environment": "production",
            "tenant": "UK"
        ]
        logger.setup()
    }


    private func setupSwiftRater() {
        SwiftRater.daysUntilPrompt = 0
        SwiftRater.usesUntilPrompt = 1
        SwiftRater.daysBeforeReminding = 1
        SwiftRater.showLaterButton = true
        SwiftRater.showLog = true
        //        SwiftRater.debugMode = true // need to set false when submitting to AppStore!!
        SwiftRater.appLaunched()
    }


    private func setupCurrentUser() {
        if UserDefaults.standard.bool(forKey: "isUserLoggedIn") {

            // setup the wordpress api
            self.setupWP()
            if let tokenDict = UserDefaults.standard.dictionary(forKey: "FCMToken") as? [String: String],
                let token = tokenDict["token"],
               let userToken = FUser.fcmToken(),
               let id = FUser.currentUserId(),
                userToken.isEmpty {
                let object = FObject(
                    path: FUser.path,
                    subpath: id
                )
                object[FUsers.keys.objectId.rawValue] = id
                object[FUsers.keys.fcmToken.rawValue] = token
                object.updateInBackground()
            }

            getIp { ip in
                if let id = FUser.currentUserId() {
                    let object = FObject(
                        path: FUser.path,
                        subpath: id
                    )
                    object["ip"] = ip
                    object[FUsers.keys.objectId.rawValue] = id
                    object.updateInBackground()
                }
            }

            self.getConfigurations()
            FUser.currentUser?.fetchInBackground()
            self.window?.rootViewController = UIStoryboard(
                name: "Main",
                bundle: nil
            ).instantiateViewController(withIdentifier: "TabBarController")
            self.window?.makeKeyAndVisible()
        }
    }


    private func setupNotifications(_ application: UIApplication) {
        UNUserNotificationCenter.current().delegate = self
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: { _, _ in }
        )
        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
    }


    private func setupKeyboardManager() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
    }


    private func loadWPAuthToken(
        completion: @escaping ((_ authKey: String?) -> Void)
    ) {
        let remoteConfig = RemoteConfig.remoteConfig()
        remoteConfig.fetchAndActivate { status, error in
            guard error == nil else {
                Logger.shared.error("[AppDelegate] loadWPAuthToken, failed to load WordPress auth token")
                return
            }
            let authKey = remoteConfig.configValue(forKey: "WP_token").stringValue
            UserDefaults.standard.set(
                authKey,
                forKey: CYBERONE_WP_AUTH_KEY
            )
            UserDefaults.standard.synchronize()
            completion(authKey)
        }
    }


    func setupWP() {
        self.loadWPAuthToken { authKey in
            if let authKey = authKey {
                _ = WP(baseURL: CYBERONE_WP_URL)
                self.WPSharedInstance = WP.sharedInstance
                self.WPSharedInstance?.updateAuthToken(authToken: authKey) { _ in }
            }
        }
    }

}
