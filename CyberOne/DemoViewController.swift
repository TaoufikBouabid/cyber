//
//  DemoViewController.swift
//  CyberOne
//
//  Created by Taoufik on 07/11/2018.
//  Copyright © 2018 Cyber One. All rights reserved.
//

import UIKit
import SDWebImage
import FirebaseDatabase
import ProgressHUD



final class DemoViewController: UIViewController, UIScrollViewDelegate {
    static let storyboardId = "DemoViewController"
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var firstImage: UIImageView!
    @IBOutlet weak var firstText: UILabel!

    @IBOutlet weak var secondImage: UIImageView!
    @IBOutlet weak var secondText: UILabel!

    @IBOutlet weak var thirdImage: UIImageView!
    @IBOutlet weak var thirdText: UILabel!

    @IBOutlet weak var fourthImage: UIImageView!
    @IBOutlet weak var fourthText: UILabel!

    @IBOutlet weak var fifthImage: UIImageView!
    @IBOutlet weak var fifthText: UILabel!

    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var bg: UIImageView!
    @IBOutlet weak var lbl: UILabel!

    var data = [Tutorial]()
    var firstLoad = true


    override func viewDidLoad() {
        super.viewDidLoad()
        self.firstImage.clipsToBounds = true
        self.secondImage.clipsToBounds = true
        self.thirdImage.clipsToBounds = true
        self.fourthImage.clipsToBounds = true
        self.fifthImage.clipsToBounds = true

        self.scrollView.delegate = self
        self.loginButton.shadowWithCornder(
            raduis: 8,
            shadowColor: UIColor.lightGray.cgColor,
            shadowOffset: CGSize.init(width: 2, height: 2),
            shadowOpacity: 1,
            shadowRadius: 5
        )
        self.pageControl.numberOfPages = 5
        self.loadData()
    }


    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.firstLoad {
            if Languages.isRightToLeft() {
                self.pageControl.currentPage = 4
                var frame = scrollView.frame
                frame.origin.x = frame.size.width * 4
                frame.origin.y = 0
                self.scrollView.contentOffset = CGPoint(x: frame.origin.x, y: frame.origin.y)
            }
            self.firstLoad = false
        }
    }


    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        var page = scrollView.contentOffset.x / scrollView.frame.size.width
        if Languages.isRightToLeft() {
            page = 4 - page
        }
        self.pageControl.currentPage = Int(page)
        self.lbl.text = self.labels()[Int(page)]
    }


    @IBAction func pageControlClicked(_ sender: Any) {
        let page = self.pageControl.currentPage
        var frame = self.scrollView.frame
        frame.origin.x = frame.size.width * CGFloat(page)
        frame.origin.y = 0
        self.scrollView.scrollRectToVisible(frame, animated: true)
    }


    func loadData() {
        let tutorial = FObject(path: Tutorial.path)
        ProgressHUD.show()
        tutorial.fetchInBackground { error in
            if error == nil {
                for tuto in tutorial.values {
                    if let value = tuto.value as? [String: Any] {
                        self.data.append(Tutorial(dict: value))
                    }
                }
                self.data.sort { $0.order < $1.order }
                self.reloadData()
            }
        }
    }

    
    private func reloadData() {
        self.lbl.text = self.labels().first
        if self.data.count == 5 {
            self.firstImage.sd_setImage(with: URL(string: data[0].image))
            self.secondImage.sd_setImage(with: URL(string: data[1].image))
            self.thirdImage.sd_setImage(with: URL(string: data[2].image))
            self.fourthImage.sd_setImage(with: URL(string: data[3].image))
            self.fifthImage.sd_setImage(with: URL(string: data[4].image))
        } else if self.data.count == 4 {
            self.firstImage.sd_setImage(with: URL(string: data[0].image))
            self.secondImage.sd_setImage(with: URL(string: data[1].image))
            self.thirdImage.sd_setImage(with: URL(string: data[2].image))
            self.fourthImage.sd_setImage(with: URL(string: data[3].image))
            self.fifthImage.sd_setImage(with: URL(string: data[3].image))
        } else if data.count == 3 {
            self.firstImage.sd_setImage(with: URL(string: data[0].image))
            self.secondImage.sd_setImage(with: URL(string: data[1].image))
            self.thirdImage.sd_setImage(with: URL(string: data[2].image))
            self.fourthImage.sd_setImage(with: URL(string: data[2].image))
            self.fifthImage.sd_setImage(with: URL(string: data[2].image))
        } else if data.count == 2 {
            self.firstImage.sd_setImage(with: URL(string: data.first?.image ?? ""))
            self.thirdImage.sd_setImage(with: URL(string: data.first?.image ?? ""))
            self.secondImage.sd_setImage(with: URL(string: data[1].image))
            self.fourthImage.sd_setImage(with: URL(string: data[1].image))
            self.fifthImage.sd_setImage(with: URL(string: data[1].image))
        } else if data.count == 1 {
            self.firstImage.sd_setImage(with: URL(string: data.first?.image ?? ""))
            self.secondImage.sd_setImage(with: URL(string: data.first?.image ?? ""))
            self.thirdImage.sd_setImage(with: URL(string: data.first?.image ?? ""))
            self.fourthImage.sd_setImage(with: URL(string: data.first?.image ?? ""))
            self.fifthImage.sd_setImage(with: URL(string: data.first?.image ?? ""))
        }
        ProgressHUD.dismiss()
    }


    private func labels() -> [String] {
        if self.data.count == 5 {
            return [
                data.first!.desc,
                data[1].desc,
                data[2].desc,
                data[3].desc,
                data[4].desc
            ]
        } else if self.data.count == 4 {
            return [
                data.first!.desc,
                data[1].desc,
                data[2].desc,
                data[3].desc,
                data[3].desc
            ]
        } else if data.count == 3 {
            return [
                data.first!.desc,
                data[1].desc,
                data[2].desc,
                data[2].desc,
                data[2].desc
            ]
            
        } else if data.count == 2 {
            return [
                data.first!.desc,
                data[1].desc,
                data[1].desc,
                data[1].desc,
                data[1].desc
            ]
        } else if data.count == 1 {
            
            return [
                data.first!.desc,
                data.first!.desc,
                data.first!.desc,
                data.first!.desc,
                data.first!.desc
            ]
        }
        return [
            "test",
            "test",
            "test",
            "test",
            "test"
        ]
    }


    @IBAction private func signup(_ sender: UIButton) {
        let signupVc = StoryboardProvider.instance.storyboard(for: .main).instantiateViewController(withIdentifier: SignUpViewController.identifier)
        let navigationController = UINavigationController(rootViewController: signupVc)
        navigationController.modalPresentationStyle = .fullScreen
        navigationController.modalTransitionStyle = .crossDissolve
        self.present(navigationController, animated: true)
    }


    @IBAction private func login(_ sender: UIButton) {
        let signupVc = StoryboardProvider.instance.storyboard(for: .main).instantiateViewController(withIdentifier: LoginTableViewController.holderVcId)
        let navigationController = UINavigationController(rootViewController: signupVc)
        navigationController.modalPresentationStyle = .fullScreen
        navigationController.modalTransitionStyle = .crossDissolve
        self.present(navigationController, animated: true)
    }

}
