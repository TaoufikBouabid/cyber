//
//  LoadingView.swift
//  Exchange
//
//  Created by Taoufik on 6/22/17.
//  Copyright © 2022 CyberOne. All rights reserved.
//

import UIKit

class LoadingView: UIView {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var loadingLabel: UILabel!
    @IBOutlet weak var noDataImage: UIImageView!
    @IBOutlet weak var tryAgainButton: UIButton!
    var loadingMessage: String!
    var noDataMessage: String!
    var image: UIImage!
    var hideButton = true
    var hideMsg = true
    var isProcessing: Bool = false{
        didSet{
            if isProcessing{
                loadingLabel.text = loadingMessage
                noDataImage.isHidden = true
                tryAgainButton.isHidden = true
                activityIndicator.startAnimating()
            }else{
                tryAgainButton.isHidden = hideButton
                noDataImage.isHidden = false
                noDataImage.image = image
                loadingLabel.text = noDataMessage
                loadingLabel.isHidden = hideMsg
                activityIndicator.stopAnimating()
            }
        }
    }
    class func loadFromNibNamed(nibNamed: String, bundle : Bundle? = nil) -> LoadingView? {
        return UINib(nibName: nibNamed, bundle: bundle).instantiate(withOwner: nil, options: nil)[0] as? LoadingView
    }
}
