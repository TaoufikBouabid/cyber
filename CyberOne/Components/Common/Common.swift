//
//  Common.swift
//  CyberOne
//
//  Created by Taoufik on 17/12/2018.
//  Copyright © 2018 Cyber One. All rights reserved.
//

import Foundation
import Toaster


let h1Regular = UIFont(name:"TheSans-Plain" ,size: 19) ?? .systemFont(ofSize: 19)
let h2Regular = UIFont(name:"TheSans-Plain", size: 17) ?? .systemFont(ofSize: 17)
let h3Regular = UIFont(name:"TheSans-Plain", size: 15) ?? .systemFont(ofSize: 15)
let h4Regular = UIFont(name:"TheSans-Plain", size: 11) ?? .systemFont(ofSize: 11)
let toastColor = UIColor(hex: 0xBD242B)



func showToast(_ message: String, backgroundColor: UIColor, textColor: UIColor, font: UIFont) {
    ToastCenter.default.cancelAll()
    let appearance = ToastView.appearance()
    appearance.backgroundColor = backgroundColor
    appearance.textColor = textColor
    appearance.font = font
    Toast(text: message, duration: Delay.short).show()
}


extension UIView {
    func roundView(_ round: CGFloat) {
        let layer: CALayer = self.layer
        layer.masksToBounds = true
        layer.cornerRadius = round
    }


    func roundViewWithoutMasks(_ round: CGFloat) {
        let layer: CALayer = self.layer
        layer.masksToBounds = false
        layer.cornerRadius = round
    }
}


func getIp(_ completionBlock: @escaping (_ result: String?) -> Void) {
    let url : String = "https://youz.co/ip"
    var request : URLRequest = URLRequest(url: URL(string: url)!)
    request.httpMethod = "GET"
    NSURLConnection.sendAsynchronousRequest(request, queue: OperationQueue.main) { (response,data, error) in
        guard let data = data else { return; }
        do {
            let jsonResult: NSDictionary! = try JSONSerialization.jsonObject(with: data, options:JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
            if (jsonResult != nil) {
                // process jsonResult
                if let ip = jsonResult?.value(forKey: "ip") as? String {
                    completionBlock(ip)
                }
            } else {
                // couldn't load JSON, look at error
                completionBlock(nil)
            }
        } catch {
            completionBlock(nil)
        }
    }
}
