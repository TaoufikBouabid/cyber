//
//  CoutrySelectorViewController.swift
//  Exchange
//
//  Created by Taoufik on 6/22/17.
//  Copyright © 2022 CyberOne. All rights reserved.
//

import UIKit
import STPopup
import JustLog



protocol CountrySelectorDelegate {
    func didSelectCountry(country: Country, viewController: UIViewController)
}



final class CountrySelectorViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    static let identifier = "CountrySelectorViewController"

    struct Const {
        static let fileName = "Countries"
        static let fileExt = "json"
    }

    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak private var searchBar: UISearchBar!

    private var countries = [Country]()
    private var filteredCountries = [Country]()
    private var selectedIndexPath : IndexPath!
    var delegate: CountrySelectorDelegate!
    var showDialCode: Bool = false


    override init(
        nibName nibNameOrNil: String?,
        bundle nibBundleOrNil: Bundle?
    ) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        let screenSize = UIScreen.main.bounds.size
        self.contentSizeInPopup = CGSize(
            width: screenSize.width - 60,
            height: screenSize.height - 160
        )
    }


    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.popupController?.navigationBar.barTintColor = UIColor(hex: 0x2062AC)
        self.popupController?.navigationBar.tintColor = .white
        self.popupController?.navigationBar.isTranslucent = false
        self.popupController?.navigationBar.titleTextAttributes = [
            .foregroundColor: UIColor.white,
            .font: h2Regular
        ]
        self.popupController?.containerView.layer.cornerRadius = 10
        self.popupController?.containerView.clipsToBounds = true
        self.popupController?.hidesCloseButton = false
        self.title = CHOOSE_COUNTRY_STR

        self.searchBar.delegate = self
        self.tableView.dataSource = self
        self.tableView.delegate = self

        self.tableView.register(
            UINib(
                nibName: CountryViewCell.identifier,
                bundle: nil
            ),
            forCellReuseIdentifier: CountryViewCell.identifier
        )

        let bundle =  Bundle(for: type(of: self))
        if let filePath = bundle.path(
            forResource: Const.fileName,
            ofType: Const.fileExt
        ) {
            do {
                let data = try String(contentsOfFile: filePath, encoding: .utf8).data(using: .utf8)
                self.countries = try JSONDecoder().decode([Country].self, from: data!)
                self.filteredCountries = self.countries
                self.tableView.reloadData()
            } catch {
                Logger.shared.error("[CountrySelectorViewController] viewDidLoad, cannot load countries, error: \(error.localizedDescription)")
            }
        }
    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(
            withIdentifier: CountryViewCell.identifier,
            for: indexPath
        ) as? CountryViewCell else {
            return UITableViewCell()
        }
        let country = self.filteredCountries[indexPath.row]
        cell.nameLabel.text = Languages.currentLanguage() == "ar" ? country.nameAr : country.nameEn
        cell.codeLabel.text = country.dialCode
        cell.flagLabel.text = country.code?.emojiFlag()
        if self.selectedIndexPath != nil && self.selectedIndexPath == indexPath {
            cell.backgroundColor = UIColor(hex: 0xf6921e).withAlphaComponent(0.2)
        } else {
            cell.backgroundColor = UIColor.white
        }
        cell.codeLabel.isHidden = !self.showDialCode
        return cell
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredCountries.count
    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }


    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let oldIndexPath = self.selectedIndexPath
        self.selectedIndexPath = indexPath
        self.tableView.reloadRows(
            at: [
                self.selectedIndexPath,
                oldIndexPath ?? self.selectedIndexPath
            ],
            with: .automatic
        )
        self.delegate.didSelectCountry(
            country: self.filteredCountries[indexPath.row],
            viewController: self
        )
    }
    
}


extension CountrySelectorViewController: UISearchBarDelegate {

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        if searchText.count == 0 {
            self.filteredCountries = self.countries
            self.tableView.reloadData()
            return
        }

        if Languages.currentLanguage() == "ar" {
            self.filteredCountries = self.countries.filter { $0.nameAr?.contains(searchText) ?? false }
            self.tableView.reloadData()
        } else {
            self.filteredCountries = self.countries.filter { $0.nameEn?.contains(searchText) ?? false }
            self.tableView.reloadData()
        }
    }

}
