//
//  CountryViewCell.swift
//  Exchange
//
//  Created by Taoufik on 6/22/17.
//  Copyright © 2022 CyberOne. All rights reserved.
//

import UIKit



final class CountryViewCell: UITableViewCell {
    static let identifier = "CountryViewCell"

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var flagLabel: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
