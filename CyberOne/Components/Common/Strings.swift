//
//  Strings.swift
//  CyberOne
//
//  Created by Taoufik on 01/01/2019.
//  Copyright © 2022 Cyber One. All rights reserved.
//

let EMPTY_FIELDS_STR = NSLocalizedString("Please fill empty fields!", comment: "")
let EMPTY_NAME_STR = NSLocalizedString("Please fill the name!", comment: "")
let EMPTY_COUNTRY_STR = NSLocalizedString("Please fill the country!", comment: "")
let EMPTY_PHONE_STR = NSLocalizedString("Please fill the phone number!", comment: "")
let VALID_PHONE_STR = NSLocalizedString("Please enter a valid phone number!", comment: "")
let EMPTY_PASSWORD_STR = NSLocalizedString("Please fill the password!", comment: "")
let LENGTH_PASSWORD_STR = NSLocalizedString("Password should be at least 6 characters!", comment: "")
let OK_STR = NSLocalizedString("Ok", comment: "")
let CHOOSE_COUNTRY_STR = NSLocalizedString("Select a country", comment: "")
let ERROR_OCCURED_STR = NSLocalizedString("An error has occured", comment: "")
let NO_DATA_STR = NSLocalizedString("No data available!", comment: "")
let PASSWORD_NOT_CONFIRM_STR = NSLocalizedString("Passwords does not match", comment: "")

let PHONE_ALREADY_USED_STR = NSLocalizedString("The phone number is already in use by another account", comment: "")
let INCORRECT_OTP_STR = NSLocalizedString("Incorrect code",comment: "")

//Report
let EMPTY_FULLNAME_STR = NSLocalizedString("Please fill the full name!", comment: "")
let EMPTY_AGE_STR = NSLocalizedString("Please fill the age!", comment: "")
let VERIFY_AGE_STR = NSLocalizedString("Please verify the age!", comment: "")
let EMPTY_EMAIL_STR = NSLocalizedString("Please fill the email!", comment: "")
let VALID_EMAIL_STR = NSLocalizedString("Please enter a valid email!", comment: "")
let EMPTY_REMARKS_STR = NSLocalizedString("Please fill the remarks!", comment: "")
let EMPTY_ASSETS_STR = NSLocalizedString("You have to attach files!", comment: "")
let REPORT_IS_CLOSED = NSLocalizedString("Close conversation", comment: "")
let REPORT_IS_OPENED = NSLocalizedString("Open conversation", comment: "")
let EMPTY_URL_STR = NSLocalizedString("Please fill the url!", comment: "")
let NOT_VALID_URL_STR = NSLocalizedString("The URL is not valid!", comment: "")
let EMPTY_COPYRIGHTS_STR = NSLocalizedString("Please fill the copyrights!", comment: "")
let EMPTY_ACCOUNT_NAME_LINK_STR = NSLocalizedString("Please fill the account name or link!", comment: "")
let EMPTY_ACCOUNT_EMAIL_STR = NSLocalizedString("Please fill the account email address!", comment: "")
let VALID_ACCOUNT_EMAIL_STR = NSLocalizedString("Please enter a valid account email address!", comment: "")
let EMPTY_ACCOUNT_PASSWORD_STR = NSLocalizedString("Please fill the account's email password!", comment: "")
let EMPTY_ACCOUNT_LAST_PASSWORD_STR = NSLocalizedString("Please fill the account's last email password!", comment: "")
let REMOVE_AUDIO_RECORD_STR = NSLocalizedString("Do you want to remove the recorded audio ?", comment: "")


let STEP0_STR = NSLocalizedString("Report received", comment: "")
let STEP1_STR = NSLocalizedString("Verifying data", comment: "")
let STEP2_STR = NSLocalizedString("Request accepted", comment: "")
let STEP3_STR = NSLocalizedString("In process", comment: "")
let STEP4_STR = NSLocalizedString("Problem resolved", comment: "")

let HOLD_STR = NSLocalizedString("Hold to send", comment: "")
let RECORDING_STR = NSLocalizedString("Tap to stop recording", comment: "")

//ContactUS
let EMPTY_SUBJECT_STR = NSLocalizedString("Please fill the subject!", comment: "")
let EMPTY_BODY_STR = NSLocalizedString("Please fill the letter body!", comment: "")

//Chats
let NO_CHATS_FOUND = NSLocalizedString("No chats Found", comment: "")
let NO_MESSAGES_FOUND = NSLocalizedString("No messages Found", comment: "")
let ENTER_MESSAGE_STR = NSLocalizedString("Enter your message...", comment: "")
let TODAY_STR = NSLocalizedString("Today", comment: "")
let YESTERDAY_STR = NSLocalizedString("Yesterday", comment: "")
let LOADING_CHAT_STR = NSLocalizedString("Loading messages in progress...", comment: "")
let EMPTY_CHAT_STR = NSLocalizedString("Empty chat", comment: "")
let CHOOSE_IMG_STR = NSLocalizedString("Choose Picture via", comment: "")
let CAMERA_STR = NSLocalizedString("Camera", comment: "")
let LIBRARY_STR = NSLocalizedString("Library", comment: "")
let CANCEL_STR = NSLocalizedString("Cancel", comment: "")
let SEND_AUDIO_STR = NSLocalizedString("Do you want to send the audio ?", comment: "")
let CLOSED_STR = NSLocalizedString("Chat Closed", comment: "")

//News
let NO_NEWS_STR = NSLocalizedString("No News Available", comment: "")
let LOADING_NEWS_STR = NSLocalizedString("Loading news in progress...", comment: "")
let COMMENTS_STR = NSLocalizedString("%@ Comments", comment: "")
let WRITE_COMMENT_STR = NSLocalizedString("Write comment please!",comment: "")
let COMMENT_ADDED_STR = NSLocalizedString("Comment added successfully!",comment: "")

//update user
let UPDATE_USER_SUCCESSFULLY_STR = NSLocalizedString("User updated successfully", comment: "")

//Subscription
let USER_ALREADY_SUBSCRIBED = NSLocalizedString("Already subscribed !", comment: "")
let USER_SUCCESS_SUBSCRIBED = NSLocalizedString("Subscription success !", comment: "")
let LOADING_SUBSCRIPTIONS_STR = NSLocalizedString("Loading subscriptions in progress...", comment: "")
let NO_SUBSCRIPTIONS_STR = NSLocalizedString("No subscriptions available", comment: "")

let NEWS_STR = NSLocalizedString("News", comment: "")
let RECORD_STR = NSLocalizedString("Tap to Record", comment: "")
let LISTEN_STR = NSLocalizedString("Tap to listen", comment: "")
let PLAY_STR = NSLocalizedString("Tap to play", comment: "")
let PAUSE_STR = NSLocalizedString("Tap to pause", comment: "")
let STEPS_STR = NSLocalizedString("Steps", comment: "")
let ALERTS_STR = NSLocalizedString("There is no alerts", comment: "")
let ALERTS_TITLE_STR = NSLocalizedString("Alerts", comment: "")
let SOCIAL_NETWORK_LABEL_STR = NSLocalizedString("Do you have a %@ account ?", comment: "")
let IMPERSONATION_ATTACHEMENT_STR = NSLocalizedString("Add the image of the fake account or any other attachment you want to send in order to look at the problem accurately.", comment: "")
let EXTORTION_ATTACHEMENT_STR = NSLocalizedString("Add pictures of harassment, threats or any other attachment you want to send to get an accurate look at the problem", comment: "")
let COPYRIGHTS_ATTACHEMENT_STR = NSLocalizedString("Add a photo about copyright and privacy or any other attachment you want to post to get an accurate look at the problem", comment: "")
let hacked_ATTACHEMENT_STR = NSLocalizedString("Add any other attachment you want to send to get an accurate look at the problem", comment: "")

let IMPERSONATION_ACCOUNT_STR = NSLocalizedString("What is the name of the network that impersonates you ?", comment: "")
let EXTORTION_ACCOUNT_STR = NSLocalizedString("What is the name of the network that blackmails you ?", comment: "")
let HARRASSEMENT_ACCOUNT_STR = NSLocalizedString("What is the name of the network from which you are subject to harassment or threats ?", comment: "")
let hacked_ACCOUNT_STR = NSLocalizedString("The name of the network your account was compromised ?", comment: "")

let SEARCH_PHONE_NUMBER = NSLocalizedString("Search with user phone number ...", comment: "")
let COPY_STR = NSLocalizedString("Copy", comment: "")
let DELETE_STR = NSLocalizedString("Delete", comment: "")
let SAVE_STR = NSLocalizedString("Save", comment: "")
let SAVE_IMAGE_ERROR = NSLocalizedString("Something went wrong. Please retry", comment: "")
let SAVE_IMAGE_SUCCESS = NSLocalizedString("Successfully added to your photo library.", comment: "")
let ADMIN_APPROVE_STR = NSLocalizedString("Comment will be visible after it has been approved.", comment: "")

let NAME_PLACEHOLDER_STR = NSLocalizedString("Name", comment: "")
let COUNTRY_PLACEHOLDER_STR = NSLocalizedString("Country", comment: "")
let AGE_PLACEHOLDER_STR = NSLocalizedString("Age", comment: "")
let EMAIL_PLACEHOLDER_STR = NSLocalizedString("Email", comment: "")
let PHONE_NUMBER_PLACEHOLDER_STR = NSLocalizedString("Phone number", comment: "")
let ACCOUNT_NAME_OR_LINK_PLACEHOLDER_STR = NSLocalizedString("Enter the account name or link", comment: "")
let REMARKS_PLACEHOLDER_STR = NSLocalizedString("Notes", comment: "")
let ACOUNT_EMAIL_ADDRESS_PLACEHOLDER_STR = NSLocalizedString("What's your account's email address ?", comment: "")
let ACCOUNT_EMAIL_PASSWORD_PLACEHOLDER_STR = NSLocalizedString("What's your email's password ?", comment: "")
let ACCOUNT_EMAIL_LAST_PASSWORD_PLACEHOLDER_STR = NSLocalizedString("What's your account's last password ?", comment: "")
let COPYRIGHTS_PLACEHOLDER_STR = NSLocalizedString("Copyrights violation link", comment: "")

let REPORT_CHAT_STR = NSLocalizedString("Report in", comment: "")
let CREATED_CHAT_STR = NSLocalizedString("created at", comment: "")

let NO_STR = NSLocalizedString("No", comment: "")
let YES_STR = NSLocalizedString("Yes", comment: "")

let SIGNOUT_STR = NSLocalizedString("Sign out", comment: "")
