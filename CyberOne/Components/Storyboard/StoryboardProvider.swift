//
//  StoryboardProvider.swift
//  Vitlex
//
//  Created by Taoufik on 03/04/2022.
//

import Foundation
import UIKit



/** Used to get the storyboard instance  */
final class StoryboardProvider {

    /// the shared instance
    static let instance = StoryboardProvider()

    /// enum containing the names of the storyboards
    enum storyboards: String {
        case main = "Main"
        case report = "Report"
    }


    /** a private initializer */
    private init() {}


    /** the storyboard instance */
    func storyboard(for name: storyboards) -> UIStoryboard {
        return UIStoryboard(name: name.rawValue , bundle: nil)
    }

}
