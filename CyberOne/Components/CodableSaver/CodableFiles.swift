/**
* MIT License
* Copyright (c) 2022 Egzon Pllana
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

import Foundation

// MARK: - Private enum for string literals
private enum SL: String {
    case json = "json"
    case fileDirectory = "file://"
    case myAppDirectory = "MyAppDirectory"
    case cfbundleName = "CFBundleName"
    case dot = "."
}

public enum CodableFilesError: Error {
    case directoryNotFound
    case fileNotFoundInDocsDirectory
    case fileInBundleNotFound
    case unableToCreateFullPath

    public var debugDescription: String {
        switch self {
        case .directoryNotFound: return "Directory with given name not found."
        case .fileNotFoundInDocsDirectory: return "File with given name not found."
        case .fileInBundleNotFound: return "File with given name not found in the current Bundle."
        case .unableToCreateFullPath: return "Unable to create full path from given URL."
        }
    }
}

// MARK: - CodableFiles
final class CodableFiles {

    // MARK: - Properties
    /// Shared singleton instance
    static let shared = CodableFiles()

    /// Private properties
    private var fileManager: FileManager
    private var defaultDirectory: String

    // MARK: - Initialization
    private init() {
        self.fileManager = FileManager.default

        // Remove whitespaces from Bundle name.
        if let bundleName = Bundle.main.object(forInfoDictionaryKey: SL.cfbundleName.rawValue) as? String {
            self.defaultDirectory = bundleName.filter { !$0.isWhitespace }
        } else {
            self.defaultDirectory = SL.myAppDirectory.rawValue
        }
    }
}


// MARK: - Public Extensions
extension CodableFiles {
    /// Save Encodable Object.
    /// - Parameters:
    ///   - object: Encodable object.
    ///   - filename: File name to save objects data.
    /// - Returns: Returns an optional directory URL where file data is saved.
    func save(
        object: Encodable,
        withFilename filename: String
    ) throws {
        // Convert object to dictionary string
        let objectDictionary = try object.toDictionary()

        // build the fileURL
        let fileURL = try self.path(with: filename)

        // Write data to file url.
        let data = try JSONSerialization.data(withJSONObject: objectDictionary, options: [.prettyPrinted])
        try data.write(to: fileURL, options: [.atomicWrite])
    }


    /// Save array of Encodable objects.
    /// - Parameters:
    ///   - objects: Encodable objects.
    ///   - filename: File name to save objects data.
    /// - Returns: Returns an optional directory URL where file data is saved.
    func saveAsArray(
        objects: [Encodable],
        withFilename filename: String
    ) throws {
        // Convert object to dictionary string.
        let objectDictionary = try objects.map { try $0.toDictionary() }

        // build the fileURL
        let fileURL = try self.path(with: filename)

        // Write data to file url.
        let data = try JSONSerialization.data(
            withJSONObject: objectDictionary,
            options: [.prettyPrinted]
        )
        try data.write(
            to: fileURL,
            options: [.atomicWrite]
        )
    }


    /// Load object from Document Directory.
    /// - Parameters:
    ///   - objectType: Decodable object.
    ///   - filename: Object name.
    /// - Returns: Returns optional Decodable object.
    func load<T: Decodable>(
        objectType type: T.Type,
        withFilename filename: String
    ) throws -> T {

        // build the fileURL
        let fileURL = try self.path(with: filename)

        // Get data from path url.
        let contentData = try Data(contentsOf: fileURL)

        // Get json object from data.
        let jsonObject = try JSONSerialization.jsonObject(
            with: contentData,
            options: [.mutableContainers, .mutableLeaves]
        )

        // Convert json object to data type.
        let jsonData = try JSONSerialization.data(
            withJSONObject: jsonObject,
            options: []
        )

        // Decode data to Decodable object.
        let decoder = JSONDecoder()
        let decodedObject = try decoder.decode(T.self, from: jsonData)

        return decodedObject
    }


    /// Load array of Encodable objects from Documents Directory.
    /// - Parameters:
    ///   - objectType: Decodable object.
    ///   - filename: Object name.
    /// - Returns: Returns optional Decodable object.
    func loadAsArray<T: Decodable>(
        objectType type: T.Type,
        withFilename filename: String
    ) throws -> [T] {

        // build the fileURL
        let fileURL = try self.path(with: filename)

        // Get data from path url.
        let contentData = try Data(contentsOf: fileURL)

        // Get json object from data.
        let jsonObject = try JSONSerialization.jsonObject(
            with: contentData,
            options: [.mutableContainers, .mutableLeaves]
        )

        // Convert json object to data type.
        let jsonData = try JSONSerialization.data(
            withJSONObject: jsonObject,
            options: []
        )

        // Decode data to Decodable object.
        let decoder = JSONDecoder()
        let decodedObject = try decoder.decode([T].self, from: jsonData)

        return decodedObject
    }


    /// Delete file with given name at given directory.
    /// Note: if directory name is not given, it try to delete from Documents folder.
    /// - Parameter fileName: file name to delete without extension.
    func deleteFile(withFileName fileName: String) throws {
        // Get default document directory path url.
        var pathUrl = try self.fileManager.url(
            for: .documentDirectory,
               in: .userDomainMask,
               appropriateFor: nil,
               create: false
        )
        let fullFileName = fileName + SL.dot.rawValue + SL.json.rawValue
        pathUrl = pathUrl.appendingPathComponent(defaultDirectory).appendingPathComponent(fullFileName)

        // Check if the directory to be deleted already exists.
        if self.fileManager.fileExists(atPath: pathUrl.path) {
            try self.fileManager.removeItem(atPath: pathUrl.path)
        } else {
            throw CodableFilesError.fileNotFoundInDocsDirectory
        }
    }


    /// Delete specific directory, if not specified it deletes the default document directory.
    func deleteDirectory() throws {
        // Get default document directory path url.
        var documentDirectoryUrl = try fileManager.url(
            for: .documentDirectory,
               in: .userDomainMask,
               appropriateFor: nil,
               create: false
        )

        documentDirectoryUrl = documentDirectoryUrl.appendingPathComponent(self.defaultDirectory)

        // Check if the directory to be deleted already exists.
        if self.fileManager.fileExists(atPath: documentDirectoryUrl.path) {
            try self.fileManager.removeItem(atPath: documentDirectoryUrl.path)
        } else {
            throw CodableFilesError.directoryNotFound
        }
    }


    /// Change default directory name to a new one.
    /// - Parameter directoryName: Directory name to save and load objects.
    private func setDefaultDirectoryName(directoryName: String) {
        self.defaultDirectory = directoryName
    }


    /** Build the path with a given file name */
    private func path(with fileName: String) throws -> URL {
        var documentDirectoryUrl = try self.fileManager.url(
            for: .documentDirectory,
               in: .userDomainMask,
               appropriateFor: nil,
               create: false
        )

        documentDirectoryUrl = documentDirectoryUrl.appendingPathComponent(self.defaultDirectory)

        // Create the right directory if it does not exist, specified one or default one.
        if !self.fileManager.fileExists(atPath: documentDirectoryUrl.path) {
            try self.fileManager.createDirectory(at: documentDirectoryUrl, withIntermediateDirectories: false)
        }

        var fileURL = documentDirectoryUrl.appendingPathComponent(fileName)
        fileURL = fileURL.appendingPathExtension(SL.json.rawValue)

        return fileURL
    }

}




// MARK: - Computed Properties
extension CodableFiles {
    /// A string representation of the default directory name.
    private var defaultDirectoryName: String {
        return self.defaultDirectory
    }
}
