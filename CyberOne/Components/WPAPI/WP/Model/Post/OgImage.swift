//
//  OgImage.swift
//
//  Created by Taoufik on 11/11/2022
//  Copyright (c) . All rights reserved.
//

import Foundation



struct OgImage: Codable {

    enum CodingKeys: String, CodingKey {
        case url
        case height
        case type
        case width
    }

    var url: String?
    var height: Int?
    var type: String?
    var width: Int?


    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        url = try container.decodeIfPresent(String.self, forKey: .url)
        height = try container.decodeIfPresent(Int.self, forKey: .height)
        type = try container.decodeIfPresent(String.self, forKey: .type)
        width = try container.decodeIfPresent(Int.self, forKey: .width)
    }

}
