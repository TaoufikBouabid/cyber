//
//  YoastHeadJson.swift
//
//  Created by Taoufik on 11/11/2022
//  Copyright (c) . All rights reserved.
//

import Foundation

struct YoastHeadJson: Codable {
    
    enum CodingKeys: String, CodingKey {
        case ogLocale = "og_locale"
        case title
        case ogSiteName = "og_site_name"
        case articleModifiedTime = "article_modified_time"
        case ogDescription = "og_description"
        case twitterCard = "twitter_card"
        case ogType = "og_type"
        case descriptionValue = "description"
        case ogUrl = "og_url"
        case ogTitle = "og_title"
        case canonical
        case articlePublishedTime = "article_published_time"
        case ogImage = "og_image"
    }

    var ogLocale: String?
    var title: String?
    var ogSiteName: String?
    var articleModifiedTime: String?
    var ogDescription: String?
    var twitterCard: String?
    var ogType: String?
    var descriptionValue: String?
    var ogUrl: String?
    var ogTitle: String?
    var canonical: String?
    var articlePublishedTime: String?
    var ogImage: [OgImage]?



    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        ogLocale = try container.decodeIfPresent(String.self, forKey: .ogLocale)
        title = try container.decodeIfPresent(String.self, forKey: .title)
        ogSiteName = try container.decodeIfPresent(String.self, forKey: .ogSiteName)
        articleModifiedTime = try container.decodeIfPresent(String.self, forKey: .articleModifiedTime)
        ogDescription = try container.decodeIfPresent(String.self, forKey: .ogDescription)
        twitterCard = try container.decodeIfPresent(String.self, forKey: .twitterCard)
        ogType = try container.decodeIfPresent(String.self, forKey: .ogType)
        descriptionValue = try container.decodeIfPresent(String.self, forKey: .descriptionValue)
        ogUrl = try container.decodeIfPresent(String.self, forKey: .ogUrl)
        ogTitle = try container.decodeIfPresent(String.self, forKey: .ogTitle)
        canonical = try container.decodeIfPresent(String.self, forKey: .canonical)
        articlePublishedTime = try container.decodeIfPresent(String.self, forKey: .articlePublishedTime)
        ogImage = try container.decodeIfPresent([OgImage].self, forKey: .ogImage)
    }

}
