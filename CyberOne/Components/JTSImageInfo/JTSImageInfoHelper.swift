//
//  JTSImageInfoHelper.swift
//  CyberOne
//
//  Created by Taoufik on 12/11/2022.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import Foundation
import JustLog



final class JTSImageInfoHelper {

    static let instance = JTSImageInfoHelper()

    private init() {}


    func display(
        url: URL? = nil,
        image: UIImage? = nil,
        frame: CGRect,
        referenceView: UIView?,
        referenceContentMode: UIViewContentMode,
        referenceCornerRadius: CGFloat,
        topVC: UIViewController
    ) {
        let info = JTSImageInfo()
        if url != nil {
            info.imageURL = url
        }
        info.image = image
        info.referenceRect = frame
        info.referenceView = referenceView
        info.referenceContentMode = referenceContentMode
        info.referenceCornerRadius = referenceCornerRadius
        let imageViewer = JTSImageViewController(
            imageInfo: info,
            mode: .image,
            backgroundStyle: .scaled
        )
        imageViewer?.show(from: topVC, transition: .fromOriginalPosition)
    }

}
