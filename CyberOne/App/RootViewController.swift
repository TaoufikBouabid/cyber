//
//  RootViewController.swift
//  CyberOne
//
//  Created by Taoufik on 3/2/20.
//  Copyright © 2020 Cyber One. All rights reserved.
//

import UIKit
import ProgressHUD



class RootViewController: UITabBarController {

    private var isSubscriptionEnabled: Bool = false
    private var isReportsEnabled: Bool = false

    private var loadingTmpView: LoadingTmpView!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadingTmpView = LoadingTmpView(frame: self.view.frame)

        let imageView = UIImageView(frame: self.view.frame)
        imageView.image = UIImage(named: "splash_background")
        self.loadingTmpView.addSubview(imageView)

        self.loadingTmpView.backgroundColor = .white
        self.view.addSubview(self.loadingTmpView)
        ProgressHUD.show()

        self.loadData()
    }


    func loadData() {
        let configurations = FObject(path: "Configurations")
        configurations.fetchInBackground { (error) in
            ProgressHUD.dismiss()
            self.loadingTmpView.removeFromSuperview()
            if error == nil {
                self.isSubscriptionEnabled = configurations.values["isSubscriptionEnabled"] as? Bool ?? false
                self.isReportsEnabled = configurations.values["isReportsEnabled"] as? Bool ?? false
                self.updateTabBarItems()
            }
        }
    }


    func updateTabBarItems() {
        if !isReportsEnabled {
            let indexToRemove = 2
            if indexToRemove < viewControllers!.count {
                self.viewControllers?.remove(at: indexToRemove)
            }
        }

        if !isSubscriptionEnabled {
            self.viewControllers?.remove(at: self.viewControllers!.count - 1)
        }
    }
}


final class LoadingTmpView: UIView { }
