//
//  SubscriptionsViewController.swift
//  CyberOne
//
//  Created by Taoufik on 06/01/2019.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import UIKit
import iCarousel
import ProgressHUD
import FirebaseDatabase
import FirebaseAuth
import STPopup



final class SubscriptionsViewController: UIViewController {
    static let storyboardId = "SubscriptionsViewController"
    
    @IBOutlet weak var carousel: iCarousel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var data = [FSubscription]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.carousel.delegate = self
        self.carousel.dataSource = self
        self.carousel.type = .rotary
        self.carousel.currentItemIndex = 2
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadData()
    }


    private func loadData() {
        let services = FObject(path: "Subscriptions")
        ProgressHUD.show(nil, interaction: false)
        services.fetchInBackground { (error) in
            if error == nil {
                self.data.removeAll()
                ProgressHUD.dismiss()
                for service in services.values {
                    if let value = service.value as? [String: Any] {
                        self.data.append(FSubscription(dict: value))
                    }
                }
                self.data.sort(by: { $0.order < $1.order })
                self.pageControl.numberOfPages = self.data.count
                self.carousel.reloadData()
                self.collectionView.reloadData()
            } else {
                ProgressHUD.showError(error!.localizedDescription)
            }
        }
    }


    @IBAction private func subscribeAction(_ sender: UIButton) {
        let stb = UIStoryboard(name: "Report", bundle: nil)
        guard let reportVc =  stb.instantiateViewController(
            withIdentifier: ReportTableViewController.storyboardId
        ) as? ReportTableViewController else {
            return
        }
        reportVc.subscription = self.data[self.carousel.currentItemIndex]
        reportVc.delegate = self
        self.navigationController?.pushViewController(reportVc, animated: true)
    }

}


extension SubscriptionsViewController: iCarouselDataSource, iCarouselDelegate {

    func numberOfItems(in carousel: iCarousel) -> Int {
        return self.data.count
    }


    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        guard let cell = Bundle.main.loadNibNamed("SubscriptionViewCell", owner: self, options: nil)?[0] as? SubscriptionViewCell else {
            return UIView()
        }
        cell.image.sd_setImage(with: URL(string: self.data[index].background))
        return cell
    }


    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        switch option {
        case .arc:
            return CGFloat(Double.pi)
        case .radius:
            return value
        case .spacing:
            return value
        case .visibleItems:
            return 3
        default:
            return value
        }
    }


    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        self.pageControl.currentPage = carousel.currentItemIndex
        if self.carousel.currentItemIndex >= 0 && self.carousel.currentItemIndex < self.collectionView.numberOfItems(inSection: 0) {
            self.collectionView.scrollToItem(
                at: IndexPath(
                    item: self.carousel.currentItemIndex,
                    section: 0
                ),
                at: .left,
                animated: false
            )
        }
    }
}



extension SubscriptionsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SubscriptionDetailsCollectionViewCell.identifer, for: indexPath) as? SubscriptionDetailsCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.subscDesc.text = data[indexPath.item].desc
        cell.subscTitle.text = data[indexPath.item].title
        cell.subscDesc.font = h3Regular
        cell.subscTitle.font = h1Regular
        cell.subscTitle.numberOfLines = 0
        cell.subscTitle.minimumScaleFactor = 0.5
        cell.subscDesc.numberOfLines = 0
        cell.subscDesc.adjustsFontSizeToFitWidth = true
        cell.subscDesc.minimumScaleFactor = 0.5
        return cell
    }


    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
}



extension SubscriptionsViewController: ReportTableViewControllerDelegate {

    func didDismiss(
        _ report: FReport?,
        _ service: FService?,
        _ socialNetwork: FSocialNetworks?
    ) {
        if let report = report, let service = service {
            self.addChat(report, service)
            guard let stepsVc = StoryboardProvider.instance.storyboard(for: .main).instantiateViewController(
                withIdentifier: ReportStepsViewController.storyboardId
            ) as? ReportStepsViewController else {
                return
            }
            stepsVc.service = service
            stepsVc.report = report
            self.navigationController?.pushViewController(stepsVc, animated: true)
        }
    }


    func addChat(_ report: FReport,_ service: FService) {
        guard let currentUserId = FUser.currentUserId(), let reportId = report.objectId() else {
            return
        }
        let objectId = currentUserId + reportId
        let dateFormatter = DateFormatterProvider.dateFormatter
        let values: [String: Any] = [
            FChat.keys.lastMessage.rawValue : "",
            FChat.keys.lastMessageDateSent.rawValue: dateFormatter.string(from: Date()),
            FChat.keys.lastMessageUserId.rawValue: "",
            FChat.keys.objectId.rawValue: objectId,
            FChat.keys.opponentKey.rawValue: reportId,
            FChat.keys.typingIndicator.rawValue: [currentUserId : false, reportId : false],
            FChat.keys.unreadMessages.rawValue: [currentUserId : 0, reportId : 0],
            FChat.keys.isClosed.rawValue: false,
            "id": service.id
        ]
        FObject(
            path: FChat.path,
            subpath: objectId,
            dictionary: values
        ).saveInBackground()
    }

}
