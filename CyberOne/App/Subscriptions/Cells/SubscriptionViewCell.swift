//
//  SubscriptionViewCell.swift
//  CyberOne
//
//  Created by Taoufik on 06/01/2019.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import UIKit

class SubscriptionViewCell: UIView {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var holderView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        holderView.shadowWithCornder(raduis: 10, shadowColor: UIColor.black.cgColor, shadowOffset: CGSize(width: 0, height: 10), shadowOpacity: 1, shadowRadius: 5)
    }
}
