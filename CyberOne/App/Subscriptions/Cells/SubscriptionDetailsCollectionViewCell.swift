//
//  SubscriptionDetailsCollectionViewCell.swift
//  CyberOne
//
//  Created by Taoufik on 07/01/2019.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import UIKit

class SubscriptionDetailsCollectionViewCell: UICollectionViewCell {
    static let identifer = "SubscriptionDetailsCollectionViewCell"

    @IBOutlet weak var subscTitle: UILabel!
    @IBOutlet weak var subscDesc: UILabel!
    
}
