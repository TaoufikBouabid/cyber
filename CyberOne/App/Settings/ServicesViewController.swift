//
//  ServicesViewController.swift
//  CyberOne
//
//  Created by Taoufik on 11/15/19.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import UIKit
import ProgressHUD
import DZNEmptyDataSet
import STPopup



final class ServicesViewController: UIViewController {
    static let storyboardId = "ServicesViewController"
    
    @IBOutlet weak private var tableView: UITableView!

    private var loadingView: LoadingView!
    private var isProcessing = false
    private var data = [FReport]()


    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadData()
        self.setupLoadingView()
    }


    func loadData() {
        self.isProcessing = true
        let reports = FObject(path: "Reports")
        ProgressHUD.show(nil, interaction: false)
        reports.fetchInBackground { (error) in
            self.isProcessing = false
            if error == nil {
                self.data.removeAll()
                ProgressHUD.dismiss()
                for report in reports.values {
                    if let value = report.value as? [String: Any] {
                        self.data.append(FReport(value: value))
                    }
                }
                if FUser.currentUserType() == FUserType.normal.rawValue {
                    self.data = self.data.filter {
                        $0.userId == FUser.currentUserId() && !$0.subscription.isEmpty
                    }
                } else {
                    self.data = self.data.filter { !$0.subscription.isEmpty }
                }

                self.tableView.reloadEmptyDataSet()
                self.tableView.reloadData()
            } else if let error = error {
                ProgressHUD.showError(error.localizedDescription)
            }
        }
    }
}


extension ServicesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ServiceTableViewCell.identifier, for: indexPath) as? ServiceTableViewCell else {
            return UITableViewCell()
        }
        let report = data[indexPath.row]
        cell.serviceTitle.text = report.subscription
        cell.serviceDesc.text = report.remarks
        if let firstImage = report.attachments.components(separatedBy: ",").first, let url = URL(string: firstImage) {
            cell.serviceImage.sd_setImage(with: url)
        }
        return cell
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let stb = UIStoryboard(name: "Report", bundle: nil)
        guard let reportVc =  stb.instantiateViewController(withIdentifier: ReportTableViewController.storyboardId) as? ReportTableViewController else { return }
        reportVc.report = self.data[indexPath.row]
        reportVc.delegate = self
        self.navigationController?.pushViewController(reportVc, animated: true)
    }
}



extension ServicesViewController: ReportTableViewControllerDelegate {

    func didDismiss(_ report: FReport?, _ service: FService?, _ socialNetwork: FSocialNetworks?) {
        if let report = report {
            guard let stepsVc = StoryboardProvider.instance.storyboard(for: .main).instantiateViewController(
                withIdentifier: ReportStepsViewController.storyboardId
            ) as? ReportStepsViewController else { return }
            stepsVc.report = report
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                self.navigationController?.pushViewController(stepsVc, animated: true)
            })
        }
    }
}



//MARK:- DZNEmptyDataSet Delegates
extension ServicesViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate{
    
    func setupLoadingView() {
        if self.tableView != nil {
            self.tableView.emptyDataSetSource = self
            self.tableView.emptyDataSetDelegate = self
            self.loadingView = LoadingView.loadFromNibNamed(nibNamed: "LoadingView", bundle: nil)
            self.loadingView.loadingMessage = LOADING_SUBSCRIPTIONS_STR
            self.loadingView.noDataMessage = NO_SUBSCRIPTIONS_STR
            self.loadingView.image = nil
            self.loadingView.hideMsg = false
            self.loadingView.hideButton = true
            self.loadingView.frame = self.view.bounds
        }
    }


    func customView(forEmptyDataSet scrollView: UIScrollView!) -> UIView! {
        self.loadingView.isProcessing = isProcessing
        return self.loadingView
    }


    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return !self.isProcessing
    }

}

