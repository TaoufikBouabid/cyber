//
//  AboutAppViewController.swift
//  CyberOne
//
//  Created by Taoufik on 16/01/2019.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import UIKit
import WebKit



final class AboutAppViewController: UIViewController {
    
    
    @IBOutlet weak private var aboutAppWebView: WKWebView!


    override func viewDidLoad() {
        super.viewDidLoad()
        
        let languages = UserDefaults.standard.dictionary(forKey: "Languages")
        if let aboutApp = UserDefaults.standard.string(forKey: "aboutApp") {
            if let languages = languages,
               let langTitle = languages[aboutApp] as? [String: Any],
                let translatedAboutApp = langTitle[Languages.currentLanguage()] as? String {
                self.aboutAppWebView.loadHTMLString(translatedAboutApp, baseURL: nil)
            } else {
                self.aboutAppWebView.loadHTMLString(aboutApp, baseURL: nil)
            }
        }
    }

}
