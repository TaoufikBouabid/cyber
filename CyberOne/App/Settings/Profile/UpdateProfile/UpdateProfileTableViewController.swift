//
//  UpdateProfileTableViewController.swift
//  CyberOne
//
//  Created by Taoufik on 29/01/2019.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import UIKit
import ProgressHUD



final class UpdateProfileTableViewController: UITableViewController {
    static let storyboardId = "UpdateProfileTableViewController"

    @IBOutlet weak private var nameField: UITextField!
    @IBOutlet weak private var countryField: UITextField!


    override func viewDidLoad() {
        super.viewDidLoad()

        self.nameField.text = FUser.currentUserName()
        self.countryField.text = FUser.currentUserCountry()
        self.nameField.textAlignment = Languages.isRightToLeft() ? .right : .left
        self.countryField.textAlignment = Languages.isRightToLeft() ? .right : .left
    }


    @IBAction private func updateUser(_ sender: UIButton) {
        if !self.validateAction() { return }
        self.view.endEditing(true)
        if FUser.currentUserName() == self.nameField.text &&
            FUser.currentUserCountry() == countryField.text {
            self.navigationController?.popViewController(animated: true)
            return
        }

        guard let currentUserId = FUser.currentUserId() else {
            return
        }
        let object = FObject(
            path: FUser.path,
            subpath: currentUserId
        )
        object[FUsers.keys.country.rawValue] = self.countryField.text ?? ""
        object[FUsers.keys.name.rawValue] = self.nameField.text ?? ""
        ProgressHUD.show()
        object.updateInBackground { error in
            ProgressHUD.dismiss()
            if error == nil {
                FUser.currentUser?.fetchInBackground()
                self.navigationController?.popViewController(animated: true)
                showToast(
                    UPDATE_USER_SUCCESSFULLY_STR,
                    backgroundColor: .black,
                    textColor: .white,
                    font: h2Regular
                )
            } else if let error = error {
                showToast(
                    error.localizedDescription,
                    backgroundColor: toastColor,
                    textColor: .white,
                    font: h2Regular
                )
            }
        }
    }


    private func validateAction() -> Bool {
        if let nameText = self.nameField.text, nameText.isBlank {
            showToast(EMPTY_NAME_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular)
            return false
        }
        
        if let countryText = self.countryField.text, countryText.isBlank {
            showToast(EMPTY_COUNTRY_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular)
            return false
        }

        return true
    }

}
