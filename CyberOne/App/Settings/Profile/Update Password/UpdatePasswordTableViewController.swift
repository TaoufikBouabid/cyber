//
//  UpdateProfileTableViewController.swift
//  CyberOne
//
//  Created by Taoufik on 29/01/2019.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import UIKit
import ProgressHUD
import FirebaseAuth



final class UpdatePasswordTableViewController: UITableViewController {
    static let storyboardId = "UpdatePasswordTableViewController"

    @IBOutlet weak private var oldPasswordField: UITextField!
    @IBOutlet weak private var newPasswordField: UITextField!
    @IBOutlet weak private var confirmPasswordField: UITextField!


    override func viewDidLoad() {
        super.viewDidLoad()
        self.oldPasswordField.textAlignment = Languages.isRightToLeft() ? .right : .left
        self.newPasswordField.textAlignment = Languages.isRightToLeft() ? .right : .left
        self.confirmPasswordField.textAlignment = Languages.isRightToLeft() ? .right : .left
    }


    @IBAction private func updateUser(_ sender: UIButton) {
        self.view.endEditing(true)
        if !self.validateAction() { return }
        let user = Auth.auth().currentUser
        let credential = EmailAuthProvider.credential(
            withEmail: FUser.currentUserEmail() ?? "",
            password: oldPasswordField.text ?? ""
        )
        ProgressHUD.show()
        user?.reauthenticate(with: credential, completion: { result, error in
            if error == nil {
                let object = FObject(
                    path: FUser.path,
                    subpath: FUser.currentUserId()
                )
                object[FUsers.keys.password.rawValue] = self.newPasswordField.text ?? ""
                object[FUsers.keys.objectId.rawValue] = FUser.currentUserId()
                object.updateInBackground { error in
                    ProgressHUD.dismiss()
                    if error == nil {
                        FUser.currentUser?.fetchInBackground()
                        self.navigationController?.popViewController(animated: true)
                        showToast(
                            UPDATE_USER_SUCCESSFULLY_STR,
                            backgroundColor: .black,
                            textColor: .white,
                            font: h2Regular
                        )
                    } else if let error = error {
                        showToast(
                            error.localizedDescription,
                            backgroundColor: toastColor,
                            textColor: .white,
                            font: h2Regular
                        )
                    }
                }
            } else if let error = error {
                ProgressHUD.dismiss()
                showToast(
                    error.localizedDescription,
                    backgroundColor: toastColor,
                    textColor: .white,
                    font: h2Regular
                )
            }
        })
    }


    private func validateAction() -> Bool {
        if let oldPassword = self.oldPasswordField.text, oldPassword.isBlank {
            showToast(EMPTY_PASSWORD_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular)
            return false
        }

        if let newPassword = self.newPasswordField.text, newPassword.isBlank {
            showToast(EMPTY_FIELDS_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular)
            return false
        }

        if let confirmPassword = self.confirmPasswordField.text, confirmPassword.isBlank {
            showToast(EMPTY_FIELDS_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular)
            return false
        }

        if newPasswordField.text! != confirmPasswordField.text! {
            showToast(PASSWORD_NOT_CONFIRM_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular)
            return false
        }

        return true
    }

}
