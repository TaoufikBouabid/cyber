//
//  ServiceTableViewCell.swift
//  CyberOne
//
//  Created by Taoufik on 11/15/19.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import UIKit



final class ServiceTableViewCell: UITableViewCell {
    static let identifier = "ServiceTableViewCell"
    
    @IBOutlet weak var serviceImage: UIImageView!
    @IBOutlet weak var serviceTitle: UILabel!
    @IBOutlet weak var serviceDesc: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.shadowWithCornder(
            raduis: 5,
            shadowColor: UIColor.lightGray.cgColor,
            shadowOffset: CGSize(width: 2, height: 2),
            shadowOpacity: 0.7,
            shadowRadius: 2
        )

        self.serviceTitle.textAlignment = Languages.currentLanguage() == "ar" ? .right : .left
        self.serviceDesc.textAlignment = Languages.currentLanguage() == "ar" ? .right : .left
    }

}
