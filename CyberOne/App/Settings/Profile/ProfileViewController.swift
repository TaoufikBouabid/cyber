//
//  ProfileViewController.swift
//  CyberOne
//
//  Created by Taoufik on 22/11/2018.
//  Copyright © 2018 Cyber One. All rights reserved.
//

import UIKit
import MobileCoreServices
import JustLog



final class ProfileViewController: UIViewController {
    static let storyboardId = "ProfileViewController"
    
    @IBOutlet weak private var bottomView: UIView!
    @IBOutlet weak private var topView: UIView!
    @IBOutlet weak private var avatar: UIImageView!
    @IBOutlet var arrows: [UIImageView]!

    private var imagePickerController: UIImagePickerController!


    override func viewDidLoad() {
        super.viewDidLoad()
        self.arrows.forEach { $0.rotate() }
        if let avatar = FUser.currentUserAvatar() {
            self.avatar.shadowWithCornder(
                raduis: 0,
                shadowColor: UIColor.darkGray.cgColor,
                shadowOffset: CGSize(width: 1, height: 1),
                shadowOpacity: 0.8,
                shadowRadius: 3
            )
            self.avatar.sd_setImage(
                with: URL(string: avatar),
                placeholderImage: #imageLiteral(resourceName: "avatar")
            )
        }
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.topView.roundCorners(
            corners: [.topLeft, .topRight],
            cornerRadius: 10
        )
        self.bottomView.roundCorners(
            corners: [.bottomLeft, .bottomRight],
            cornerRadius: 10
        )
    }


    @IBAction private func avatarAction(_ sender: UIButton) {
        self.showTwoChoicesActionSheet(CHOOSE_IMG_STR, message: nil, CAMERA_STR, { action in
            self.chooseCamera()
        }, LIBRARY_STR, { (action) in
            self.chooseLibrary()
        }, CANCEL_STR , nil)
    }


    private func chooseCamera() {
        if UIImagePickerController.isSourceTypeAvailable(
            UIImagePickerControllerSourceType.camera
        ) {
            self.showImagePicker(source: .camera)
        }
    }


    private func chooseLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(
            UIImagePickerControllerSourceType.savedPhotosAlbum
        ) {
            self.showImagePicker(source: .savedPhotosAlbum)
        }
    }


    private func showImagePicker(source: UIImagePickerControllerSourceType) {
        self.imagePickerController = UIImagePickerController()
        self.imagePickerController.delegate = self
        self.imagePickerController.sourceType = source
        self.imagePickerController.allowsEditing = false
        self.imagePickerController.mediaTypes = [kUTTypeImage as String]
        self.imagePickerController.navigationBar.barTintColor = self.navigationController?.navigationBar.barTintColor
        self.imagePickerController.navigationBar.tintColor = self.navigationController?.navigationBar.tintColor
        self.imagePickerController.navigationBar.barStyle = .black
        self.imagePickerController.navigationBar.isTranslucent = false
        self.present(self.imagePickerController, animated: true, completion: nil)
    }

}



extension ProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]){
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let imageData = UIImageJPEGRepresentation(pickedImage, 0.4)
            dismiss(animated: true, completion: nil)
            guard let imageData = imageData else {
                Logger.shared.error("[ProfileViewController] didFinishPickingMediaWithInfo, cannot load imageData")
                return
            }
            UploadManager.upload(
                data: imageData,
                name: randomString(length: 10),
                ext: "png"
            ) { link, error in
                if error == nil, let link = link {
                    self.avatar.sd_setImage(
                        with: URL(string: link),
                        placeholderImage: #imageLiteral(resourceName: "avatar")
                    )
                    let object = FObject(
                        path: FUser.path,
                        subpath: FUser.currentUserId()
                    )
                    object[FUsers.keys.avatar.rawValue] = link
                    object.updateInBackground { (error) in
                        if error == nil {
                            FUser.currentUser?.fetchInBackground()
                        }
                    }
                }
            }
        }
    }


    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

}
