//
//  ReportTableViewCell.swift
//  CyberOne
//
//  Created by Taoufik on 22/11/2018.
//  Copyright © 2018 Cyber One. All rights reserved.
//

import UIKit
import FirebaseDatabase
class ReportTableViewCell: UITableViewCell {
    static let identifier = "ReportTableViewCell"
    
    
    @IBOutlet weak var verticalView: UIView!
    @IBOutlet weak var horizontalView: UIView!
    @IBOutlet weak var reportImage: UIImageView!
    @IBOutlet weak var reportTitle: UILabel!
    @IBOutlet weak var reportDetails: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    
    var report: FReport!
    var controller: ReportsViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.shadowWithCornder(raduis: 5, shadowColor: UIColor.lightGray.cgColor, shadowOffset: CGSize(width: 2, height: 2), shadowOpacity: 0.7, shadowRadius: 2)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func startConversation(_ sender: UIButton) {
        let chatId = self.controller.chatIds.filter { $0.contains(report.objectId) }.first

        guard let chatObjId = chatId else {
            self.controller.showAlertView(
                isOneButton: true,
                title: ERROR_OCCURED_STR,
                cancelButtonTitle: OK_STR,
                submitButtonTitle: OK_STR
            )
            return
        }
        guard let conversationVc = StoryboardProvider.instance.storyboard(for: .main).instantiateViewController(
            withIdentifier: ConversationChatViewController.storyboardId
        ) as? ConversationChatViewController else {
            return
        }
        conversationVc.chatId = chatObjId
        conversationVc.isClosed = report.isClosed
        conversationVc.opponentId = report.objectId
        self.controller.navigationController?.pushViewController(conversationVc, animated: true)
    }


    @IBAction func stopConversation(_ sender: UIButton) {
        guard let currentUserId = FUser.currentUserId(), let reportId = report.objectId() else {
            return
        }
        report.isClosed = !report.isClosed
        let updates: [String: Any] = [
            FChat.keys.isClosed.rawValue: self.report.isClosed
        ]
        Database.database()
            .reference()
            .child(FChat.path)
            .child(currentUserId + reportId)
            .updateChildValues(updates)
        Database.database()
            .reference()
            .child("Reports")
            .child(reportId)
            .updateChildValues(updates)
        if !report.isClosed {
            self.closeButton.setTitle(REPORT_IS_CLOSED, for: .normal)
        } else {
            self.closeButton.setTitle(REPORT_IS_OPENED, for: .normal)
        }
    }
    
}
