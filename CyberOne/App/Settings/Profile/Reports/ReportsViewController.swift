//
//  ReportsViewController.swift
//  CyberOne
//
//  Created by Taoufik on 22/11/2018.
//  Copyright © 2018 Cyber One. All rights reserved.
//

import UIKit
import ProgressHUD
import FirebaseDatabase
import STPopup
import JustLog


final class ReportsViewController: UIViewController {
    static let storyboardId = "ReportsViewController"

    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak private var searchBar: UISearchBar!

    private var data = [FReport]()
    private var filteredData = [FReport]()
    var chatIds = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.searchBar.delegate = self
        self.searchBar.placeholder = SEARCH_PHONE_NUMBER
        self.loadData()
        self.fetchChat()
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


    private func loadData() {
        let reports = FObject(path: "Reports")
        ProgressHUD.show(nil, interaction: false)
        reports.databaseReference().observeSingleEvent(of: DataEventType.value, with: { snapshot in
            ProgressHUD.dismiss()
            if let values = snapshot.value as? [String: Any] {
                reports.values = values
                for report in reports.values {
                    if let value = report.value as? [String: Any] {
                        self.data.append(FReport(value: value))
                    }
                }
                if FUser.currentUserType() == 0 {
                    self.data = self.data.filter({ $0.userId == FUser.currentUserId() })
                }
                let reportsArray = self.data.sorted(by: { obj1, obj2 -> Bool in
                    var d1 = Date()
                    var d2 = Date()
                    let dateFormatter = DateFormatterProvider.dateFormatter
                    if let dateTime = obj1[FObject.Const.updatedAt] as? Int {
                        d1 = Date(timeIntervalSince1970: Double(dateTime))
                    }
                    if let dateTime = obj2[FObject.Const.updatedAt] as? Int {
                        d2 = Date(timeIntervalSince1970: Double(dateTime))
                    }
                    return d2 < d1
                })
                self.data = reportsArray
                self.tableView.reloadData()

            }
        }) { error in
            ProgressHUD.showError(error.localizedDescription)
        }
    }


    private func fetchChat() {
        Database.database()
            .reference()
            .child(FChat.path)
            .observeSingleEvent(of: .value, with: { snapshot in
                if snapshot.exists() {
                    let enumerator = snapshot.children
                    while let rest = enumerator.nextObject() as? DataSnapshot {
                        if let child = rest.value as? [String: Any] {
                            let chat = FChat(chatDict: child)
                            self.chatIds.append(chat.objectId)
                        }
                    }
                }
            }) { error in
                Logger.shared.error("\(#function) error: \(error.localizedDescription)")
        }
    }

}



extension ReportsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let searchBarText = self.searchBar.text, searchBarText.count > 0 {
            return self.filteredData.count
        }
        return self.data.count
    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: ReportTableViewCell.identifier,
            for: indexPath
        ) as? ReportTableViewCell else {
            return UITableViewCell()
        }
        let report = (self.searchBar.text?.count ?? 0) > 0 ? self.filteredData[indexPath.row] : self.data[indexPath.row]

        if let image = report.values["photos"] as? String, let firstImage = image.components(separatedBy: ",").first {
            cell.reportImage.sd_setImage(with: URL(string: firstImage), placeholderImage: nil, options: [], completed: nil)
        }
        else {
            cell.reportImage.image = #imageLiteral(resourceName: "avatar")
        }
        if !report.isClosed {
            cell.closeButton.setTitle(REPORT_IS_CLOSED, for: .normal)
        }else{
            cell.closeButton.setTitle(REPORT_IS_OPENED, for: .normal)
        }
        cell.dateLabel.isHidden = true
        cell.reportTitle.text = report.fullName
        cell.reportDetails.text = report.notes.isEmpty ? report.remarks : report.notes
        cell.report = report
        cell.controller = self

        cell.horizontalView.isHidden = FUser.currentUserType() == 0
        cell.verticalView.isHidden = FUser.currentUserType() == 0
        cell.closeButton.isHidden = FUser.currentUserType() == 0
        cell.startButton.isHidden = FUser.currentUserType() == 0

        return cell
    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170
    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let report = self.searchBar.text?.count ?? 0 > 0 ? self.filteredData[indexPath.row] : self.data[indexPath.row]

        let stb = UIStoryboard.init(name: "Report", bundle: nil)
        let reportVc =  stb.instantiateViewController(withIdentifier: ReportTableViewController.storyboardId) as! ReportTableViewController
        reportVc.service = FService(dict: ["id" : report.serviceType])
        reportVc.socialNetwork = FSocialNetworks(dict: ["logo" : report.socialNetworkLogo])
        reportVc.report = report
        reportVc.delegate = self
        self.navigationController?.pushViewController(reportVc, animated: true)
    }
}



extension ReportsViewController: ReportTableViewControllerDelegate {
    func didDismiss(_ report: FReport?, _ service: FService?, _ socialNetwork: FSocialNetworks?) {
        if let report = report, let service = service, let socialNetwork = socialNetwork {
            guard let stepsVc = StoryboardProvider.instance.storyboard(for: .main).instantiateViewController(
                withIdentifier: ReportStepsViewController.storyboardId
            ) as? ReportStepsViewController else {
                return
            }
            stepsVc.service = service
            stepsVc.report = report
            stepsVc.socialNetwork = socialNetwork
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                self.navigationController?.pushViewController(stepsVc, animated: true)
            })
        }
    }
}



extension ReportsViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.filteredData = self.data.filter { (report) -> Bool in
            report.phoneNumber.lowercased().contains(searchText.lowercased())
        }
        self.tableView.reloadData()
    }
}
