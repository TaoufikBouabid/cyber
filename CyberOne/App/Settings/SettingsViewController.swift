//
//  SettingsViewController.swift
//  CyberOne
//
//  Created by Taoufik on 22/11/2018.
//  Copyright © 2018 Cyber One. All rights reserved.
//

import UIKit
import SwiftRater



final class SettingsViewController: UIViewController {
    static let storyboardId = "SettingsViewController"
    
    @IBOutlet weak private var logoImage: UIImageView!
    @IBOutlet private var arrows: [UIImageView]!

    @IBOutlet weak private var signoutLbl: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.logoImage.shadowWithCornder(
            raduis: 0,
            shadowColor: UIColor.darkGray.cgColor,
            shadowOffset: CGSize(width: 2, height: 2),
            shadowOpacity: 0.8,
            shadowRadius: 5
        )
        self.arrows.forEach { $0.rotate() }
        self.signoutLbl.text = SIGNOUT_STR
    }


    @IBAction private func shareAppAction(_ sender: UIButton) {
        if let shareText = UserDefaults.standard.string(forKey: "shareText"),
           let appStoreLink = UserDefaults.standard.string(forKey: "appStoreLink") {

            let firstActivityItem = shareText
            let secondActivityItem = URL(string: appStoreLink)

            let activityViewController = UIActivityViewController(
                activityItems: [firstActivityItem, secondActivityItem!],
                applicationActivities: nil
            )

            // Anything you want to exclude
            activityViewController.excludedActivityTypes = [
                UIActivityType.postToWeibo,
                UIActivityType.print,
                UIActivityType.assignToContact,
                UIActivityType.saveToCameraRoll,
                UIActivityType.addToReadingList,
                UIActivityType.postToFlickr,
                UIActivityType.postToVimeo,
                UIActivityType.postToTencentWeibo
            ]
            
            self.present(activityViewController, animated: true, completion: nil)
        }
    }


    @IBAction private func rateAppAction(_ sender: UIButton) {
        SwiftRater.rateApp()
    }


    @IBAction private func signout(_ sender: UIButton) {
        self.showAlertView(
            isOneButton: false,
            title: NSLocalizedString("Do you want to sign out", comment: ""),
            cancelButtonTitle: NO_STR,
            submitButtonTitle: YES_STR,
            doneHandler:  {
                UserDefaults.standard.removeObject(forKey: "isUserLoggedIn")
                UIApplication.shared.windows.first?.rootViewController =
                UINavigationController(rootViewController: StoryboardProvider.instance.storyboard(for: .main).instantiateViewController(withIdentifier: LoginTableViewController.holderVcId))
            }
        )
    }
}
