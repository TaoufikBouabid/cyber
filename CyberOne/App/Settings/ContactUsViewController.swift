//
//  ContactUsViewController.swift
//  CyberOne
//
//  Created by Taoufik on 16/01/2019.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import UIKit
import SZTextView
import ProgressHUD



final class ContactUsViewController: UIViewController {
    static let storyboardId = "ContactUsViewController"
    
    @IBOutlet weak private var nameField: UITextField!
    @IBOutlet weak private var emailField: UITextField!
    @IBOutlet weak private var subjectField: UITextField!
    @IBOutlet weak private var bodyField: SZTextView!


    override func viewDidLoad() {
        super.viewDidLoad()
        self.nameField.textAlignment = Languages.isRightToLeft() ? .right : .left
        self.emailField.textAlignment = Languages.isRightToLeft() ? .right : .left
        self.subjectField.textAlignment = Languages.isRightToLeft() ? .right : .left
        self.bodyField.textAlignment = Languages.isRightToLeft() ? .right : .left
        self.bodyField.placeholder = REMARKS_PLACEHOLDER_STR
    }

    
    @IBAction private func sendAction(_ sender: UIButton) {
        if !validationAction() { return }
        
        let contactUs = FObject(
            path: "ContactUs",
            subpath: randomString(length: 10)
        )

        contactUs.values = [
            "name": nameField.text ?? "",
            "email": emailField.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) ?? "",
            "subject": subjectField.text ?? "",
            "body": bodyField.text ?? "",
            "user": FUser.currentUserId() ?? ""
        ]

        ProgressHUD.show(nil, interaction: false)
        contactUs.saveInBackground { (error) in
            if error == nil {
                ProgressHUD.dismiss()
                self.navigationController?.popViewController(animated: true)
            } else {
                ProgressHUD.showError(error?.localizedDescription)
            }
        }
    }


    private func validationAction() -> Bool {
        if let body = self.bodyField.text, body.isBlank {
            showToast(EMPTY_BODY_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular)
            return false
        }
        
        if let subject = self.subjectField.text, subject.isBlank {
            showToast(EMPTY_SUBJECT_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular)
            return false
        }
        
        if let name = self.nameField.text, name.isBlank {
            showToast(EMPTY_NAME_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular)
            return false
        }
        
        if let email = self.emailField.text, email.isBlank {
            showToast(EMPTY_EMAIL_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular)
            return false
        }
        
        if let email = self.emailField.text, !email.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmail {
            showToast(VALID_EMAIL_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular)
            return false
        }
        
        return true
    }

}
