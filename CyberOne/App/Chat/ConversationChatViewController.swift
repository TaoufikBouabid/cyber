//
//  ConversationChatViewController.swift
//  CyberOne
//
//  Created by Taoufik on 09/09/2022.
//  Copyright © 2022 CyberOne All rights reserved.
//

import UIKit
import Firebase
import SDWebImage
import SZTextView
import MobileCoreServices
import AVKit
import AVFoundation
import JustLog



final class ConversationChatViewController: UIViewController {
    static let storyboardId = "ConversationChatViewController"

    @IBOutlet weak private var audioBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak private var recordButton: UIButton!
    @IBOutlet weak private var recordViewHolder: UIView!
    @IBOutlet weak private var indicator: UIActivityIndicatorView!
    @IBOutlet weak private var indicatorLabel: UILabel!
    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak private var textViewHeight: NSLayoutConstraint!
    @IBOutlet weak private var messageTextView: SZTextView!
    @IBOutlet weak private var bottomView: UIView!
    @IBOutlet weak private var imgButton: UIButton!
    @IBOutlet weak private var voiceButton: UIButton!
    @IBOutlet weak private var sendButton: UIButton!
    @IBOutlet weak private var firstAudioLabel: UILabel!
    @IBOutlet weak private var secondAudioLabel: UILabel!
    
    var fromProfil = false
    var messages = NSMutableArray()
    var sendingMessages = NSMutableArray()
    var chatSections = NSArray()
    var chatId: String!
    var currentUser: User!
    var opponentId = ""
    var reportId = ""
    var opponentUser: FReport!
    var typing = 0
    var seenMessageID = ""
    var imagePickerController: UIImagePickerController!
    var player: AVAudioPlayer!
    var sliderTimer: Timer!
    var playingIndexPath: IndexPath!
    var isClosed = false
    var audioRecorder: AVAudioRecorder!
    var audioPath: String?
    var firstTextAudio: String!
    var secondTextAudio: String!


    override func viewDidLoad() {
        super.viewDidLoad()

//        if let type = FUser.currentUserType(), type == 1 {
//            self.addCallAudioVideoItems()
//        }

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: UIImage(named: "reportIcon"),
            style: .plain,
            target: self,
            action: #selector(self.displayReport(_:))
        )

        self.setupAudioLabels()
        self.setupShadows()
        self.setupMessageTextView()
        self.setupTableViewRowHeight()
        self.setCurrentUserId()
        self.setRecordGesture()
    }


    @objc private func displayReport(_ sender: UIBarButtonItem) {
        guard let reportVc = StoryboardProvider.instance.storyboard(for: .report).instantiateViewController(withIdentifier: ReportTableViewController.storyboardId) as? ReportTableViewController else {
            return
        }
        reportVc.report = self.opponentUser
        reportVc.service = FService(dict: ["id" : self.opponentUser.serviceType])
        reportVc.socialNetwork = FSocialNetworks(dict: ["logo" : self.opponentUser.socialNetworkLogo])
        reportVc.delegate = self
        self.navigationController?.pushViewController(reportVc, animated: true)
    }


    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.setUnreadMessagesToNull()
        self.view.endEditing(true)
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isClosedProcess(self.isClosed)
        self.removeListener()
        self.AddListener()
        self.handleIsClosed()
        self.getMessages()
    }


    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.recordViewHolder.roundedCorner(corner: [.topLeft, .topRight], raduis: 5)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


    @IBAction func cameraAction(_ sender: UIButton) {
        self.showTwoChoicesActionSheet(CHOOSE_IMG_STR, message: nil, CAMERA_STR, { (action) in
            self.chooseCamera()
        }, LIBRARY_STR, { (action) in
            self.chooseLibrary()
        }, CANCEL_STR , nil)
    }

}


// MARK: - UIImagePickerControllerDelegate
extension ConversationChatViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]){

        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            Database.database()
                .reference()
                .child(FChat.path)
                .child(self.chatId)
                .child(FChat.keys.typingIndicator.rawValue)
                .child(FUser.currentUserId() ?? "")
                .setValue(false)
            
            let dateFormatter = DateFormatterProvider.gmtDateFormatter
            let stringDate = dateFormatter.string(from: Date())
            let imageData = UIImageJPEGRepresentation(pickedImage, 0.4)
            self.dismiss(animated: true, completion: nil)
            UploadManager.upload(
                data: imageData!,
                name: randomString(length: 10), ext: "png"
            ) { link, error in
                if error == nil {
                    let key: String = Database.database().reference().child(FMessage.path)
                        .child(self.chatId)
                        .childByAutoId()
                        .key!
                    let message: [String:Any] = [
                        FMessage.keys.message.rawValue: "",
                        FMessage.keys.type.rawValue: "image",
                        FMessage.keys.url.rawValue: link!,
                        FMessage.keys.dateSent.rawValue: stringDate,
                        FMessage.keys.senderId.rawValue: FUser.currentUserId() ?? ""
                    ]
                    self.sendingMessages.add(key)
                    Database.database().reference()
                        .child(FMessage.path)
                        .child(self.chatId)
                        .child(key)
                        .setValue(message) { error, ref in
                        if error == nil {
                            self.sendingMessages.remove(key)
                            self.tableView.reloadData()
                            var updates: [String: Any] = [
                                FChat.keys.lastMessage.rawValue: "image",
                                FChat.keys.lastMessageUserId.rawValue: FUser.currentUserId() ?? "",
                                FChat.keys.lastMessageDateSent.rawValue: message[FMessage.keys.dateSent.rawValue]!
                            ]
                            if self.messages.count == 1 {
                                updates[FChat.keys.unreadMessages.rawValue] = [
                                    self.opponentId: 0,
                                    FUser.currentUserId(): 0
                                ]
                            }
                            Database.database().reference()
                                .child(FChat.path)
                                .child(self.chatId)
                                .updateChildValues(updates)
                            Database.database().reference()
                                .child(FChat.path)
                                .child(self.chatId)
                                .child(FChat.keys.unreadMessages.rawValue)
                                .runTransactionBlock({ currentData -> TransactionResult in
                                if let unreadMessages: NSMutableDictionary = currentData.value as? NSMutableDictionary {
                                    if unreadMessages.count == 0 {
                                        return TransactionResult.success(withValue: currentData)
                                    }
                                    var nbr = 0
                                    if unreadMessages[self.opponentId] as? Int != nil {
                                        nbr = unreadMessages[self.opponentId] as! Int
                                    }else{
                                        unreadMessages.addEntries(from: [self.opponentId:0])
                                    }
                                    nbr += 1
                                    unreadMessages[self.opponentId] = nbr
                                    currentData.value = unreadMessages
                                }
                                return TransactionResult.success(withValue: currentData)
                            }, andCompletionBlock: { error, _, _ in
                                if let error = error {
                                    Logger.shared.error("\(#function) error: \(error.localizedDescription)")
                                }
                            })
                        } else if let error = error {
                            Logger.shared.error("\(#function) error: \(error.localizedDescription)")
                        }
                    }
                }
            }
        }
    }


    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }

}


// MARK: - LISTENERES
extension ConversationChatViewController {
    func getMessages() {
        Database.database().reference()
            .child(FMessage.path)
            .child(self.chatId)
            .observeSingleEvent(of: .value, with: { snapshot in
            self.indicator.stopAnimating()
            if snapshot.exists() {
                self.indicatorLabel.isHidden = true
                for child in snapshot.children {
                    let snap = child as! DataSnapshot
                    if let snapshotValue = snap.value as? NSMutableDictionary {
                        
                        var found = false
                        let msgLoop = self.messages.mutableCopy() as! [FMessage]
                        for msg: FMessage in msgLoop {
                            if msg.objectId == snap.key {
                                found = true
                                break;
                            }
                        }
                        if !found {
                            let msg: FMessage = FMessage().initWithDictionary(msgDict: snapshotValue.mutableCopy() as! [String:Any])
                            msg.objectId = snap.key
                            self.messages.insert(msg, at: 0)
                        }
                        
                    }
                }
                self.messages = NSMutableArray(array: self.messages.sorted(by: { (message1, message2) -> Bool in
                    return (message1 as! FMessage).dateSent.compare((message2 as! FMessage).dateSent) == .orderedAscending
                }))
                self.chatSections = self.sortedChatSectionsFromMessageArray(messageArray: self.messages.mutableCopy() as! [FMessage]) as NSArray
                self.tableView.reloadData()
                self.scrollContentAccordingToChatHistory()
                
            } else {
                self.indicatorLabel.text = EMPTY_CHAT_STR
            }
        }) { (error) in
            Logger.shared.error("\(#function) error: \(error.localizedDescription)")
        }
    }


    func scrollContentAccordingToChatHistory() {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            let numberOfSections = self.tableView.numberOfSections
            if numberOfSections > 0 {
                let numberOfRows = self.tableView.numberOfRows(inSection: numberOfSections - 1)
                if numberOfRows > 0 {
                    let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                    self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                }
            }
        }
    }


    func sortedChatSectionsFromMessageArray(messageArray: [FMessage]) -> NSMutableArray{
        let arrayOfSections = NSMutableArray()
        let sectionsDictionary = NSMutableDictionary()
        let dateNow = Date()
        
        for historyMessage in messageArray{
            let dateFormatter = DateFormatterProvider.dateFormatter
            if let date = dateFormatter.date(from: historyMessage.dateSent) {

                let key = ChatSection(date: date)
                    .daysBetweenDate(
                        fromDateTime: dateNow,
                        andDate: date
                    )

                var section = sectionsDictionary[key] as? ChatSection
                if section == nil {
                    section = ChatSection(date: date)
                    sectionsDictionary[key] = section
                    arrayOfSections.add(section!)
                }
                section?.add(historyMessage)
            }
        }
        return arrayOfSections
    }


    func removeListener() {
        // add remove listner
        Database.database()
            .reference()
            .child(FMessage.path)
            .child(self.chatId)
            .observe(.childRemoved, with: { snapshot in
            for message in self.messages.mutableCopy() as! [FMessage]{
                if message.objectId == snapshot.key{
                    let x = self.messages.index(of: message)
                    self.messages.removeObject(at: x)
                    for section in self.chatSections.mutableCopy() as! [ChatSection]{
                        for msg in section.messages{
                            if msg.objectId == snapshot.key{
                                let indexPath = IndexPath(row: section.messages.index(of:msg)!, section:self.chatSections.index(of:section))
                                let y = section.messages.index(of: msg)
                                section.messages.remove(at: y!)
                                
                                if indexPath.section == self.chatSections.count - 1 && indexPath.row == section.messages.count {
                                    self.tableView.deleteRows(at: [indexPath], with: .automatic)
                                    
                                    if section.messages.count == 0 {
                                        //let index = self.chatSections.index(of: section)
                                        let mutableArray: NSMutableArray = (self.chatSections.mutableCopy() as! NSMutableArray)
                                        mutableArray.remove(section)
                                        self.chatSections = mutableArray
                                        self.tableView.deleteSections(NSIndexSet(index: indexPath.section) as IndexSet, with: UITableViewRowAnimation.fade)
                                        
                                    }
                                    
                                    if section.messages.count > 0 {
                                        let message: FMessage = section.messages[section.messages.count-1]
                                        let updates: [String: Any] = [FChat.keys.lastMessage.rawValue: message.message, FChat.keys.lastMessageUserId.rawValue: message.senderId, FChat.keys.lastMessageDateSent.rawValue: message.dateSent, "chatHides": NSNull()]
                                        Database.database().reference().child(FChat.path)
                                            .child(self.chatId)
                                            .updateChildValues(updates)
                                    }else if self.chatSections.count > 0 {
                                        let chatSection: [ChatSection] = self.chatSections.mutableCopy() as! [ChatSection]
                                        let prevSection: ChatSection = chatSection[self.chatSections.count-1]
                                        let message: FMessage = prevSection.messages[prevSection.messages.count-1]
                                        let updates: [String: Any] = [
                                            FChat.keys.lastMessage.rawValue: message.message,
                                            FChat.keys.lastMessageUserId.rawValue: FUser.currentUserId() ?? "",
                                            FChat.keys.lastMessageDateSent.rawValue: message.dateSent,
                                            "chatHides": NSNull()
                                        ]
                                        Database.database().reference().child(FChat.path)
                                            .child(self.chatId)
                                            .updateChildValues(updates)
                                    }else if self.chatSections.count == 0{
                                        let updates: [String: Any] = [
                                            FChat.keys.lastMessageDateSent.rawValue: "",
                                            FChat.keys.lastMessage.rawValue: ""
                                        ]
                                        Database.database().reference().child(FChat.path)
                                            .child(self.chatId)
                                            .updateChildValues(updates)
                                    }
                                }else{
                                    self.tableView.deleteRows(at: [indexPath], with: .automatic)
                                    
                                    if section.messages.count == 0 {
                                        //let index = self.chatSections.index(of: section)
                                        let mutableArray: NSMutableArray = (self.chatSections.mutableCopy() as! NSMutableArray)
                                        mutableArray.remove(section)
                                        self.chatSections = mutableArray
                                        self.tableView.deleteSections(NSIndexSet(index: indexPath.section) as IndexSet, with: UITableViewRowAnimation.fade)
                                    }
                                }
                                break;
                            }
                        }
                    }
                    break;
                }
            }
        })
    }


    func AddListener(){
        //add listener messages added
        let query: DatabaseQuery = Database.database()
            .reference()
            .child(FMessage.path)
            .child(self.chatId)
            .queryOrdered(byChild:FMessage.keys.dateSent.rawValue)
        query.observe(.childAdded, with: { (snapshot) in
            if let snapshotValue = snapshot.value as? [String:Any]{
                self.indicator.stopAnimating()
                self.indicatorLabel.isHidden = true
                
                let message: FMessage = FMessage().initWithDictionary(msgDict: snapshotValue)
                message.objectId = snapshot.key
                var found: Bool = false
                let msgLoop = self.messages.mutableCopy() as! [FMessage]
                for msg: FMessage in msgLoop {
                    if msg.objectId == snapshot.key {
                        found = true
                        break;
                    }
                }
                if !found {
                    if snapshotValue[FMessage.keys.senderId.rawValue] != nil && snapshotValue[FMessage.keys.senderId.rawValue] as? String == FUser.currentUserId() {
                        //add sound here for sent
                    } else {
                        //add sound here for receive
                    }
                    self.messages.add(message)
                    self.chatSections = self.sortedChatSectionsFromMessageArray(messageArray: self.messages.mutableCopy() as! [FMessage]) as NSArray
                    
                    self.tableView.reloadData()
                    self.scrollContentAccordingToChatHistory()
                }
            }
        }, withCancel: { error in
            Logger.shared.error("\(#function) error: \(error.localizedDescription)")
        })
    }


    func handleIsClosed() {
        Database.database().reference().child(FChat.path)
            .child(self.chatId!)
            .observe(.childChanged, with: { (snapshot) in
                if snapshot.key == FChat.keys.isClosed.rawValue, let isClosed = snapshot.value as? Bool {
                self.isClosedProcess(isClosed)
            }
        })
    }


    func isClosedProcess(_ isClosed: Bool) {
        self.messageTextView.isUserInteractionEnabled = !isClosed
        self.messageTextView.text = isClosed ? CLOSED_STR : nil
        self.imgButton.isEnabled = !isClosed
        self.voiceButton.isEnabled = !isClosed
        self.sendButton.isEnabled = !isClosed
    }


    func setUnreadMessagesToNull() {
        Database.database()
            .reference()
            .child(FMessage.path)
            .child(self.chatId)
            .removeAllObservers()
        Database.database()
            .reference()
            .child(FChat.path)
            .child(self.chatId)
            .child(FChat.keys.typingIndicator.rawValue)
            .child(FUser.currentUserId() ?? "")
            .setValue(false)
        if FUser.currentUserType() == 1 {
            if reportId.isEmpty {
                return
            }
        }
        Database.database()
            .reference()
            .child(FChat.path)
            .child(self.chatId)
            .child(FChat.keys.unreadMessages.rawValue)
            .child(FUser.currentUserType() == 1 ? reportId : FUser.currentUserId() ?? "")
            .runTransactionBlock({ currentData -> TransactionResult in
            if currentData.value != nil {
                if let nbr = currentData.value as? Int {
                    if nbr == 0 {
                        return TransactionResult.success(withValue: currentData)
                    }
                    currentData.value = 0
                }
            }
            return TransactionResult.success(withValue: currentData)
            
        }, andCompletionBlock: { error, _, _ in
            if let error = error {
                Logger.shared.error("\(#function) error: \(error.localizedDescription)")
            }
        })
    }


    func deleteMessage(messageId: String){
        Database.database()
            .reference()
            .child(FMessage.path)
            .child(self.chatId)
            .child(messageId)
            .setValue(nil)
        self.tableView.reloadData()
    }
}



// MARK: - UITABLEVIEW DATASOURCE
extension ConversationChatViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view: headerSectionChat = headerSectionChat.instanceFromNib() as! headerSectionChat
        let chatSection: ChatSection? = self.chatSections[section] as? ChatSection
        view.headerTitle?.text = chatSection?.name
        view.headerHolder.layer.cornerRadius = view.headerHolder.frame.height/2
        view.headerHolder.clipsToBounds = true
        view.needsUpdateConstraints()
        view.layoutIfNeeded()
        return view
    }


    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }


    func numberOfSections(in tableView: UITableView) -> Int {
        return self.chatSections.count
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let chatSection: ChatSection? = self.chatSections[section] as? ChatSection
        return (chatSection?.messages.count)!
    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let chatSection = self.chatSections[indexPath.section] as? ChatSection else {
            return UITableViewCell()
        }

        let message = chatSection.messages[indexPath.row]
        if message.senderId == FUser.currentUserId() {
            if message.type == "text" {
                guard let cell = tableView.dequeueReusableCell(
                    withIdentifier: "MainMessageSentTableViewCell",
                    for: indexPath
                ) as? MainMessageSentTableViewCell else {
                    return UITableViewCell()
                }
                cell.bubbleImage.alpha = self.sendingMessages.contains(message.objectId) ? 0.7 : 1
                cell.handleParametersForCellWithMessage(message: message)
                cell.controller = self
                cell.msg = message
                cell.message.textAlignment = Languages.isRightToLeft() ? .left : .right
                return cell
            } else if message.type == "image" { // image message
                guard let cell = tableView.dequeueReusableCell(
                    withIdentifier: "ImageSentTableViewCell",
                    for: indexPath
                ) as? ImageSentTableViewCell else {
                    return UITableViewCell()
                }
                cell.setMessageDetails(msg: message)
                cell.controller = self
                return cell
            } else { // audio message
                guard let cell = tableView.dequeueReusableCell(
                    withIdentifier: "AudioSentTableViewCell",
                    for: indexPath
                ) as? AudioSentTableViewCell else {
                    return UITableViewCell()
                }
                cell.controller = self
                cell.playPause.tag = indexPath.row * 1000 + indexPath.section
                cell.playPause.addTarget(self, action: #selector(self.playAudio(_:)), for: .touchUpInside)
                cell.setMessageDetails(msg: message)
                if self.playingIndexPath != nil {
                    if self.playingIndexPath != indexPath {
                        cell.slider.value = 0
                    }
                }
                return cell
            }
        } else {
            if message.type == "text" {
                guard let cell = self.tableView.dequeueReusableCell(
                    withIdentifier: "MainMessageRecievedTableViewCell",
                    for: indexPath
                ) as? MainMessageRecievedTableViewCell else {
                    return UITableViewCell()
                }
                cell.handleParametersForCellWithMessage(message: message)
                cell.controller = self
                cell.msg = message
                cell.message.textAlignment = Languages.isRightToLeft() ? .right : .left
                if let avatar = self.opponentUser["photos"] as? String {
                    cell.profilImage?.sd_setImage(
                        with: URL(string: avatar),
                        placeholderImage: UIImage(named: "photos"),
                        options: SDWebImageOptions.progressiveDownload,
                        completed: { image, _, _, _ in
                            cell.profilImage.image = image != nil ? image : UIImage(named: "photos")
                        }
                    )
                } else {
                    cell.profilImage.image = UIImage(named: "avatar")
                }
                return cell
            } else if message.type == "image" {
                guard let cell = tableView.dequeueReusableCell(
                    withIdentifier: "ImageReceivedTableViewCell",
                    for: indexPath
                ) as? ImageReceivedTableViewCell else {
                    return UITableViewCell()
                }
                cell.setMessageDetails(msg: message)
                return cell
            } else {
                guard let cell = tableView.dequeueReusableCell(
                    withIdentifier: "AudioReceivedTableViewCell",
                    for: indexPath
                ) as? AudioReceivedTableViewCell else {
                    return UITableViewCell()
                }
                cell.controller = self
                cell.playPause.tag = indexPath.row * 1000 + indexPath.section
                cell.playPause.addTarget(
                    self,
                    action: #selector(self.playAudio(_:)),
                    for: .touchUpInside
                )
                cell.setMessageDetails(msg: message)
                return cell
            }
        }
    }
}



// MARK: - UITABLEVIEW & UITEXTVIEW DELEGATE
extension ConversationChatViewController: UITableViewDelegate, UITextViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let chatSection = self.chatSections[indexPath.section] as? ChatSection else {
            return 0
        }
        let message = chatSection.messages[indexPath.row]
        if message.type == "image" {
            return 230
        }
        return UITableViewAutomaticDimension
    }


    func textViewDidChange(_ textView: UITextView) {
        let sizeThatFitsTextView = textView.sizeThatFits(
            CGSize(
                width: self.view.frame.size.width - 70,
                height: 10000
            )
        )
        if sizeThatFitsTextView.height <= 30 {
            self.textViewHeight.constant = 30
            self.messageTextView.isScrollEnabled = false
        } else if sizeThatFitsTextView.height < 100 {
            self.textViewHeight.constant = sizeThatFitsTextView.height
            self.messageTextView.isScrollEnabled = false
        }else{
            self.textViewHeight.constant = 100
            self.messageTextView.isScrollEnabled = true
        }
    }


    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if self.typing == 0 {
            self.decrement()
        }
        self.typing = 20
        Database.database()
            .reference()
            .child(FChat.path)
            .child(self.chatId)
            .child(FChat.keys.typingIndicator.rawValue)
            .child(FUser.currentUserId() ?? "")
            .setValue(true)
        return true
    }


    func decrement() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {() -> Void in
            self.typing -= 1
            if self.typing > 0 {
                self.decrement()
            }
            else {
                Database.database().reference()
                    .child(FChat.path).child(self.chatId).child(FChat.keys.typingIndicator.rawValue)
                    .child(FUser.currentUserId() ?? "")
                    .setValue(false)
            }
        })
    }
}



// MARK: - SEND MESSAGE
extension ConversationChatViewController{
    
    @IBAction func sendMessage(_ sender: Any) {
        let trimmedString = self.messageTextView.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if trimmedString.isEmpty {
            self.messageTextView.shake()
            return
        }
        
        self.view.endEditing(true)
        self.indicatorLabel.isHidden = true
        self.messageTextView.text = ""
        self.textViewHeight.constant = 36
        self.view.needsUpdateConstraints()
        self.view.layoutIfNeeded()
        let dateFormatter = DateFormatterProvider.dateFormatterFromGMT
        let stringDate = dateFormatter.string(from: Date())

        let key = Database.database()
            .reference()
            .child(FMessage.path)
            .child(self.chatId)
            .childByAutoId()
            .key!
        
        let message: [String:Any] = [
            FMessage.keys.message.rawValue: trimmedString,
            FMessage.keys.type.rawValue: "text",
            FMessage.keys.dateSent.rawValue: stringDate,
            FMessage.keys.senderId.rawValue: FUser.currentUserId() ?? ""
        ]
        self.sendingMessages.add(key)
        Database.database()
            .reference()
            .child(FMessage.path)
            .child(self.chatId)
            .child(key)
            .setValue(message) { error, ref in
            if error == nil {
                self.sendingMessages.remove(key)
                self.tableView.reloadData()
                var updates: [String: Any] = [
                    FChat.keys.lastMessage.rawValue: message[FMessage.keys.message.rawValue]!,
                    FChat.keys.lastMessageUserId.rawValue: FUser.currentUserId() ?? "",
                    FChat.keys.lastMessageDateSent.rawValue: message[FMessage.keys.dateSent.rawValue]!
                ]

                if self.messages.count == 1 {
                    updates[FChat.keys.unreadMessages.rawValue] = [
                        self.opponentId: 0,
                        FUser.currentUserId(): 0
                    ]
                }
                Database.database()
                    .reference()
                    .child(FChat.path)
                    .child(self.chatId)
                    .updateChildValues(updates)
                Database.database()
                    .reference()
                    .child(FChat.path)
                    .child(self.chatId)
                    .child(FChat.keys.unreadMessages.rawValue).runTransactionBlock({ currentData -> TransactionResult in
                    if let unreadMessages: NSMutableDictionary = currentData.value as? NSMutableDictionary {
                        if unreadMessages.count == 0 {
                            return TransactionResult.success(withValue: currentData)
                        }
                        var nbr = 0
                        if unreadMessages[self.opponentId] as? Int != nil {
                            nbr = unreadMessages[self.opponentId] as! Int
                        }else{
                            unreadMessages.addEntries(from: [self.opponentId:0])
                        }
                        nbr += 1
                        unreadMessages[self.opponentId] = nbr
                        currentData.value = unreadMessages
                    }
                    return TransactionResult.success(withValue: currentData)
                }, andCompletionBlock: { error, _, _ in
                    if let error = error {
                        Logger.shared.error("\(#function) error: \(error.localizedDescription)")
                    }
                })
            } else if let error = error {
                Logger.shared.error("\(#function) error: \(error.localizedDescription)")
            }
        }
    }
}


// MARK: - AUDIO PLAYER ( PAUSE / PLAY )
extension ConversationChatViewController {
    
    @objc func playAudio(_ sender: UIButton) {
        let section = Int(sender.tag % 1000)
        let row = Int(sender.tag / 1000)
        let chatSection: ChatSection = (self.chatSections[section] as? ChatSection)!
        let message: FMessage = chatSection.messages[row]
        if playingIndexPath != nil {
            if section == playingIndexPath.section && row == playingIndexPath.row {
                setAudioPlayer(tableView.cellForRow(at: IndexPath.init(row: row, section: section))!)
                return;
            }
        }

        if message.url.count > 0 {
            if let audioUrl = URL(string: message.url) {
                do{
                    let audioData = try Data(contentsOf: audioUrl)
                    do {
                        self.player = try AVAudioPlayer(data: audioData)
                        self.player.delegate = self
                    } catch {
                    }
                }catch{
                }
            }
        }
        self.pauseAudioIfPlaying(IndexPath.init(row: row, section: section))
        self.playingIndexPath = IndexPath.init(row: row, section: section)
        self.setAudioPlayer(tableView.cellForRow(at: IndexPath.init(row: row, section: section))!)
    }


    func pauseAudioIfPlaying(_ indexPath: IndexPath) {
        if self.playingIndexPath != nil {
            if self.playingIndexPath.section == indexPath.section {
                if self.playingIndexPath.row != indexPath.row, self.player != nil {
                    self.player.pause()
                    self.sliderTimer.invalidate()
                    if let cell = self.tableView.cellForRow(at: self.playingIndexPath) as? AudioSentTableViewCell {
                        //Pause
                        cell.playPause.isSelected = false
                    }else if let cell =  tableView.cellForRow(at: self.playingIndexPath) as? AudioReceivedTableViewCell {
                        //Pause
                        cell.playPause.isSelected = false
                    }
                }
            }
        }
    }


    func setAudioPlayer(_ cell: UITableViewCell) {
        if let cell = cell as? AudioSentTableViewCell {
            cell.playPause.isSelected = !cell.playPause.isSelected
            if self.player != nil {
                if self.player.isPlaying {
                    // Pause
                    self.player.pause()
                    self.sliderTimer.invalidate()
                }else{
                    // Play
                    cell.slider.maximumValue = Float(player.duration)
                    self.player.prepareToPlay()
                    self.player.play()
                    self.sliderTimer = Timer(timeInterval: 0.1, target: self, selector: #selector(updateDurationLabel), userInfo: nil, repeats: true)
                    RunLoop.main.add(self.sliderTimer, forMode: .commonModes)
                }
            }
        } else if let cell = cell as? AudioReceivedTableViewCell {
            cell.playPause.isSelected = !cell.playPause.isSelected
            if self.player != nil {
                if self.player.isPlaying {
                    //Pause
                    self.player.pause()
                    self.sliderTimer.invalidate()
                } else {
                    //Play
                    cell.slider.maximumValue = Float(player.duration)
                    self.player.prepareToPlay()
                    self.player.play()
                    self.sliderTimer = Timer(timeInterval: 0.1, target: self, selector: #selector(updateDurationLabel), userInfo: nil, repeats: true)
                    RunLoop.main.add(self.sliderTimer, forMode: .commonModes)
                }
            }
        }
    }
    
    
    @objc func updateDurationLabel() {
        if let cell = tableView.cellForRow(at: playingIndexPath) as? AudioSentTableViewCell {
            let time: Float = Float(player!.currentTime)
            let min: Int = Int(time/60)
            let sec: Int = Int(time.truncatingRemainder(dividingBy: 60))
            cell.duration.text = String(format: "%02li:%02li", Int(min), Int(sec))
            cell.slider.value = Float((player?.currentTime)!)
        }else if let cell = tableView.cellForRow(at: playingIndexPath) as? AudioReceivedTableViewCell {
            let time: Float = Float(player!.currentTime)
            let min: Int = Int(time/60)
            let sec: Int = Int(time.truncatingRemainder(dividingBy: 60))
            cell.duration.text = String(format: "%02li:%02li", Int(min), Int(sec))
            cell.slider.value = Float((player?.currentTime)!)
        }
    }
}



// MARK: - AUDIO PLAYER DELEGATE
extension ConversationChatViewController: AVAudioPlayerDelegate {

    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if let playingIndexPath = playingIndexPath, let cell = tableView.cellForRow(at: playingIndexPath) {
            guard let chatSection = self.chatSections[playingIndexPath.section] as? ChatSection else {
                return
            }
            let message = chatSection.messages[playingIndexPath.row]
            if let cell = cell as? AudioReceivedTableViewCell {
                sliderTimer.invalidate()
                cell.playPause.isSelected = false
                cell.duration.text = message.audioDuration
            }else if let cell = cell as? AudioSentTableViewCell {
                sliderTimer.invalidate()
                cell.playPause.isSelected = false
                cell.duration.text = message.audioDuration
            }
        } else {
            self.recordButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
            self.recordButton.setImage(UIImage(named: "play"), for: .normal)
        }
    }
}


// MARK: - SEND AUDIO
extension ConversationChatViewController {
    
    func didSendAudio(audioPath: String) {
        self.sendAudioMessage(audio: audioPath)
    }
    
    func sendAudioMessage(audio: String) {
        if let dataAudio = NSData(contentsOfFile: audio) as Data? {

            Database.database().reference()
                .child(FChat.path)
                .child(self.chatId)
                .child(FChat.keys.typingIndicator.rawValue)
                .child(FUser.currentUserId() ?? "")
                .setValue(false)

            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
            dateFormatter.locale = Locale(identifier: "en_US")
            let stringDate = dateFormatter.string(from: Date())

            let currentPlayer: AVAudioPlayer = try! AVAudioPlayer(data: dataAudio)
            let currentTime: Float = Float(currentPlayer.duration)

            UploadManager.upload(data: dataAudio, name: randomString(length: 10), ext: "mp3") { (link, error) in

                if error == nil {

                    UIView.animate(withDuration: 0.3, animations: {
                        self.audioBottomConstraint.constant = -400
                        self.view.layoutIfNeeded()
                        if let path = self.audioPath, let audioURL = URL(string: path) {
                            try? FileManager.default.removeItem(at: audioURL)
                            self.audioPath = nil
                            self.player = nil
                            self.audioRecorder = nil
                            self.recordButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                            self.recordButton.setImage(UIImage(named: "microphone"), for: .normal)
                            self.firstAudioLabel.text = self.firstTextAudio
                            self.secondAudioLabel.text = self.secondTextAudio
                        }
                    })

                    let key: String = Database.database().reference().child(FMessage.path).child(self.chatId).childByAutoId().key!
                    
                    let message: [String:Any] = [
                        FMessage.keys.message.rawValue: "",
                        FMessage.keys.type.rawValue: FMessage.keys.audio.rawValue,
                        FMessage.keys.url.rawValue: link!,
                        FMessage.keys.dateSent.rawValue: stringDate,
                        FMessage.keys.senderId.rawValue: FUser.currentUserId() ?? "",
                        "audio_duration": String(format: "%02i:%02i", Int(currentTime / 60),Int(currentTime.truncatingRemainder(dividingBy: 60)))
                    ]
                    self.sendingMessages.add(key)
                    Database.database().reference().child(FMessage.path).child(self.chatId).child(key).setValue(message) { (error, ref) in
                        if error == nil {
                            self.sendingMessages.remove(key)
                            self.tableView.reloadData()
                            var updates: [String: Any] = [
                                FChat.keys.lastMessage.rawValue: FMessage.keys.audio.rawValue,
                                FChat.keys.lastMessageUserId.rawValue: FUser.currentUserId() ?? "",
                                FChat.keys.lastMessageDateSent.rawValue: message[FMessage.keys.dateSent.rawValue]!
                            ]
                            
                            if self.messages.count == 1 {
                                updates[FChat.keys.unreadMessages.rawValue] = [
                                    self.opponentId: 0,
                                    FUser.currentUserId(): 0
                                ]
                            }
                            Database.database()
                                .reference()
                                .child(FChat.path)
                                .child(self.chatId)
                                .updateChildValues(updates)
                            Database.database()
                                .reference()
                                .child(FChat.path)
                                .child(self.chatId)
                                .child(FChat.keys.unreadMessages.rawValue)
                                .runTransactionBlock({ (currentData) -> TransactionResult in
                                if let unreadMessages: NSMutableDictionary = currentData.value as? NSMutableDictionary {
                                    if unreadMessages.count == 0 {
                                        return TransactionResult.success(withValue: currentData)
                                    }
                                    var nbr = 0
                                    if unreadMessages[self.opponentId] as? Int != nil {
                                        nbr = unreadMessages[self.opponentId] as! Int
                                    }else{
                                        unreadMessages.addEntries(from: [self.opponentId:0])
                                    }
                                    nbr += 1
                                    unreadMessages[self.opponentId] = nbr
                                    currentData.value = unreadMessages
                                }
                                return TransactionResult.success(withValue: currentData)
                            }, andCompletionBlock: { error, _, _ in
                                if let error = error {
                                    Logger.shared.error("\(#function) error: \(error.localizedDescription)")
                                }
                            })
                        } else if let error = error {
                            Logger.shared.error("\(#function) error: \(error.localizedDescription)")
                        }
                    }
                }
            }
        }
    }


    func duration(path: String) -> Int {
        let asset = AVURLAsset(url: URL(fileURLWithPath: path), options: nil)
        return Int(round(CMTimeGetSeconds(asset.duration)))
    }
}



// MARK: - AUDIO DELEGATE
extension ConversationChatViewController: AVAudioRecorderDelegate {

    func recordAudio() {
        let dirPath = NSSearchPathForDirectoriesInDomains(.documentDirectory,.userDomainMask, true)[0] as String
        let recordingName = "recordedVoice.wav"
        let pathArray = [dirPath, recordingName]
        let filePath = URL(string: pathArray.joined(separator: "/"))
        
        let session = AVAudioSession.sharedInstance()
        try! session.setCategory(AVAudioSessionCategoryPlayAndRecord, with:AVAudioSessionCategoryOptions.defaultToSpeaker)
        
        try! self.audioRecorder = AVAudioRecorder(url: filePath!, settings: [:])
        
        self.audioRecorder.delegate = self
        self.audioRecorder.isMeteringEnabled = true
        self.audioRecorder.prepareToRecord()
        self.audioRecorder.record()
    }


    func stopRecording() {
        self.audioRecorder.stop()
        let audioSession = AVAudioSession.sharedInstance()
        try! audioSession.setActive(false)
    }


    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if flag {
            self.audioPath = self.audioRecorder.url.path
        } else {
            self.showAlertView(isOneButton: true, title: "Ops... Some problem happened recording.", cancelButtonTitle: OK_STR, submitButtonTitle: OK_STR)
        }
    }
}



// MARK: - AUDIO SETUP AND ACTION
extension ConversationChatViewController {

    func setupAudioLabels() {
        self.firstTextAudio = self.firstAudioLabel.text
        self.secondTextAudio = self.secondAudioLabel.text
    }


    @IBAction func voiceAction(_ sender: UIButton) {
        if self.audioBottomConstraint.constant < 0 {
            UIView.animate(withDuration: 0.3) {
                self.audioBottomConstraint.constant = 0
                self.view.layoutIfNeeded()
            }
        } else {
            UIView.animate(withDuration: 0.3, animations: {
                self.audioBottomConstraint.constant = -400
                self.view.layoutIfNeeded()
                if let path = self.audioPath, let audioURL = URL(string: path) {
                    try? FileManager.default.removeItem(at: audioURL)
                    self.audioPath = nil
                    self.player = nil
                    self.audioRecorder = nil
                    self.recordButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                    self.recordButton.setImage(UIImage(named: "microphone"), for: .normal)
                    self.firstAudioLabel.text = self.firstTextAudio
                    self.secondAudioLabel.text = self.secondTextAudio
                }
            })
        }
    }


    @objc func recordSimpleTap(_ tap: UITapGestureRecognizer) {
        if audioPath == nil {
            self.recordButton.shake()
        } else {
            self.playAudio()
        }
    }


    func playAudio() {
        guard let urlString = self.audioPath else { return }
        guard let url = URL(string: urlString) else { return }

        if let player = self.player {
            if player.isPlaying {
                self.recordButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
                self.recordButton.setImage(UIImage(named:"play"), for: .normal)
                player.stop()
            } else {
                self.recordButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                self.recordButton.setImage(UIImage(named:"pause"), for: .normal)
                player.play()
            }
        } else {
            do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, mode: AVAudioSessionModeDefault)
                try AVAudioSession.sharedInstance().setActive(true)
                
                
                /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
                self.player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.m4a.rawValue)
                self.player?.delegate = self
                /* iOS 10 and earlier require the following line:
                 player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */
                
                guard let player = player else { return }
                
                DispatchQueue.main.asyncAfter(deadline: .now()+0.3, execute: {
                    player.play()
                    self.recordButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                    self.recordButton.setImage(UIImage(named:"pause"), for: .normal)
                })
                
            } catch let error {
                Logger.shared.error("\(#function) error: \(error.localizedDescription)")
            }
        }
    }


    @objc func recordLongTap(_ tap: UILongPressGestureRecognizer) {
        if audioPath == nil {
            if tap.state == .began {
                self.recordButton.alpha = 0.7
                self.recordAudio()
            }
            if tap.state == .ended {
                self.recordButton.alpha = 1
                self.stopRecording()
                self.firstAudioLabel.text = HOLD_STR
                self.secondAudioLabel.text = nil
                self.recordButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
                self.recordButton.setImage(UIImage(named: "play"), for: .normal)
            }
        } else {
            self.showAlertView(isOneButton: false, title: SEND_AUDIO_STR, cancelButtonTitle: CANCEL_STR, submitButtonTitle: OK_STR, cancelHandler: {
                
                UIView.animate(withDuration: 0.3, animations: {
                    self.audioBottomConstraint.constant = -400
                    self.view.layoutIfNeeded()
                })
            }, doneHandler: {
                UIView.animate(withDuration: 0.3, animations: {
                    self.audioBottomConstraint.constant = -400
                    self.view.layoutIfNeeded()
                })
                self.didSendAudio(audioPath: self.audioPath!)
            })
        }
    }

}


// MARK: - CALL AUDIO AND VIDEO SETUP AND ACTION
extension ConversationChatViewController {

    func addCallAudioVideoItems() {
        let audio = UIImage(named: "callAudio")
        let audioButton = UIButton(type: .custom)
        audioButton.frame = CGRect(x: 0, y: 0, width: audio?.size.width ?? 0.0, height: audio?.size.height ?? 0.0)
        audioButton.addTarget(self, action: #selector(self.callAudio(_:)), for: .touchDown)
        audioButton.setBackgroundImage(audio, for: .normal)

        let video = UIImage(named: "callVideo")
        let videoButton = UIButton(type: .custom)
        videoButton.frame = CGRect(x: 0, y: 0, width: video?.size.width ?? 0.0, height: video?.size.height ?? 0.0)
        videoButton.addTarget(self, action: #selector(self.callVideo(_:)), for: .touchDown)
        videoButton.setBackgroundImage(video, for: .normal)

        let stackview = UIStackView.init(arrangedSubviews: [videoButton, audioButton])
        stackview.distribution = .equalSpacing
        stackview.axis = .horizontal
        stackview.alignment = .center
        stackview.spacing = 20

        // Make BarButton Item
        let navRightButton = UIBarButtonItem(customView: stackview)

        navigationItem.rightBarButtonItem = navRightButton
    }


    @objc func callAudio(_ sender: Any) {
        print("call audio")
    }


    @objc func callVideo(_ sender: Any) {
        print("call video")
    }

}


// MARK: - Choose camera & library
extension ConversationChatViewController {

    func chooseCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            self.imagePickerController = UIImagePickerController()
            self.imagePickerController.delegate = self
            self.imagePickerController.sourceType = UIImagePickerControllerSourceType.camera;
            self.imagePickerController.allowsEditing = false
            self.imagePickerController.mediaTypes = [kUTTypeImage as String]
            self.imagePickerController.navigationBar.barTintColor = self.navigationController?.navigationBar.barTintColor
            self.imagePickerController.navigationBar.tintColor = self.navigationController?.navigationBar.tintColor
            self.imagePickerController.navigationBar.barStyle = UIBarStyle.black
            self.imagePickerController.navigationBar.isTranslucent = false
            self.present(self.imagePickerController, animated: true, completion: nil)
        }
    }
    
    func chooseLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum) {
            self.imagePickerController = UIImagePickerController()
            self.imagePickerController.delegate = self
            self.imagePickerController.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum;
            self.imagePickerController.allowsEditing = false
            self.imagePickerController.mediaTypes = [kUTTypeImage as String]
            self.imagePickerController.navigationBar.barTintColor = self.navigationController?.navigationBar.barTintColor
            self.imagePickerController.navigationBar.tintColor = self.navigationController?.navigationBar.tintColor
            self.imagePickerController.navigationBar.barStyle = UIBarStyle.black
            self.imagePickerController.navigationBar.isTranslucent = false
            self.present(self.imagePickerController, animated: true, completion: nil)
        }
    }

}


extension ConversationChatViewController {

    func setupShadows() {
        self.bottomView.shadowWithCornder(
            raduis: 5,
            shadowColor: UIColor.lightGray.cgColor,
            shadowOffset: CGSize(width: 2, height: 2),
            shadowOpacity: 1,
            shadowRadius: 5
        )
        self.recordViewHolder.shadowWithCornder(
            raduis: 5,
            shadowColor: UIColor.lightGray.cgColor,
            shadowOffset: CGSize(width: 5, height: 5),
            shadowOpacity: 1,
            shadowRadius: 5
        )
        self.recordButton.shadowWithCornder(
            raduis: 50,
            shadowColor: UIColor.black.withAlphaComponent(0.9).cgColor,
            shadowOffset: CGSize(width: 2, height: 2),
            shadowOpacity: 1,
            shadowRadius: 5
        )
    }


    func setupMessageTextView() {
        self.messageTextView.delegate = self
        self.messageTextView.layer.cornerRadius = 5
        self.messageTextView.placeholder = ENTER_MESSAGE_STR
        self.messageTextView.textAlignment = Languages.isRightToLeft() ? .right : .left
    }


    func setupTableViewRowHeight() {
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 100
    }


    func setCurrentUserId() {
        if Auth.auth().currentUser != nil {
            self.currentUser = Auth.auth().currentUser
        }
    }


    func setRecordGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.recordSimpleTap(_:)))
        recordButton.addGestureRecognizer(tapGesture)
        
        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(self.recordLongTap(_:)))
        lpgr.minimumPressDuration = 0.5
        lpgr.delaysTouchesBegan = true
        recordButton.addGestureRecognizer(lpgr)
    }
}


extension ConversationChatViewController: ReportTableViewControllerDelegate {
    func didDismiss(_ report: FReport?, _ service: FService?, _ socialNetwork: FSocialNetworks?) {
        if let report = report, let service = service, let socialNetwork = socialNetwork {
            guard let stepsVc = StoryboardProvider.instance.storyboard(for: .main).instantiateViewController(
                withIdentifier: ReportStepsViewController.storyboardId
            ) as? ReportStepsViewController else {
                return
            }
            stepsVc.service = service
            stepsVc.report = report
            stepsVc.socialNetwork = socialNetwork
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                self.navigationController?.pushViewController(stepsVc, animated: true)
            })
        }
    }
}
