//
//  ChatSection.swift
//  maktoobProject
//
//  Created by Taoufik on 24/01/2022.
//  Copyright © 2022 CyberOne All rights reserved.
//

import Foundation
class ChatSection: NSObject {
    var identifier: Int = 0
    var name: String = ""
    var messages = [FMessage]()
override var description: String{ return "<ChatSection; identifier: \(identifier); name; \(name);messages: \(messages) >" }
    init(date: Date) {
        super.init()
        self.messages = [FMessage]()
        self.name = self.formattedStringFromDate(date: date)
        self.identifier = self.daysBetweenDate(fromDateTime: Date(), andDate: date)
    }

    func add(_ message: FMessage) {
        self.messages.append(message)
    }

    func daysBetweenDate(fromDateTime: Date, andDate toDateTime: Date) -> Int {
        var fromDate: Date?
        var toDate: Date?
        let calendar: NSCalendar = NSCalendar.current as NSCalendar
        fromDate = calendar.startOfDay(for: toDateTime)
        toDate = calendar.startOfDay(for: fromDateTime)
        let flags = NSCalendar.Unit.day
        let components = calendar.components(flags, from: fromDate!, to: toDate!, options: [])
        return components.day!
    }

    func formattedStringFromDate(date:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM"
        dateFormatter.locale = Locale.current
        var formattedString: String? = nil
        let calendar: NSCalendar = NSCalendar.current as NSCalendar
        let components: NSDateComponents = calendar.components([.day, .month, .year] , from: date) as NSDateComponents
        let currentComponents: NSDateComponents = calendar.components([.day, .month, .year] , from: Date()) as NSDateComponents
        if components.day == currentComponents.day && components.month == currentComponents.month && components.year == currentComponents.year {
            formattedString = TODAY_STR
        }else if components.day == currentComponents.day-1 && components.month == currentComponents.month && components.year == currentComponents.year {
            formattedString = YESTERDAY_STR
        }else if components.year == currentComponents.year {
            formattedString = "\(Int(components.day)) \(dateFormatter.string(from: date))"
        }else{
            formattedString = "\(Int(components.day)) \(dateFormatter.string(from: date)) \(Int(components.year))"

        }
        return formattedString!
       
    }

}
