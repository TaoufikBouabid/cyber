//
//  InboxCell.swift
//  Tawsil
//
//  Created by Taoufik on 09/09/2022.
//  Copyright © 2022 CyberOne All rights reserved.
//

import UIKit



final class InboxCell: UITableViewCell {

    @IBOutlet weak var numbrtUnreadMsg: UILabel!
    @IBOutlet weak var hour: UILabel!
    @IBOutlet weak var content: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var gradientView: GradientView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.numbrtUnreadMsg.layer.cornerRadius = self.numbrtUnreadMsg.frame.height / 2
        self.numbrtUnreadMsg.clipsToBounds = true
        self.photo.contentMode = .scaleAspectFit
    }

}
