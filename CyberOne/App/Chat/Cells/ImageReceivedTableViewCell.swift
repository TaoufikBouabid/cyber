//
//  ImageReceivedTableViewCell.swift
//  Woin
//
//  Created by macbook on 20/09/2022.
//  Copyright © 2022 macbook. All rights reserved.
//

import UIKit
import Toaster
import SDWebImage



final class ImageReceivedTableViewCell: UITableViewCell {
    
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var messageImage: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    
    var message: FMessage!
    var chat: FChat!
    var user: FUser!
    var fullImage : UIImage!


    override func awakeFromNib() {
        super.awakeFromNib()
        
        let mask : CALayer = CALayer()
        mask.contents = Languages.isRightToLeft() ? UIImage(named: "recuBigRTL")?.cgImage : UIImage(named: "recuBig")?.cgImage
        mask.frame = CGRect.init(x: 0, y: 0, width: (messageImage.frame.size.width), height: (messageImage.frame.size.height))
        self.messageImage.layer.mask = mask
        self.messageImage.layer.masksToBounds = true
        
        self.dateLabel.layer.shadowColor = UIColor.black.cgColor
        self.dateLabel.layer.shadowOffset = CGSize(width:1.0,height:1.0)
        self.dateLabel.layer.shadowOpacity = 0.1
        self.dateLabel.layer.shadowRadius = 1.0
    }


    @objc func tapMessageImage(sender: UITapGestureRecognizer) {
        
        // open the image
        if self.fullImage != nil, let topVC = UIApplication.topViewController() {
            JTSImageInfoHelper.instance.display(
                url: URL(string: self.message.url),
                image: self.fullImage,
                frame: self.messageImage.frame,
                referenceView: self.messageImage.superview,
                referenceContentMode: self.messageImage.contentMode,
                referenceCornerRadius: self.messageImage.layer.cornerRadius,
                topVC: topVC
            )
        }
    }


    override func prepareForReuse() {
        super.prepareForReuse()
        for recognizer in self.messageImage.gestureRecognizers ?? [] {
            self.messageImage.removeGestureRecognizer(recognizer)
        }
    }


    func setMessageDetails(msg: FMessage, chat: FChat? = nil , user: FUser? = nil){
        let tapGesture = UITapGestureRecognizer(
            target: self,
            action: #selector(self.tapMessageImage(sender:))
        )
        self.messageImage.addGestureRecognizer(tapGesture)
        
        let longPressGesture = UILongPressGestureRecognizer(
            target: self,
            action: #selector(self.showMessageOptions(sender:))
        )
        longPressGesture.minimumPressDuration = 1.0
        longPressGesture.delaysTouchesBegan = true
        self.contentView.isUserInteractionEnabled = true
        self.contentView.addGestureRecognizer(longPressGesture)
        self.message = msg
        if let user = user, let avatar = user["photos"] as? String {
            self.avatar.sd_setImage(
                with: URL(string: avatar),
                placeholderImage: UIImage(named: "photos"),
                options: SDWebImageOptions.progressiveDownload,
                completed: { image, _, _, _ in
                if image != nil {
                    self.avatar.image = image
                } else {
                    self.avatar.image = UIImage(named: "avatar")
                }
            })
        } else {
            self.avatar.image = UIImage(named: "avatar")
        }
        
        // message image
        self.messageImage.image = UIImage(named: "recuBig")
        self.messageImage.sd_setImage(
            with: URL(string: message.url),
            placeholderImage: messageImage.image,
            options: [],
            completed: { (image, error, cashTyoe, url) in
                if let image = image {
                    self.fullImage = image
                    self.messageImage.image = self.messageImage.image?.cropTo(
                        size: CGSize(width: 300, height: 300)
                    )
                }
            }
        )

        // message date
        if let date = DateFormatterProvider().displayTimeInMessages(dateSent: message.dateSent) {
            self.dateLabel.isHidden = false
            self.dateLabel.text = date
        } else{
            dateLabel.isHidden = true
        }
    }


    @objc func showMessageOptions(sender: UITapGestureRecognizer) {
        self.becomeFirstResponder()
        let menu = UIMenuController.shared
        if self.fullImage != nil {
            let saveItem = UIMenuItem(title: SAVE_STR, action: #selector(self.save))
            menu.menuItems = [saveItem]
        }
        let rect = CGRect(
            x: messageImage.frame.origin.x + messageImage.frame.size.width / 2 - 5,
            y: 15,
            width: 20,
            height: 20
        )
        menu.setTargetRect(rect, in: self)
        menu.setMenuVisible(true, animated: true)
    }


    @objc private func save(){
        if self.fullImage != nil {
            UIImageWriteToSavedPhotosAlbum(
                self.fullImage,
                self, #selector(image(_:didFinishSavingWithError:contextInfo:)),
                nil
            )
        }
    }


    @objc private func image(
        _ image: UIImage,
        didFinishSavingWithError error: Error?,
        contextInfo: UnsafeRawPointer
    ) {
        Toast(text: error != nil ? SAVE_IMAGE_ERROR : SAVE_IMAGE_SUCCESS).show()
    }


    override var canBecomeFirstResponder: Bool {
        return true
    }


    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return action == #selector(save) ? true : false
    }
    
}
