//
//  MainMessageSentTableViewCell.swift
//  maktoobProject
//
//  Created by Taoufik on 23/01/2022.
//  Copyright © 2022 CyberOne All rights reserved.
//

import UIKit

class MainMessageSentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var bubbleImage: UIImageView!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var date: UILabel!
    var controller: ConversationChatViewController!
    var msg: FMessage!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.bubbleImage.image = Languages.isRightToLeft() ? UIImage.init(
            named: "envoiRTL")?.stretchableImage(
                withLeftCapWidth: 20,
                topCapHeight: 20
            ) :
        UIImage.init(named: "envoi")?.stretchableImage(
            withLeftCapWidth: 20,
            topCapHeight: 20
        )

        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width:1.0,height:1.0)
        self.layer.shadowOpacity = 0.2
        self.layer.shadowRadius = 1.0
        
//        self.seenImg?.layer.shadowColor = UIColor.black.cgColor
//        self.seenImg?.layer.shadowOffset = CGSize(width:1.0,height:1.0)
//        self.seenImg?.layer.shadowOpacity = 0.2
//        self.seenImg?.layer.shadowRadius = 1.0
        
        self.date?.layer.shadowColor = UIColor.black.cgColor
        self.date?.layer.shadowOffset = CGSize(width:1.0,height:1.0)
        self.date?.layer.shadowOpacity = 0.1
        self.date?.layer.shadowRadius = 1.0
        
        let recognizer: UILongPressGestureRecognizer = UILongPressGestureRecognizer(
            target: self,
            action: #selector(self.handleLongPress(longPressRecognizer:))
        )
        recognizer.minimumPressDuration = 1.0
        recognizer.delaysTouchesBegan = true
        self.bubbleImage.addGestureRecognizer(recognizer)
    }


    override var canBecomeFirstResponder: Bool {
        return true
    }


    @objc func copyMessage(sender: AnyObject) {
        UIPasteboard.general.string = message.text
        self.resignFirstResponder()
    }


    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {

        if action == #selector(copyMessage(sender:)){
            return true
        }

        if action == #selector(deleteMessage(sender:)) && self.msg != nil {
            return true
        }

        return super.canPerformAction(action, withSender:sender)
    }


    @objc func deleteMessage(sender: AnyObject) {
        self.controller.deleteMessage(messageId: self.msg.objectId)
        self.resignFirstResponder()
    }


    @objc func handleLongPress(longPressRecognizer: UILongPressGestureRecognizer) {
        
        if longPressRecognizer.state != .began {
            return
        }

        if self.becomeFirstResponder() == false {
            return
        }

        let menu = UIMenuController.shared
        let targetRect = bubbleImage.frame
        menu.setTargetRect(targetRect, in: bubbleImage)
        let delete: UIMenuItem = UIMenuItem(
            title: DELETE_STR,
            action: #selector(self.deleteMessage(sender:))
        )
        let copy: UIMenuItem = UIMenuItem(
            title: COPY_STR,
            action: #selector(self.copyMessage(sender:))
        )
        menu.menuItems = [delete, copy]
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.menuWillShow),
            name: NSNotification.Name.UIMenuControllerWillShowMenu,
            object: nil
        )
        menu.setMenuVisible(true, animated: true)
    }


    @objc func menuWillShow(_ notification: Notification) {
        NotificationCenter.default.removeObserver(
            self,
            name: NSNotification.Name.UIMenuControllerWillShowMenu,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.menuWillHide),
            name: NSNotification.Name.UIMenuControllerWillHideMenu,
            object: nil
        )
        
    }


    @objc func menuWillHide(_ notification: Notification) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIMenuControllerWillHideMenu, object: nil)
    }


    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        if self.isFirstResponder == false {
            return;
        }
        let menu: UIMenuController = UIMenuController.shared
        menu.setMenuVisible(false, animated: true)
        menu.update()
        self.resignFirstResponder()
    }


    func handleParametersForCellWithMessage(message: FMessage) {
        let dateFormatter = DateFormatterProvider.dateFormatterFromGMT
        guard let date = dateFormatter.date(from: message.dateSent) else {
            return
        }

        let dateFormatterHour = DateFormatter()
        dateFormatterHour.dateFormat = "HH:mm"
        self.date?.text = dateFormatterHour.string(from: date)

        let msg: String = message.message
        self.message?.text = msg
        self.toEmoji()
    }


    private func toEmoji() {
        self.message.text = self.message.text?.replacingOccurrences(of: ":)", with: "😊")
        self.message.text = self.message.text?.replacingOccurrences(of: ":(", with: "😞")
        self.message.text = self.message.text?.replacingOccurrences(of: ":'(", with: "😭")
        self.message.text = self.message.text?.replacingOccurrences(of: ":D", with: "😁")
        self.message.text = self.message.text?.replacingOccurrences(of: ":p", with: "😛")
        self.message.text = self.message.text?.replacingOccurrences(of: "B)", with: "😎")
        self.message.text = self.message.text?.replacingOccurrences(of: "<3", with: "❤️")
        self.message.text = self.message.text?.replacingOccurrences(of: ";)", with: "😉")
        self.message.text = self.message.text?.replacingOccurrences(of: ":O", with: "😱")
        self.message.text = self.message.text?.replacingOccurrences(of: "XD", with: "😵")
        self.message.text = self.message.text?.replacingOccurrences(of: ":*", with: "😘")
        self.message.text = self.message.text?.replacingOccurrences(of: ":x", with: "😶")
        self.message.text = self.message.text?.replacingOccurrences(of: "-_-", with: "😑")
        self.message.text = self.message.text?.replacingOccurrences(of: "O:)", with: "😇")
        self.message.text = self.message.text?.replacingOccurrences(of: "3:)", with: "😈")
    }
}
