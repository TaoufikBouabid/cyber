//
//  MainMessageRecievedTableViewCell.swift
//  maktoobProject
//
//  Created by Taoufik on 23/01/2022.
//  Copyright © 2022 CyberOne All rights reserved.
//

import UIKit
class MainMessageRecievedTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profilImage: UIImageView!
    @IBOutlet weak var bubbleImage: UIImageView!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var date: UILabel!
    var controller: ConversationChatViewController!
    var msg: FMessage!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.bubbleImage.image = Languages.isRightToLeft() ? UIImage.init(
            named: "recuRTL")?.stretchableImage(
                withLeftCapWidth: 20,
                topCapHeight: 20
            ) :
        UIImage.init(named: "recu")?.stretchableImage(
            withLeftCapWidth: 20,
            topCapHeight: 20
        )

        // Initialization code
        self.profilImage.contentMode = .scaleAspectFit
        let layer = self.bubbleImage.layer
        layer.masksToBounds = false
        layer.cornerRadius = 10.0
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset =  CGSize(width: 1.0,height: 1.0)
        layer.shadowOpacity = 0.2
        layer.shadowRadius = 1.0
        
        self.date?.layer.shadowColor = UIColor.black.cgColor
        self.date?.layer.shadowOffset =  CGSize(width: 1.0,height: 1.0)
        self.date?.layer.shadowOpacity = 0.1
        self.date?.layer.shadowRadius = 1.0
        
        let recognizer = UILongPressGestureRecognizer(
            target: self,
            action: #selector(self.handleLongPress(longPressRecognizer:))
        )
        recognizer.minimumPressDuration = 1.0
        recognizer.delaysTouchesBegan = true
        self.bubbleImage.addGestureRecognizer(recognizer)

    }


    override var canBecomeFirstResponder: Bool {
        return true
    }


    @objc func copyMessage(sender: AnyObject) {
        UIPasteboard.general.string = self.message.text
        self.resignFirstResponder()
    }


    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(copyMessage(sender:)){
            return true
        }
        return super.canPerformAction(action, withSender:sender)
    }


    @objc func handleLongPress(longPressRecognizer: UILongPressGestureRecognizer) {
        if longPressRecognizer.state != .began {
            return
        }

        if self.becomeFirstResponder() == false {
            return
        }

        let menu = UIMenuController.shared
        let targetRect = self.bubbleImage.frame
        menu.setTargetRect(targetRect, in: self.bubbleImage)
        let copy: UIMenuItem = UIMenuItem(
            title: COPY_STR,
            action: #selector(copyMessage(sender:))
        )
        menu.menuItems = [copy]
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.menuWillShow),
            name: NSNotification.Name.UIMenuControllerWillShowMenu,
            object: nil
        )
        menu.setMenuVisible(true, animated: true)
    }


    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        if self.isFirstResponder == false {
            return;
        }
        let menu: UIMenuController = UIMenuController.shared
        menu.setMenuVisible(false, animated: true)
        menu.update()
        self.resignFirstResponder()
    }


    @objc func menuWillShow(_ notification: Notification) {
        NotificationCenter.default.removeObserver(
            self,
            name: NSNotification.Name.UIMenuControllerWillShowMenu,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.menuWillHide),
            name: NSNotification.Name.UIMenuControllerWillHideMenu,
            object: nil
        )
    }


    @objc func menuWillHide(_ notification: Notification) {
        NotificationCenter.default.removeObserver(
            self,
            name: NSNotification.Name.UIMenuControllerWillHideMenu,
            object: nil
        )
    }


    func handleParametersForCellWithMessage(message: FMessage) {
        let dateFormatter = DateFormatterProvider.dateFormatterFromGMT
        guard let date = dateFormatter.date(from: message.dateSent) else {
            return
        }

        let dateFormatterHour = DateFormatter()
        dateFormatterHour.dateFormat = "HH:mm"
        self.date?.text = dateFormatterHour.string(from: date)

        let msg: String = message.message
        self.message.text = msg
        self.toEmoji()
    }


    private func toEmoji() {
        self.message.text = self.message.text?.replacingOccurrences(of: ":)", with: "😊")
        self.message.text = self.message.text?.replacingOccurrences(of: ":(", with: "😞")
        self.message.text = self.message.text?.replacingOccurrences(of: ":'(", with: "😭")
        self.message.text = self.message.text?.replacingOccurrences(of: ":D", with: "😁")
        self.message.text = self.message.text?.replacingOccurrences(of: ":p", with: "😛")
        self.message.text = self.message.text?.replacingOccurrences(of: "B)", with: "😎")
        self.message.text = self.message.text?.replacingOccurrences(of: "<3", with: "❤️")
        self.message.text = self.message.text?.replacingOccurrences(of: ";)", with: "😉")
        self.message.text = self.message.text?.replacingOccurrences(of: ":O", with: "😱")
        self.message.text = self.message.text?.replacingOccurrences(of: "XD", with: "😵")
        self.message.text = self.message.text?.replacingOccurrences(of: ":*", with: "😘")
        self.message.text = self.message.text?.replacingOccurrences(of: ":x", with: "😶")
        self.message.text = self.message.text?.replacingOccurrences(of: "-_-", with: "😑")
        self.message.text = self.message.text?.replacingOccurrences(of: "O:)", with: "😇")
        self.message.text = self.message.text?.replacingOccurrences(of: "3:)", with: "😈")
    }
}
