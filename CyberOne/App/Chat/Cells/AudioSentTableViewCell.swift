//
//  AudioSentTableViewCell.swift
//  Woin
//
//  Created by macbook on 20/09/2022.
//  Copyright © 2022 macbook. All rights reserved.
//

import UIKit
import AVKit
import JustLog



final class AudioSentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var bubbleImage: UIImageView!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var playPause: UIButton!
    @IBOutlet weak var duration: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    var user: FUser!
    var message: FMessage!
    var controller: ConversationChatViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.bubbleImage.image = UIImage.init(named: "envoi")?.stretchableImage(withLeftCapWidth: 20, topCapHeight: 20)
        
        self.bubbleImage.image = Languages.isRightToLeft() ? UIImage.init(named: "envoiRTL")?.stretchableImage(withLeftCapWidth: 20, topCapHeight: 20) : UIImage.init(named: "envoi")?.stretchableImage(withLeftCapWidth: 20, topCapHeight: 20)

        self.slider.setThumbImage(UIImage.init(named: "lineWhite"), for: .normal)
        
        self.dateLabel.layer.shadowColor = UIColor.black.cgColor
        self.dateLabel.layer.shadowOffset = CGSize(width:1.0,height:1.0)
        self.dateLabel.layer.shadowOpacity = 0.1
        self.dateLabel.layer.shadowRadius = 1.0
    }


    func setMessageDetails(msg: FMessage){
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.showMessageOptions(sender:)))
        longPressGesture.minimumPressDuration = 1.0
        longPressGesture.delaysTouchesBegan = true
        contentView.isUserInteractionEnabled = true
        contentView.addGestureRecognizer(longPressGesture)

        self.message = msg
        
        // message audio
        self.duration.text = message.audioDuration
        
        if message.url.count > 0 {
            if let audioUrl = URL(string: message.url) {
                DispatchQueue.global(qos: .default).async(execute: {() -> Void in
                    do{
                        let audioData = try Data(contentsOf: audioUrl)
                        do {
                            self.controller.player = try AVAudioPlayer(data: audioData)
                            self.controller.player.delegate = self.controller
                            DispatchQueue.main.async {
                                self.slider.maximumValue = Float(self.controller.player.duration)
                            }
                        } catch {
                            Logger.shared.error("[AudioSentTableViewCell] setMessageDetails, error: \(error.localizedDescription)")
                        }
                    } catch {
                        Logger.shared.error("[AudioSentTableViewCell] setMessageDetails, error: \(error.localizedDescription)")
                    }
                })
            }
        }

        // message date
        if let date = DateFormatterProvider().displayTimeInMessages(dateSent: self.message.dateSent) {
            self.dateLabel.isHidden = false
            self.dateLabel.text = date
        } else {
            self.dateLabel.isHidden = true
        }
    }


    @objc func showMessageOptions(sender: UITapGestureRecognizer) {
        self.becomeFirstResponder()
        let menu = UIMenuController.shared
        let rect = CGRect(
            x: self.bubbleImage.frame.origin.x + bubbleImage.frame.size.width / 2 - 15,
            y: 15,
            width: 20,
            height: 20
        )
        menu.setTargetRect(rect, in: self)
        menu.setMenuVisible(true, animated: true)
    }


    override func delete(_ sender: Any?) {
        self.controller.deleteMessage(messageId: message.objectId)
        self.resignFirstResponder()
    }


    override var canBecomeFirstResponder: Bool {
        return true
    }


    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(delete(_:)) {
            return true
        }
        return false
    }

}
