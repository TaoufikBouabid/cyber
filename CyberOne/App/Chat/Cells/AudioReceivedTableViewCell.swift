//
//  AudioReceivedTableViewCell.swift
//  Woin
//
//  Created by macbook on 20/09/2022.
//  Copyright © 2022 macbook. All rights reserved.
//

import UIKit
import AVKit
import SDWebImage
import JustLog



final class AudioReceivedTableViewCell: UITableViewCell {
    
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var bubbleImage: UIImageView!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var playPause: UIButton!
    @IBOutlet weak var duration: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    var user: FUser!
    var message: FMessage!
    var player: AVAudioPlayer!
    var sliderTimer: Timer!
    var controller: ConversationChatViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.bubbleImage.image = Languages.isRightToLeft() ? UIImage.init(named: "recuRTL")?.stretchableImage(withLeftCapWidth: 20, topCapHeight: 20) : UIImage.init(named: "recu")?.stretchableImage(withLeftCapWidth: 20, topCapHeight: 20)

        self.slider.setThumbImage(UIImage(named: "lineGray"), for: .normal)

        self.dateLabel.layer.shadowColor = UIColor.black.cgColor
        self.dateLabel.layer.shadowOffset = CGSize(width:1.0,height:1.0)
        self.dateLabel.layer.shadowOpacity = 0.1
        self.dateLabel.layer.shadowRadius = 1.0
    }

    
    func setMessageDetails(msg: FMessage) {
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.showMessageOptions(sender:)))
        longPressGesture.minimumPressDuration = 1.0
        longPressGesture.delaysTouchesBegan = true
        self.contentView.isUserInteractionEnabled = true
        self.contentView.addGestureRecognizer(longPressGesture)
        self.message = msg

        // user avatar
        if let user = self.user,
            let avatar = user["photos"] as? String {
            self.avatar.sd_setImage(with: URL(string: avatar), placeholderImage: UIImage(named: "photos"),options: SDWebImageOptions.progressiveDownload, completed: { (image, _, _, _) in
                self.avatar.image = image != nil ? image : UIImage(named: "avatar")
            })
        } else {
            self.avatar.image = UIImage(named: "avatar")
        }
        
        // message audio
        self.duration.text = self.message.audioDuration

        if message.audio.count > 0 {
            if let audioUrl = URL(string: message.audio) {
                DispatchQueue.global(qos: .default).async(execute: {() -> Void in
                    do{
                        let audioData = try Data(contentsOf: audioUrl)
                        do {
                            self.controller.player = try AVAudioPlayer(data: audioData)
                            self.controller.player.delegate = self.controller
                            DispatchQueue.main.async {
                                self.slider.maximumValue = Float(self.controller.player.duration)
                            }
                        } catch {
                            Logger.shared.error("[AudioReceivedTableViewCell] setMessageDetails, error: \(error.localizedDescription)")
                        }
                    } catch {
                        Logger.shared.error("[AudioReceivedTableViewCell] setMessageDetails, error: \(error.localizedDescription)")
                    }
                })
            }
        }
        
        // message date
        if let date = DateFormatterProvider().displayTimeInMessages(dateSent: self.message.dateSent) {
            self.dateLabel.isHidden = false
            self.dateLabel.text = date
        } else {
            self.dateLabel.isHidden = true
        }
    }


    @objc func showMessageOptions(sender: UITapGestureRecognizer) {
        self.becomeFirstResponder()
        let menu = UIMenuController.shared
        let rect = CGRect(
            x: self.bubbleImage.frame.origin.x + bubbleImage.frame.size.width / 2 - 15,
            y: 15,
            width: 20,
            height: 20
        )
        menu.setTargetRect(rect, in: self)
        menu.setMenuVisible(true, animated: true)
    }


    override func delete(_ sender: Any?) {
        self.controller.deleteMessage(messageId: message.objectId)
        self.resignFirstResponder()
    }


    override var canBecomeFirstResponder: Bool {
        return true
    }


    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(delete(_:)) {
            return true
        }
        return false
    }

}
