//
//  headerSectionChat.swift
//  maktoobProject
//
//  Created by Taoufik on 24/01/2022.
//  Copyright © 2022 CyberOne All rights reserved.
//

import UIKit

class headerSectionChat: UIView {
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var headerHolder: UIView!

    class func instanceFromNib() -> UIView {
        
        return UINib(nibName: "headerSectionChat", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }

}
