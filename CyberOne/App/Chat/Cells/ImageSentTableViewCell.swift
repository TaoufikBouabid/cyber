//
//  ImageSentTableViewCell.swift
//  Woin
//
//  Created by macbook on 20/09/2022.
//  Copyright © 2022 macbook. All rights reserved.
//

import UIKit
import Toaster
import AssetsLibrary



final class ImageSentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var messageImage: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    var message: FMessage!
    var fullImage : UIImage!
    var controller: ConversationChatViewController!


    override func awakeFromNib() {
        super.awakeFromNib()

        let mask = CALayer()
        mask.contents = Languages.isRightToLeft() ? UIImage(named: "envoiBigRTL")?.cgImage : UIImage(named: "envoiBig")?.cgImage
        mask.frame = CGRect(
            x: 0,
            y: 0,
            width: self.messageImage.frame.size.width,
            height: self.messageImage.frame.size.height
        )
        self.messageImage.layer.mask = mask
        self.messageImage.layer.masksToBounds = true
        
        self.dateLabel.layer.shadowColor = UIColor.black.cgColor
        self.dateLabel.layer.shadowOffset = CGSize(width:1.0,height:1.0)
        self.dateLabel.layer.shadowOpacity = 0.1
        self.dateLabel.layer.shadowRadius = 1.0
    }


    @objc private func tapMessageImage(sender: UITapGestureRecognizer) {

        // open the image
        if self.fullImage != nil, let topVC = UIApplication.topViewController() {
            JTSImageInfoHelper.instance.display(
                url: URL(string: self.message.url),
                image: self.fullImage,
                frame: self.messageImage.frame,
                referenceView: self.messageImage.superview,
                referenceContentMode: self.messageImage.contentMode,
                referenceCornerRadius: self.messageImage.layer.cornerRadius,
                topVC: topVC
            )
        }
    }


    override func prepareForReuse() {
        super.prepareForReuse()
        for recognizer in self.messageImage.gestureRecognizers ?? [] {
            self.messageImage.removeGestureRecognizer(recognizer)
        }
    }


    func setMessageDetails(msg: FMessage) {
        let tapGesture = UITapGestureRecognizer(
            target: self,
            action: #selector(self.tapMessageImage(sender:))
        )
        self.messageImage.addGestureRecognizer(tapGesture)
        
        let longPressGesture = UILongPressGestureRecognizer(
            target: self,
            action: #selector(self.showMessageOptions(sender:))
        )
        longPressGesture.minimumPressDuration = 1.0
        longPressGesture.delaysTouchesBegan = true
        contentView.isUserInteractionEnabled = true
        contentView.addGestureRecognizer(longPressGesture)
        self.message = msg
        self.messageImage.sd_setImage(
            with: URL(string: message.url),
            placeholderImage: messageImage.image,
            options: [],
            completed: { image, _, _, _ in
                if let image = image {
                    self.fullImage = image
                    self.messageImage.image = self.messageImage.image?.cropTo(
                        size: CGSize(width: 300, height: 300)
                    )
                }
            }
        )

        // message date
        if let date = DateFormatterProvider().displayTimeInMessages(dateSent: message.dateSent) {
            self.dateLabel.isHidden = false
            self.dateLabel.text = date
        } else{
            self.dateLabel.isHidden = true
        }
    }


    @objc func showMessageOptions(sender: UITapGestureRecognizer) {
        self.becomeFirstResponder()
        let menu = UIMenuController.shared
        if self.fullImage != nil {
            let saveItem = UIMenuItem(
                title: SAVE_STR,
                action: #selector(save)
            )
            menu.menuItems = [saveItem]
        }
        let rect = CGRect(
            x: messageImage.frame.origin.x + messageImage.frame.size.width / 2 - 15,
            y: 15,
            width: 20,
            height: 20
        )
        menu.setTargetRect(rect, in: self)
        menu.setMenuVisible(true, animated: true)
    }


    @objc func save() {
        if self.fullImage != nil {
            UIImageWriteToSavedPhotosAlbum(
                self.fullImage,
                self,
                #selector(image(_:didFinishSavingWithError:contextInfo:)),
                nil
            )
        }
    }


    @objc func image(
        _ image: UIImage,
        didFinishSavingWithError error: Error?,
        contextInfo: UnsafeRawPointer
    ) {
        Toast(
            text: error != nil ? SAVE_IMAGE_ERROR : SAVE_IMAGE_SUCCESS
        ).show()
    }


    override func delete(_ sender: Any?) {
        self.controller.deleteMessage(messageId: self.message.objectId)
        self.resignFirstResponder()
    }


    override var canBecomeFirstResponder: Bool {
        return true
    }


    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(delete(_:)) || action == #selector(save) {
            return true
        }
        return false
    }

}
