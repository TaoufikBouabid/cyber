//
//  MessagesViewController.swift
//  CyberOne
//
//  Created by Taoufik on 09/09/2022.
//  Copyright © 2022 CyberOne All rights reserved.
//

import UIKit
import Firebase
import SDWebImage
import NSDate_TimeAgo
import JustLog



final class ChatListViewController: UIViewController {
    static let storyboardId = "ChatListViewController"

    @IBOutlet weak private var indicator: UIActivityIndicatorView!
    @IBOutlet weak private var indicatorLabel: UILabel!
    @IBOutlet weak private var tableView: UITableView!
    private var parentViewConroller: UIViewController!
    private var currentUserId: String!
    private var isClient: Bool!
    private var reports = [String]()

    private var chats = [FChat]()


    override func viewDidLoad() {
        super.viewDidLoad()
        self.currentUserId = FUser.currentUserId()
        self.loadMyReports()
        self.handleAdd()
        self.handleChange()
        self.handleDelete()
    }
}



extension ChatListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "inboxCell", for: indexPath) as? InboxCell else {
            return UITableViewCell()
        }
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor(hex: 0x0040A3).withAlphaComponent(0.1)
        cell.selectedBackgroundView = bgColorView
        
        let chat = self.chats[indexPath.row]
        let date = Date(timeIntervalSince1970: Double(self.chats[indexPath.row].report.createdAt))
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        let dateStr = dateFormatter.string(from: date)
        cell.name.text = "\(REPORT_CHAT_STR) \(ServiceType.type(from: self.chats[indexPath.row].report.serviceType)) \(CREATED_CHAT_STR) \(dateStr)"
        cell.name.textAlignment = Languages.currentLanguage() == "ar" ? .right : .left
        cell.name.textColor = UIColor(hex: 0x0040A3)
        cell.content.textColor = UIColor.darkGray

        if let photos = self.chats[indexPath.row].serviceImage {
            let firstPhoto = photos.components(separatedBy: ",").first
            cell.photo.sd_setImage(with: URL(string: firstPhoto!), placeholderImage: UIImage(named: "avatar"), options: [], completed: nil)
        } else {
            cell.photo.image = UIImage(named: "avatar")
        }
        
        // chat unreadMessages
        var nbr = 0
        if let type = FUser.currentUserType(), type == 1 {
            if let unreadMsg = chat.unreadMessages[self.reportExistsInChat(chat.objectId).1] as? Int, unreadMsg > 0 {
                nbr = unreadMsg
            }
        } else if let unreadMsg = chat.unreadMessages[currentUserId] as? Int, unreadMsg > 0 {
            nbr = unreadMsg
        }

        cell.gradientView.isHidden = nbr == 0
        cell.numbrtUnreadMsg.isHidden = nbr == 0
        cell.numbrtUnreadMsg.text = nbr == 0 ? nil : String(nbr)
       
        // chat last message
        cell.content.text = !chat.lastMessage.isEmpty ? chat.lastMessage : NO_MESSAGES_FOUND

        // chat date
        cell.hour.text = (chat.lastMessageDateSent as NSDate).timeAgo()
        cell.hour.textAlignment = Languages.currentLanguage() == "ar" ? .left : .right
        return cell
    }
}



extension ChatListViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let chatVC = StoryboardProvider.instance.storyboard(for: .main).instantiateViewController(
            withIdentifier: ConversationChatViewController.storyboardId
        ) as? ConversationChatViewController else {
            return
        }
        chatVC.chatId = self.chats[indexPath.row].objectId
        chatVC.opponentUser = self.chats[indexPath.row].report
        chatVC.opponentId = self.chats[indexPath.row].report.objectId
        chatVC.reportId = self.reportExistsInChat(self.chats[indexPath.row].objectId).1
        chatVC.title = (tableView.cellForRow(at: indexPath) as? InboxCell)?.name.text ?? self.chats[indexPath.row].report.fullName
        chatVC.isClosed = self.chats[indexPath.row].isClosed
        self.navigationController?.pushViewController(chatVC, animated: true)
    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

}



extension ChatListViewController {

    func reportExistsInChat(_ chatId: String) -> (success: Bool, reportId: String) {
        for report in self.reports {
            if chatId.contains(report) {
                return (true,report)
            }
        }
        return (false, "")
    }


    private func addChat(_ value: [String: Any], objectId: String) {
        let chat = FChat(chatDict: value)
        if self.reportExistsInChat(objectId).0 {
            chat.opponentKey = self.reportExistsInChat(objectId).1
        } else {
            if let range = chat.opponentKey.range(of: self.currentUserId){
                chat.opponentKey.removeSubrange(range)
            }
        }
        self.chats.append(chat)
        self.chats = self.chats.sorted(
            by: { $0.lastMessageDateSent > $1.lastMessageDateSent }
        )
        self.getUser()
    }


    func getChats() {
        Database.database().reference().child(FChat.path).observeSingleEvent(of: .value) { snapshot in
            if snapshot.exists() {
                self.indicator.stopAnimating()
                var chatExists = false
                for value in snapshot.children.allObjects {
                    if let val = value as? [String: Any],
                       let objId = val[FChat.keys.objectId.rawValue] as? String {
                        if FUser.currentUserType() == FUserType.admin.rawValue {
                            if self.reportExistsInChat(objId).0 {
                                chatExists = true
                                self.addChat(val, objectId: objId)
                            }
                        } else if objId.contains(self.currentUserId) {
                            chatExists = true
                            self.addChat(val, objectId: objId)
                        }
                    }
                }

                if !chatExists {
                    self.indicatorLabel.text = NO_CHATS_FOUND
                    self.indicatorLabel.isHidden = false
                } else {
                    self.indicatorLabel.isHidden = true
                }
            }
        }
    }


    func getUser() {
        Database.database()
            .reference()
            .child(FReport.path)
            .observeSingleEvent(of: .value, with: { snap in
            if snap.exists() {
                if let data = snap.value as? [String: Any] {
                    let keys = Array(data.keys)
                    for chat in self.chats {
                        let reportId = chat.opponentKey
                        if keys.contains(reportId) {
                            if let report = (snap.value as? [String: Any])?.first (where: { chat.opponentKey == $0.key }),
                                let value = report.value as? [String: Any] {
                                chat.report = FReport(value: value)
                            }
                        } else {
                            chat.report = FReport(value: ["fullName" : "User Deleted"])
                        }
                    }
                    self.getReportsImages()
                }
            }
        }) {error in
            print("user",error.localizedDescription)
        }
    }


    func getReportsImages() {
        Database.database().reference().child("Services")
            .observeSingleEvent(of: .value, with: { (snap) in
            if snap.exists() {
                var index = 0
                for chat in self.chats {
                    let enumerator = snap.children
                    while let rest = enumerator.nextObject() as? DataSnapshot {
                        if let dict = rest.value as? [String: Any],
                            dict["id"] as? Int == chat.report.serviceType {
                            if let serviceType = rest.value as? [String:Any] {
                                chat.serviceImage = serviceType["icon"] as? String ?? ""
                            }
                        }
                    }
                    index += 1
                }
                self.tableView.reloadData()
            }
        }) {error in
            print("user",error.localizedDescription)
        }
    }


    func handleChange() {
        Database.database()
            .reference()
            .child(FChat.path)
            .observe(.childChanged, with: { snapshot in
            if snapshot.exists() {
                if self.isChatInScope(snapshot.key) {
                    if let updatedValues = snapshot.value as? [String: Any],
                       let index = self.chats.firstIndex(where: { $0.objectId == snapshot.key }) {
                        self.chats[index].update(from: updatedValues)
                        self.chats.sort(by: { $0.lastMessageDateSent > $1.lastMessageDateSent} )
                        self.tableView.reloadData()
                    }
                }
            }
        }){ error in
            Logger.shared.error("\(#function) error: \(error.localizedDescription)")
        }
    }


    func handleAdd() {
        Database.database()
            .reference()
            .child(FChat.path)
            .observe(.childAdded, with: { snapshot in
            if snapshot.exists() {
                let type = FUser.currentUserType() ?? 0
                if self.isChatInScope(snapshot.key) {
                    if let snapValue = snapshot.value as? [String:Any] {
                        let chat = FChat(chatDict: snapValue)
                        chat.objectId = snapshot.key
                        chat.opponentKey = snapshot.key
                        if let range = chat.opponentKey.range(of: self.currentUserId) {
                            chat.opponentKey.removeSubrange(range)
                        }
                        var found: Bool = false
                        let chatsLoop = self.chats
                        for chat: FChat in chatsLoop {
                            if chat.objectId == snapshot.key {
                                found = true
                                break;
                            }
                        }
                        if !found {
                            self.chats.append(chat)
                            self.chats = self.chats.sorted(by: { (chat1, chat2) -> Bool in
                                return chat1.lastMessageDateSent.compare(chat2.lastMessageDateSent) == .orderedDescending
                            })
                            self.getUser()
                        }
                    }
                }
            }
        }){ error in
            Logger.shared.error("\(#function) error: \(error.localizedDescription)")
        }
    }


    func handleDelete() {
        Database.database().reference()
            .child(FChat.path)
            .observe(.childRemoved, with: { snapshot in
            if snapshot.exists() {
                if self.isChatInScope(snapshot.key) {
                    let ids: [String] = self.chats.compactMap { $0.objectId }
                    if let position = ids.index(of: snapshot.key) {
                        self.chats.remove(at: position)
                        self.tableView.deleteRows(at: [IndexPath.init(row: position, section: 0)], with: .right)
                    }
                }
            }
        }){ error in
            Logger.shared.error("\(#function) error: \(error.localizedDescription)")
        }
    }


    func loadMyReports() {
        Database.database()
            .reference()
            .child("Reports")
            .observe(.childAdded, with: { (snapshot) in
            if snapshot.exists() {
                if let snapValue = snapshot.value as? [String:Any] {
                    let type = FUser.currentUserType() ?? 0
                    let report = FReport(value: snapValue)
                    if report.userId == FUser.currentUserId() || type == 1 {
                        self.reports.append(report.objectId)
                    }
                }
                self.getChats()
            }
        }){ error in
            Logger.shared.error("\(#function) error: \(error.localizedDescription)")
        }
    }


    private func isChatInScope(_ key: String) -> Bool {
        return key.contains(self.currentUserId)
        || (self.reportExistsInChat(key).success && FUser.currentUserType() == FUserType.admin.rawValue)
    }

}
