//
//  Constants.swift
//  CyberOne
//
//  Created by Taoufik on 17/12/2018.
//  Copyright © 2018 Cyber One. All rights reserved.
//

// Firebase storage URL
let STORAGE_URL = "gs://cyberonedemo-87961.appspot.com"

// WP URL
let CYBERONE_WP_URL = "https://cyberone.co"

// WP AUTH_KEY
let CYBERONE_WP_AUTH_KEY = "authKey"

// WP Users notif
let CYBERONE_WP_USERS_LIST = "UsersListNotif"
