//
//  ForgotPasswordTableViewController.swift
//  CyberOne
//
//  Created by Taoufik on 12/11/2018.
//  Copyright © 2018 Cyber One. All rights reserved.
//

import UIKit

final class ForgotPasswordTableViewController: UIViewController {


    override func viewDidLoad() {
        super.viewDidLoad()
    }


    @IBAction func backAction(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.tintColor = UIColor.init(hex: 0x2062AC)
        self.navigationController?.navigationBar.barTintColor = UIColor.init(hex: 0x2062AC)
    }


    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}
