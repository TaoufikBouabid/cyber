//
//  LoginTableViewController.swift
//  CyberOne
//
//  Created by Taoufik on 12/11/2018.
//  Copyright © 2018 Cyber One. All rights reserved.
//

import UIKit
import ProgressHUD
import STPopup
import FirebaseDatabase



final class LoginTableViewController: UITableViewController {
    static let storyboardId = "LoginTableViewController"
    static let holderVcId = "LoginViewController"

    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var dialCode: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setRTL()
        self.setLocalDialCodeFlag()
    }


    private func setRTL() {
        self.phoneField.textAlignment = Languages.isRightToLeft() ? .right : .left
        self.passwordField.textAlignment = Languages.isRightToLeft() ? .right : .left
    }


    private func setLocalDialCodeFlag() {
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String,
           let countryDialCode = countryCode.countryCallingCode {
            self.dialCode.setTitle("+"+countryDialCode, for: .normal)
        }
    }


    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }


    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 30
        case 1:
            return self.view.frame.height/2 - 84
        case 3, 4, 6:
            return 80
        case 2, 5, 7:
            return 44
        default:
            return 44
        }
    }


    @IBAction func forgotAction(_ sender: UIButton) {
        if let phoneText = self.phoneField.text, phoneText.isEmpty {
            showToast(EMPTY_PHONE_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular)
            return
        }
        if let phoneText = self.phoneField.text, !phoneText.isPhoneNumber {
            showToast(VALID_PHONE_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular)
            return
        }
        let values: [String: Any] = [
            "createdAt": ServerValue.timestamp(),
            "phoneNumber": dialCode.titleLabel!.text! + phoneField!.text!
        ]

        let object = FObject(
            path: "ForgotPassword",
            subpath: randomString(length: 10),
            dictionary: values
        )
        ProgressHUD.show()
        object.saveInBackground { error in
            error == nil ? ProgressHUD.showSuccess() :  ProgressHUD.showError()
        }
    }


    func removeFirstZero() -> String? {
        if (self.phoneField.text as NSString?)?.substring(to: 1) == "0" {
            self.phoneField.text?.removeFirst()
        }
        return self.phoneField.text
    }


    func validationAction() -> Bool {
        if let phoneText = self.phoneField.text, phoneText.isEmpty {
            showToast(EMPTY_PHONE_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular)
            return false
        }
        if let phoneText = self.phoneField.text, !phoneText.isPhoneNumber {
            showToast(VALID_PHONE_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular)
            return false
        }
        if let passwordText = self.passwordField.text, passwordText.isEmpty {
            showToast(EMPTY_PASSWORD_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular)
            return false
        }
        return true
    }


    @IBAction func loginAction(_ sender: UIButton) {
        if !self.validationAction() { return }
        guard let phoneEnglish = self.phoneField.text!.englishNumbers else {
            return
        }
        let email = (dialCode.titleLabel!.text! + "\(phoneEnglish)")+"@gmail.com"
        ProgressHUD.show(nil, interaction: false)
        FUser.signIn(email: email, password: self.passwordField.text ?? "") { user, error in
            if error == nil {
                UserDefaults.standard.set(true, forKey: "isUserLoggedIn")
                if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                    appDelegate.setupWP()
                }
                self.performSegue(withIdentifier: "goToHome", sender: nil)
            } else {
                ProgressHUD.showError(error!.localizedDescription)
            }
        }
    }


    @IBAction func dialCodeAction(_ sender: UIButton) {
        let countryVC = CountrySelectorViewController(
            nibName: CountrySelectorViewController.identifier,
            bundle: nil
        )
        countryVC.delegate = self
        countryVC.showDialCode = true
        let countrySelector = STPopupController(rootViewController: countryVC)
        countrySelector.present(in: self)
    }


    @IBAction private func signup(_ sender: UIButton) {
        let signupVc = StoryboardProvider.instance.storyboard(for: .main).instantiateViewController(withIdentifier: SignUpViewController.identifier)
        self.navigationController?.pushViewController(signupVc, animated: true)
    }

}



extension LoginTableViewController: CountrySelectorDelegate {

    func didSelectCountry(country: Country, viewController: UIViewController) {
        viewController.dismiss(animated: true) {
            self.dialCode.setTitle(country.dialCode, for: .normal)
        }
    }
}
