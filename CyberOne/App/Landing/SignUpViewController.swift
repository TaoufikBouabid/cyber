//
//  SignUpViewController.swift
//  CyberOne
//
//  Created by Taoufik on 14/11/2018.
//  Copyright © 2018 Cyber One. All rights reserved.
//

import UIKit
import ProgressHUD
import FirebaseAuth
import FirebaseDatabase



final class SignUpViewController: UIViewController {
    static let identifier = "SignUpViewController"


    override func viewDidLoad() {
        super.viewDidLoad()
    }


    @IBAction private func backAction(_ sender: UIButton) {
        if self.navigationController?.viewControllers.first == self {
            self.dismiss(animated: true, completion: nil)
            return
        }
        self.navigationController?.popViewController(animated: true)
    }


    private func validateAction() -> Bool {
        guard let signUpVc = self.childViewControllers.first as? SignUpFormTableViewController else { return false }
        
        if let nameText = signUpVc.nameField.text, nameText.isBlank {
            showToast(EMPTY_NAME_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular)
            return false
        }
        
        if let ageText = signUpVc.ageField.text, ageText.isBlank {
            showToast(EMPTY_AGE_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular)
            return false
        }
        
        if let emailText = signUpVc.emailField.text, emailText.isBlank {
            showToast(EMPTY_EMAIL_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular)
            return false
        }
        
        if let emailText = signUpVc.emailField.text, !emailText.trimmingCharacters(in: .whitespacesAndNewlines).isEmail {
            showToast(VALID_EMAIL_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular)
            return false
        }
        
        if let phoneText = signUpVc.phoneField.text, phoneText.isBlank {
            showToast(EMPTY_PHONE_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular)
            return false
        }
        
        if let phoneText = signUpVc.phoneField.text, !phoneText.isPhoneNumber {
            showToast(VALID_PHONE_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular)
            return false
        }
        
        if let passwordText = signUpVc.passwordField.text, passwordText.isBlank {
            showToast(EMPTY_PASSWORD_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular)
            return false
        }
        
        if let passwordText = signUpVc.passwordField.text, passwordText.count < 6 {
            showToast(LENGTH_PASSWORD_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular)
            return false
        }
        
        if let confirmPasswordText = signUpVc.confirmPasswordField.text, confirmPasswordText.isBlank {
            showToast(EMPTY_PASSWORD_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular)
            return false
        }
        
        if let passwordText = signUpVc.passwordField.text,
            let confirmPasswordText = signUpVc.confirmPasswordField.text,
            passwordText != confirmPasswordText {
            showToast(PASSWORD_NOT_CONFIRM_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular)
            return false
        }

        if let ageText = signUpVc.ageField.text, Int(ageText) == nil {
            showToast(VERIFY_AGE_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular)
            return false
        }
        return true
    }


    @IBAction private func submitAction(_ sender: UIButton) {
        if !self.validateAction() { return }
        
        guard let signUpVc = self.childViewControllers.first as? SignUpFormTableViewController else { return; }
        self.verifyUser(
            "+" + signUpVc.dialCodeLabel.text!.components(separatedBy: "+").last! + signUpVc.phoneField.text!
        )
        
        guard let verifyVc = StoryboardProvider.instance.storyboard(for: .main).instantiateViewController(
            withIdentifier: VerifyPhoneViewController.storyboardId
        ) as? VerifyPhoneViewController else {
            return
        }

        verifyVc.modalTransitionStyle = .crossDissolve
        verifyVc.modalPresentationStyle = .fullScreen

        verifyVc.user = [
            "name": signUpVc.nameField.text!,
            "phoneNumber": "+" + signUpVc.dialCodeLabel.text!.components(separatedBy: "+").last! + signUpVc.phoneField.text!,
            "age": Int(signUpVc.ageField.text!)! ,
            "password": signUpVc.passwordField.text!,
            "email": signUpVc.emailField.text!,
            "country": signUpVc.country,
            "fcmToken": UserDefaults.standard.dictionary(forKey: "FCMToken")?["token"] ?? ""
        ]
        self.navigationController?.pushViewController(verifyVc, animated: true)
    }


    private func verifyUser(_ fullPhoneNumber: String) {
        PhoneAuthProvider.provider().verifyPhoneNumber(fullPhoneNumber, uiDelegate: nil) { (verificationID, error) in
            DispatchQueue.main.async {
                if let error = error {
                    print(error.localizedDescription)
                    UIApplication.topViewController()?.showAlertView(
                        isOneButton: true,
                        title: NSLocalizedString(
                            error.localizedDescription ,
                            comment: ""
                        ),
                        cancelButtonTitle: OK_STR,
                        submitButtonTitle: OK_STR
                    )
                } else {
                    UserDefaults.standard.set(verificationID, forKey: "verificationID")
                }
            }
        }
    }

}
