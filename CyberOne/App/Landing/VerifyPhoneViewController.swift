//
//  VerifyPhoneViewController.swift
//  CyberOne
//
//  Created by Taoufik on 14/11/2018.
//  Copyright © 2018 Cyber One. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import ProgressHUD



final class VerifyPhoneViewController: UIViewController {
    static let storyboardId = "VerifyPhoneViewController"
    
    @IBOutlet weak private var OTPField: UITextField!
    @IBOutlet weak private var logoImage: UIImageView!

    var user = [String: Any]()


    override func viewDidLoad() {
        super.viewDidLoad()
        self.OTPField.becomeFirstResponder()

        self.OTPField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)

    }


    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text?.count == 6 {
            self.view.endEditing(true)
        }
    }


    @IBAction private func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }


    @IBAction private func verifyAction(_ sender: UIButton) {
        if UserDefaults.standard.string(forKey: "verificationID") != nil {
            let credential = PhoneAuthProvider.provider().credential(
                withVerificationID: UserDefaults.standard.string(forKey: "verificationID")!,
                verificationCode: OTPField.text!)
            Auth.auth().signIn(with: credential) { user, error in
                UIApplication.shared.windows.first?.isUserInteractionEnabled = true
                if error != nil {
                    showToast(
                        INCORRECT_OTP_STR,
                        backgroundColor: toastColor,
                        textColor: .white,
                        font: h2Regular
                    )
                    return
                }
                UserDefaults.standard.set(nil, forKey: "verificationID")
                self.signup()
            }
        } else {
            showToast(
                NSLocalizedString("Incorrect_code", comment: ""),
                backgroundColor: toastColor,
                textColor: .white,
                font: h2Regular
            )
        }
    }


    private func signup() {
        ProgressHUD.show(nil, interaction: false)
        FUser.createUser(
            email: self.user["phoneNumber"] as! String + "@gmail.com",
            password: self.user["password"] as! String
        ) { user, error in
            if (error == nil) {
                self.saveUser(
                    [
                        "phoneNumber" : self.user["phoneNumber"] as! String,
                        "name" : self.user["name"] as! String,
                        "password" : self.user["password"] as! String,
                        "age": self.user["age"] as! Int,
                        "email": self.user["email"] as! String,
                        "country": self.user["country"] as! String
                    ]
                )
            } else {
                if (error! as NSError).code == 17007 {
                    ProgressHUD.showError(PHONE_ALREADY_USED_STR)
                    return;
                }
                ProgressHUD.showError(error!.localizedDescription)
            }
        }
    }


    @IBAction private func resendAction(_ sender: UIButton) {
        self.verifyUser(self.user["phoneNumber"] as! String)
    }


    private func verifyUser(_ fullPhoneNumber: String) {
        PhoneAuthProvider.provider().verifyPhoneNumber(fullPhoneNumber, uiDelegate: nil) { (verificationID, error) in
            DispatchQueue.main.async {
                if error != nil {
                    showToast(
                        NSLocalizedString("error creating account", comment: ""),
                        backgroundColor: toastColor,
                        textColor: .white,
                        font: h2Regular
                    )
                    return
                } else {
                    UserDefaults.standard.set(verificationID, forKey: "verificationID")
                }
            }
        }
    }


    private func saveUser(_ usr: [String: Any]) {
        if let currentUser = Auth.auth().currentUser {
            Database.database()
                .reference()
                .child("User")
                .child(currentUser.uid)
                .updateChildValues(usr)
            UserDefaults.standard.set(true, forKey: "isUserLoggedIn")
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                appDelegate.setupWP()
            }
            let tabBarController = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "TabBarController")
            tabBarController.modalTransitionStyle = .crossDissolve
            tabBarController.modalPresentationStyle = .fullScreen
            self.present(
                tabBarController, animated: true, completion: nil)
        }
    }


}
