//
//  SignUpFormTableViewController.swift
//  CyberOne
//
//  Created by Taoufik on 14/11/2018.
//  Copyright © 2018 Cyber One. All rights reserved.
//

import UIKit
import STPopup
import UITextField_Max
import JustLog



final class SignUpFormTableViewController: UITableViewController {

    @IBOutlet weak var arrow: UIImageView!
    @IBOutlet weak var personalInfoView: UIView!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var secondPasswordView: UIView!
    @IBOutlet weak var confirmPasswordView: UIView!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var ageField: UITextField!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var dialCodeLabel: UILabel!
    @IBOutlet weak var ageView: UIView!
    @IBOutlet weak var flagLabel: UILabel!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var confirmPasswordField: UITextField!

    @IBOutlet private var fields: [UITextField]!

    var country: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupRTL()
        self.setupCurrentDialCodeFlag()
        self.ageField.maxLength = 3
    }


    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.setView()
    }


    @IBAction func countryAction(_ sender: UITapGestureRecognizer) {
        let countryVC = CountrySelectorViewController(
            nibName: CountrySelectorViewController.identifier,
            bundle: nil
        )
        countryVC.delegate = self
        countryVC.showDialCode = true
        let countrySelector = STPopupController(rootViewController: countryVC)
        countrySelector.present(in: self)
    }


    private func setupRTL() {
        self.fields.forEach { $0.textAlignment = Languages.isRightToLeft() ? .right : .left }
        if Languages.isRightToLeft() {
            self.arrow.rotate()
        }
    }


    private func setupCurrentDialCodeFlag() {
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String,
            let countryDialCode = countryCode.countryCallingCode {
            self.dialCodeLabel.text = "+"+countryDialCode
            self.flagLabel.text = countryCode.emojiFlag()
            let bundle =  Bundle(for: type(of: self))
            if let filePath = bundle.path(
                forResource: "Countries",
                ofType: "json"
            ) {
                do {
                    let data = try String(contentsOfFile: filePath, encoding: .utf8).data(using: .utf8)
                    let countries = try JSONDecoder().decode([Country].self, from: data!)
                    let country = countries.first(where: { $0.dialCode == "+"+countryDialCode })
                    self.country = Languages.currentLanguage() == "ar" ? country?.nameAr ?? "" : country?.nameEn ?? ""
                } catch {
                    Logger.shared.error("[CountrySelectorViewController] viewDidLoad, cannot load countries, error: \(error.localizedDescription)")
                }
            }
        }
    }


    private func setView() {
        let radiusValue = 10.0
        self.personalInfoView.roundCorners(corners: [.topLeft,.topRight], cornerRadius: radiusValue)
        self.ageView.roundCorners(corners: [.bottomLeft,.bottomRight], cornerRadius: radiusValue)
        self.passwordView.roundCorners(corners: [.topRight,.topLeft], cornerRadius: radiusValue)
        self.confirmPasswordView.roundCorners(corners: [.bottomLeft,.bottomRight], cornerRadius: radiusValue)
    }

}



extension SignUpFormTableViewController: CountrySelectorDelegate {

    func didSelectCountry(country: Country, viewController: UIViewController) {
        viewController.dismiss(animated: true) {
            self.country = Languages.currentLanguage() == "ar" ? country.nameAr : country.nameEn
            self.dialCodeLabel.text = country.dialCode
            self.flagLabel.text = country.code?.emojiFlag()
        }
    }
}
