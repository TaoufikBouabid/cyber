//
//  Services.swift
//  CyberOne
//
//  Created by Taoufik on 25/01/2019.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import Foundation

let GET_ALL_NEWS = "http://cyberone.co/wp-json/wp/v2/posts"

let GET_NEWS_COMMENTS = "http://cyberone.co/wp-json/wp/v2/comments?post=%@"

let ADD_COMMENT = "https://www.cyberone.co/wp-json/wp/v2/comments?author_name=%@&author_email=%@&author_name=%@&content=%@&parent=%@&post=%@"
