//
//  NewsDetailsViewController.swift
//  CyberOne
//
//  Created by Taoufik on 15/11/2018.
//  Copyright © 2018 Cyber One. All rights reserved.
//

import UIKit
import SwiftyJSON
import NSDate_TimeAgo
import ProgressHUD
import JustLog



final class NewsDetailsViewController: UIViewController, UIWebViewDelegate {
    static let storyboardId = "NewsDetailsViewController"
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var commentTextField: UITextField!
    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var webViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var commentViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var commentHolderView: UIView!
    
    var news: Post!
    var allComments = [Comment]()
    var users = [FUsers]()
    var commentToRespond: Int!
    var webViewLoads = 0


    override func viewDidLoad() {
        super.viewDidLoad()

        self.scrollView.keyboardDismissMode = .onDrag
        self.commentViewBottomConstraint.constant = 25
        self.commentTextField.textAlignment = Languages.isRightToLeft() ? .right : .left
        self.scrollView.delegate = self
        
        if let image = news.yoastHeadJson?.ogImage?.first?.url {
            self.newsImage.sd_setImage(with: URL(string: image))
        }
        
        self.titleLabel.text = self.news.title?.rendered
        self.titleLabel.textAlignment = .right

        let content = self.news.content?.rendered ?? "can't load news content"
        
        let div = "<div style=\"text-align:right\";><style type='text/css'>img { max-width: 100%%; width: auto; height: auto; }</style><style type='text/css'>iframe { width: "+String(describing: self.webView.frame.size.width)+"; height: "+String(describing: self.webView.frame.size.width*9/16)+"; }</style>"
        self.webView.loadHTMLString(div+content+"</div>", baseURL: nil)
        self.webView.allowsInlineMediaPlayback = true
        self.webView.allowsLinkPreview = true
        self.webView.allowsPictureInPictureMediaPlayback = true
        self.webView.scrollView.isScrollEnabled = false
        self.webView.delegate = self

        self.commentHolderView.shadowWithCornder(
            raduis: 10,
            shadowColor: UIColor.darkGray.cgColor,
            shadowOffset: CGSize(width: 2, height: 2),
            shadowOpacity: 0.5,
            shadowRadius: 5
        )

        self.getComments()
    }


    func getComments() {
        guard let newsID = self.news.id else {
            return
        }
        Comment.list(
            parent: nil,
            parentExclude: nil,
            post: [newsID],
            status: nil,
            type: nil,
            password: nil
        ) { (response: Result<[Comment]>) in
            switch response {
            case .success(let value):
                self.allComments = value
                DispatchQueue.main.async {
                    self.organizeDatasource()
                }
            case .failure(let error):
                Logger.shared.error("[NewsDetailsViewController] getComments, an error has occured: \(error.localizedDescription)")
            }
        }
    }


    func organizeDatasource() {
        var dict = [Comment: [Comment]]()
        var parentComments = self.allComments.filter { $0.parent ?? 0 == 0 }
        parentComments.sort(by: { $0.date ?? Date() < $1.date ?? Date() })
        let subComments = self.allComments.filter { $0.parent ?? 0 != 0 }
        parentComments.forEach { parent in
            dict[parent] = subComments.filter { $0.parent == parent.id }
        }
        self.allComments.removeAll()
        for parentComment in parentComments {
            self.allComments.append(parentComment)
            self.allComments.append(contentsOf: dict[parentComment] ?? [])
        }
        self.resizeTableView()
    }


    @IBAction func addComment(_ sender: UIButton) {
        self.view.endEditing(true)
        let comment = self.commentTextField.text?.trimmingCharacters(
            in: .whitespacesAndNewlines
        )
        if comment?.count == 0 {
            self.showAlertView(
                isOneButton: true,
                title: WRITE_COMMENT_STR,
                cancelButtonTitle: OK_STR,
                submitButtonTitle: OK_STR
            )
            return
        }
        let commentObj = Comment(
            post: self.news.id,
            parent: self.commentToRespond,
            email: FUser.currentUserEmail(),
            authorName: FUser.currentUserName(),
            content: self.commentTextField.text ?? ""
        )
        commentObj.save { (response: Result<Comment>) in
            switch response {
            case .success(let value):
                DispatchQueue.main.async {
                    showToast(
                        ADMIN_APPROVE_STR,
                        backgroundColor: .black,
                        textColor: .white,
                        font: h2Regular
                    )
                    self.allComments.append(value)
                    self.commentToRespond == nil ? self.resizeTableView() : self.organizeDatasource()
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    showToast(
                        error.localizedDescription,
                        backgroundColor: toastColor,
                        textColor: .white,
                        font: h2Regular
                    )
                }
            }
        }
        self.commentToRespond = nil
        self.commentTextField.text?.removeAll()
    }


    @objc func replyAction(_ sender: UIButton) {
        let comment = self.allComments[sender.tag]
        self.commentToRespond = comment.id
        self.commentTextField.becomeFirstResponder()
    }


    func resizeTableView(){
        self.tableView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            UIView.animate(withDuration: 0.2, animations: {
                self.tableViewHeight.constant = self.tableView.contentSize.height
                self.view.layoutIfNeeded()
            }, completion: { (finished) in
                UIView.animate(withDuration: 0.2, animations: {
                    self.tableViewHeight.constant = self.tableView.contentSize.height
                    self.view.layoutIfNeeded()
                })
            })
        }
    }
}

extension NewsDetailsViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allComments.count + 2
    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(
                withIdentifier: CommentsNumberTableViewCell.identifier,
                for: indexPath
            ) as? CommentsNumberTableViewCell else {
                return UITableViewCell()
            }
            cell.commentsLabel.text = String(format: COMMENTS_STR, "\(allComments.count)")
            return cell
        case self.allComments.count + 1:
            return UITableViewCell()
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: NewsCommentTableViewCell.identifier, for: indexPath) as? NewsCommentTableViewCell else {
                return UITableViewCell()
            }
            let index = indexPath.row - 1
            let comment = self.allComments[index]

            if let parent = comment.parent {
                cell.replyConstraint.constant = parent == 0 ? 0 : 50
            } else {
                cell.replyConstraint.constant = 0
            }
            cell.commentDetails.attributedText = comment.content?.convertHtml()
            cell.userName.text = comment.authorName
            cell.userImage.contentMode = .scaleAspectFit
            if let avatar = comment.authorAvatarUrls?.size48 {
                cell.userImage.sd_setImage(with: URL(string: avatar), placeholderImage: UIImage(named: "avatar"), options: [])
            } else {
                cell.userImage.image = UIImage(named: "avatar")
            }

            if let date = comment.date {
                let dateFormatter = DateFormatterProvider.dateFormatterFromGMT
                dateFormatter.timeZone = .current
                cell.commentTime.text = (date as NSDate).timeAgo()
            }

            cell.replyButton.tag = indexPath.row - 1
            cell.replyButton.addTarget(
                self,
                action: #selector(self.replyAction(_:)),
                for: .touchUpInside
            )

            return cell
        }
    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0, self.allComments.count + 1:
            return 60
        default:
            return UITableViewAutomaticDimension
        }
    }


    func webViewDidStartLoad(_ webView: UIWebView) {
        self.webViewLoads += 1
    }


    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.webViewLoads -= 1
    }


    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.webViewLoads -= 1
        if self.webViewLoads > 0 {
            return
        }
        self.webViewHeight.constant = webView.scrollView.contentSize.height
        view.layoutIfNeeded()
    }


    func webView(
        _ webView: UIWebView,
        shouldStartLoadWith request: URLRequest,
        navigationType: UIWebViewNavigationType
    ) -> Bool {
        if navigationType == .linkClicked, let url = request.url {
            UIApplication.shared.open(url)
            return false
        }
        return true
    }

}
