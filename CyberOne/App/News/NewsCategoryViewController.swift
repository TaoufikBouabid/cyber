//
//  NewsCategoryViewController.swift
//  CyberOne
//
//  Created by Taoufik on 11/26/19.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import UIKit
import ProgressHUD
import JustLog



final class NewsCategoryViewController: UIViewController {

    struct Const {
        static let maxPage = 50
        static let newsFileName = "News"
        static let newsCategoriesFileName = "NewsCategory"
    }

    private var pageMenu: CAPSPageMenu?
    private var categories = [Category]()
    private var onlineCategories = [Category]()
    private var news = [Post]()
    private var onlineNews = [Post]()
    @IBOutlet weak private var containerView: UIView!

    lazy var wpSharedInstance: WP! = {
        guard let wpSharedInstance = (UIApplication.shared.delegate as? AppDelegate)?.WPSharedInstance else {
            Logger.shared.error("[NewsCategoryViewController] getNewsCategory, error loading the WP shared instance!")
            return WP.sharedInstance
        }
        return wpSharedInstance
    }()

    /// Used to save objects
    private let codableFiles = CodableFiles.shared


    override func viewDidLoad() {
        super.viewDidLoad()
        self.getDataOffline()
    }


    private func getDataOffline() {
        do {
            let newsCategoriesOffline = try self.codableFiles.loadAsArray(
                objectType: Category.self,
                withFilename: Const.newsCategoriesFileName
            )
            let newsOffline = try self.codableFiles.loadAsArray(
                objectType: Post.self,
                withFilename: Const.newsFileName
            )
            self.news = newsOffline
            self.categories = newsCategoriesOffline
            self.setupPager()
            self.getNewsCategory(loadedOffline: true)
        } catch {
            Logger.shared.error("[NewsCategoryViewController] getNews, Unable to load news offline")
            self.getNewsCategory(loadedOffline: false)
        }
    }


    private func getNewsCategory(loadedOffline: Bool) {
        if !loadedOffline {
            ProgressHUD.show(nil, interaction: false)
        }
        Category.list(WPSharedInstance: self.wpSharedInstance) { (response: Result<[Category]>) in
            switch response {
            case .success(let value):
                self.onlineCategories = value
                do {
                    try self.codableFiles.saveAsArray(
                        objects: self.onlineCategories,
                        withFilename: Const.newsCategoriesFileName
                    )
                } catch {
                    Logger.shared.error("[NewsCategoryViewController] getNewsCategory, Unable to save news in codable files locally")
                }

                self.categories = self.onlineCategories
                self.getNews(page: 1, loadedOffline: loadedOffline)
                break
            case .failure(let error):
                ProgressHUD.showError(error.localizedDescription)
                Logger.shared.error("[NewsCategoryViewController] getNewsCategory, an error has occured: \(error.localizedDescription)")
                break
            }
        }
    }


    private func getNews(page: Int, loadedOffline: Bool) {
        BLPost.list(
            page: page,
            perPage: Const.maxPage,
            categories: self.categories.compactMap { $0.id }
        ) { (response: Result<[BLPost]>) in
            switch response {
            case .success(let value):
                self.onlineNews.append(contentsOf: value)
                if value.count == Const.maxPage {
                    let nextPage = page + 1
                    self.getNews(page: nextPage, loadedOffline: loadedOffline)
                } else {
                    do {
                        try self.codableFiles.saveAsArray(
                            objects: self.onlineNews,
                            withFilename: Const.newsFileName
                        )
                    } catch {
                        Logger.shared.error("[NewsCategoryViewController] getNews, Unable to save news in codable files locally")
                    }
                    DispatchQueue.main.async {
                        self.news = self.onlineNews
                        if !loadedOffline {
                            self.setupPager()
                        } else {
                            self.reloadNews()
                        }
                    }
                }
            case .failure(let error):
                ProgressHUD.dismiss()
                Logger.shared.error("[NewsCategoryViewController] getNews, an error has occured: \(error.localizedDescription)")
                break
            }
        }
    }


    private func reloadNews() {
        guard let pageMenu = self.pageMenu else {
            return
        }
        for controller in pageMenu.controllerArray {
            guard let newsVC = controller as? NewsViewController else {
                continue
            }
            newsVC.news = self.news.filter { $0.categories?.contains(newsVC.category?.id ?? -1) ?? false
            }
            newsVC.reloadData()
        }
    }


    private func setupPager() {
        ProgressHUD.dismiss()
        let pageMenuConfig: [CAPSPageMenuOption] = [
            .SelectionIndicatorHeight(0.5)
            ,.MenuItemSeparatorWidth(0)
            ,.ScrollMenuBackgroundColor(.clear)
            ,.ViewBackgroundColor(.clear)
            ,.MenuMargin(15)
            ,.UseMenuLikeSegmentedControl(false)
            ,.MenuHeight(50)
            ,.MenuRadius(25)
            ,.CenterMenuItems(true)
            ,.HideTopMenuBar(false)
            ,.SelectedMenuItemLabelColor(.white)
            ,.UnselectedMenuItemLabelColor(.white)
            ,.MenuItemFont(.boldSystemFont(ofSize: 15))
            ,.SelectedMenuItemBackgroundColor(.clear)
            ,.UnselectedMenuItemBackgroundColor(.clear)
            ,.SelectionIndicatorColor(.white)
            ,.AddBottomMenuHairline(false)
            ,.MenuItemWidthBasedOnTitleTextWidth(true)
        ]

        var controllerArray = [UIViewController]()
        let stb = StoryboardProvider.instance.storyboard(for: .main)
        for category in self.categories {
            guard let controller1 = stb.instantiateViewController(
                withIdentifier: NewsViewController.storyboardId
            ) as? NewsViewController else {
                continue
            }
            controller1.title = category.name
            controller1.category = category
            controller1.news = self.news.filter {
                $0.categories?.contains(category.id ?? -1) ?? false
            }
            controllerArray.append(controller1)
        }
        self.pageMenu = CAPSPageMenu(
            viewControllers: controllerArray,
            frame: CGRect(
                x: 0,
                y: 88,
                width: self.view.frame.width,
                height: self.view.frame.height
            ),
            pageMenuOptions: pageMenuConfig
        )
        if let pageMenu = self.pageMenu {
            self.addChildViewController(pageMenu)
            pageMenu.view.frame = self.containerView.bounds
            pageMenu.view.autoresizesSubviews = false
            if Languages.isRightToLeft() {
                pageMenu.view.transform = CGAffineTransform(scaleX: -1, y: 1)
                pageMenu.menuItems.forEach { item in
                    item.transform = CGAffineTransform(scaleX: -1, y: 1)
                }
                pageMenu.controllerArray.forEach { viewController in
                    viewController.view.transform = CGAffineTransform(
                        scaleX: -1,
                        y: 1
                    )
                }
            }
            self.containerView.addSubview(pageMenu.view)
            pageMenu.didMove(toParentViewController: self)
        }
    }

}
