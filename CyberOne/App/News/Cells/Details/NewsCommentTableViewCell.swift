//
//  NewsCommentTableViewCell.swift
//  CyberOne
//
//  Created by Taoufik on 15/11/2018.
//  Copyright © 2018 Cyber One. All rights reserved.
//

import UIKit

class NewsCommentTableViewCell: UITableViewCell {
    static let identifier = "NewsCommentTableViewCell"
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var commentTime: UILabel!
    @IBOutlet weak var commentDetails: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var replyButton: UIButton!
    @IBOutlet weak var replyConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
