//
//  CommentsNumberTableViewCell.swift
//  CyberOne
//
//  Created by Taoufik on 15/11/2018.
//  Copyright © 2018 Cyber One. All rights reserved.
//

import UIKit

class CommentsNumberTableViewCell: UITableViewCell {
    static let identifier = "CommentsNumberTableViewCell"
    
    @IBOutlet weak var commentsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
