//
//  NewsTableViewCell.swift
//  CyberOne
//
//  Created by Taoufik on 14/11/2018.
//  Copyright © 2018 Cyber One. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {
    static let identifier = "NewsTableViewCell"
    
    
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var viewsCount: UILabel!
    @IBOutlet weak var likesCount: UILabel!
    @IBOutlet weak var categoryId: UILabel!

    var mainImageURL: URL?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentView.shadowWithCornder(raduis: 0, shadowColor: UIColor.lightGray.cgColor, shadowOffset: CGSize(width: 2, height: 2), shadowOpacity: 0.8, shadowRadius: 5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
