//
//  NewsViewController.swift
//  CyberOne
//
//  Created by Taoufik on 14/11/2018.
//  Copyright © 2018 Cyber One. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import SwiftyJSON
import ProgressHUD
import JustLog



final class NewsViewController: UIViewController {
    static let storyboardId = "NewsViewController"

    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak private var emptyLbl: UILabel!

    var news = [Post]()
    var category: Category?


    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = NEWS_STR
        self.emptyLbl.text = NO_NEWS_STR
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.emptyLbl.isHidden = self.news.count != 0
        self.tableView.reloadData()
    }


    /** Reload the data after refreshing data from remote */
    func reloadData() {
        if self.tableView != nil {
            self.emptyLbl.isHidden = self.news.count != 0
            self.tableView.reloadData()
        }
    }
}



extension NewsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.news.count
    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: NewsTableViewCell.identifier,
            for: indexPath
        ) as? NewsTableViewCell else {
            return UITableViewCell()
        }
        let news = self.news[indexPath.row]
        cell.newsTitle.textAlignment = .center
        cell.newsTitle.text = news.title?.rendered
        if let newsImage = news.yoastHeadJson?.ogImage?.first?.url {
            cell.newsImage.sd_setImage(with: URL(string: newsImage))
        }
        cell.categoryId.text = "\(self.category?.name ?? "")"
        cell.categoryId.isHidden = (self.category?.name ?? "").isEmpty
        return cell
    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let detailsVc = StoryboardProvider.instance.storyboard(for: .main).instantiateViewController(
            withIdentifier: NewsDetailsViewController.storyboardId
        ) as? NewsDetailsViewController else {
            return
        }
        detailsVc.news = news[indexPath.row]
        navigationController?.pushViewController(detailsVc, animated: true)
    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
}
