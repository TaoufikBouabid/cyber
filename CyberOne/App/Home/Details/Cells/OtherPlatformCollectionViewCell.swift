//
//  OtherPlatformCollectionViewCell.swift
//  CyberOne
//
//  Created by Taoufik on 9/29/19.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import UIKit



final class OtherPlatformCollectionViewCell: UICollectionViewCell {
    static let identifier = "otherPlatformCollectionViewCell"


    @IBOutlet weak var label: UILabel!
}
