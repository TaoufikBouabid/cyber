//
//  HomeDetailsCollectionViewCell.swift
//  CyberOne
//
//  Created by Taoufik on 07/01/2019.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import UIKit

class HomeDetailsCollectionViewCell: UICollectionViewCell {
    static let identifier = "HomeDetailsCollectionViewCell"
    
    @IBOutlet weak var image: UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentView.setCardView(cornerRadius: 10, borderColor: UIColor.clear.cgColor, borderWidth: 0, shadowOpacity: 0.8, shadowColor: UIColor.lightGray.cgColor, shadowRadius: 5, shadowOffset: CGSize(width: 2, height: 2))
        //contentView.shadowWithCornder(raduis: 10, shadowColor: UIColor.lightGray.cgColor, shadowOffset: CGSize(width: 2, height: 2), shadowOpacity: 0.8, shadowRadius: 5)
    }
}
