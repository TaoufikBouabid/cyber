//
//  ReportTableViewController+Media.swift
//  CyberOne
//
//  Created by Taoufik on 13/11/2022.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import Foundation
import TLPhotoPicker



extension ReportTableViewController {


    @IBAction private func pickerWithCustomCameraCell() {
        let photoPickerviewController = CustomPhotoPickerViewController()
        photoPickerviewController.delegate = self
        var configure = TLPhotosPickerConfigure()
        configure.numberOfColumn = 3
        configure.cameraCellNibSet = (
            nibName: Const.cameraCellNibSet,
            bundle: Bundle.main
        )
        photoPickerviewController.configure = configure
        photoPickerviewController.selectedAssets = self.selectedAssets
        self.present(
            photoPickerviewController.wrapNavigationControllerWithoutBar(),
            animated: true,
            completion: nil
        )
    }
}



extension ReportTableViewController: TLPhotosPickerViewControllerDelegate  {
    
    func dismissPhotoPicker(withTLPHAssets: [TLPHAsset]) {
        self.tableView.beginUpdates()
        self.selectedAssets = withTLPHAssets
        self.tableView.endUpdates()
        self.exportVideo()
    }


    private func exportVideo() {
        let datasource = self.selectedAssets.filter { $0.type == .video }
        datasource.forEach { $0.exportVideoFile(progressBlock: { _ in }) { (_, _) in } }
        self.imagesCollectionView.reloadData()
        self.videosCollectionView.reloadData()
    }

}
