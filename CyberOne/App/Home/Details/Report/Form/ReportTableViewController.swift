//
//  ReportTableViewController.swift
//  CyberOne
//
//  Created by Taoufik on 08/01/2019.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import UIKit
import AVFAudio
import TLPhotoPicker
import SZTextView



@objc protocol ReportTableViewControllerDelegate {
    @objc optional func didDismiss(_ report: FReport)
    @objc optional func didDismiss(_ report: FReport?, _ service: FService?, _ socialNetwork: FSocialNetworks?)
}



final class ReportTableViewController: UITableViewController {
    static let storyboardId = "ReportTableViewController"

    struct Const {
        static let android = "Android"
        static let cameraCellNibSet = "CustomCameraCell"
        static let mediaCollectionViewHeight: CGFloat = 150
        static let zeroHeight: CGFloat = 0
    }

    @IBOutlet weak var fullNameField: SZTextView!
    @IBOutlet weak var fullNameFieldHeight: NSLayoutConstraint!

    @IBOutlet weak var countryField: SZTextView!
    @IBOutlet weak var countryFieldHeight: NSLayoutConstraint!

    @IBOutlet weak var ageField: SZTextView!
    @IBOutlet weak var ageFieldHeight: NSLayoutConstraint!

    @IBOutlet weak var emailField: SZTextView!
    @IBOutlet weak var emailFieldHeight: NSLayoutConstraint!

    @IBOutlet weak var phoneNumberField: SZTextView!
    @IBOutlet weak var phoneNumberFieldHeight: NSLayoutConstraint!

    @IBOutlet weak var remarksField: SZTextView!
    @IBOutlet weak var remarksFieldHeight: NSLayoutConstraint!

    @IBOutlet weak var imagesCollectionView: UICollectionView!
    @IBOutlet weak var videosCollectionView: UICollectionView!
    @IBOutlet weak var dateButton: UIButton!
    @IBOutlet weak var secondDateButton: UIButton!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var imageButton: UIButton!

    @IBOutlet weak var accountNameOrLink: SZTextView!
    @IBOutlet weak var accountNameOrLinkHeight: NSLayoutConstraint!

    @IBOutlet weak var accountEmailAddress: SZTextView!
    @IBOutlet weak var accountEmailAddressHeight: NSLayoutConstraint!

    @IBOutlet weak var accountEmailPassword: SZTextView!
    @IBOutlet weak var accountEmailPasswordHeight: NSLayoutConstraint!

    @IBOutlet weak var accountLastPassword: SZTextView!
    @IBOutlet weak var accountLastPasswordHeight: NSLayoutConstraint!

    @IBOutlet weak var copyrightsField: SZTextView!
    @IBOutlet weak var copyrightsFieldHeight: NSLayoutConstraint!

    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var socialNetworkBorderView: UIView!
    @IBOutlet weak var removeAudio: UIButton!

    // Questions
    @IBOutlet weak var facebookYes: UIButton!
    @IBOutlet weak var facebookNo: UIButton!

    @IBOutlet weak var ownerYes: UIButton!
    @IBOutlet weak var ownerNo: UIButton!

    @IBOutlet weak var publishYes: UIButton!
    @IBOutlet weak var publishNo: UIButton!

    @IBOutlet weak var profilePictureYes: UIButton!
    @IBOutlet weak var profilePictureNo: UIButton!

    @IBOutlet weak var personBlackmaildYes: UIButton!
    @IBOutlet weak var personBlackmaildNo: UIButton!

    @IBOutlet weak var over18Yes: UIButton!
    @IBOutlet weak var over18No: UIButton!

    @IBOutlet weak var contentReal: UIButton!
    @IBOutlet weak var contentFake: UIButton!

    @IBOutlet weak var knowPasswordYes: UIButton!
    @IBOutlet weak var knowPasswordNo: UIButton!

    @IBOutlet weak var phoneTypeAndroid: UIButton!
    @IBOutlet weak var phoneTypeiOS: UIButton!

    @IBOutlet weak var harassesYes: UIButton!
    @IBOutlet weak var harassesNo: UIButton!
    @IBOutlet weak var socialNetworkLabel: UILabel!

    @IBOutlet weak var imageAttachementLabel: UILabel!
    @IBOutlet weak var firstHarrassementYes: UIButton!
    @IBOutlet weak var firstHarrassementNo: UIButton!

    @IBOutlet weak var sexualHarrassementYes: UIButton!
    @IBOutlet weak var sexualHarrassementNo: UIButton!

    @IBOutlet weak var doubtPersonYes: UIButton!
    @IBOutlet weak var doubtPersonNo: UIButton!

    @IBOutlet weak var privateBtn: UIButton!
    @IBOutlet weak var workBtn: UIButton!

    @IBOutlet weak var socialNetworkBg: UIView!
    @IBOutlet weak var subscriptionLogo: UIImageView!
    @IBOutlet weak var recordLabel: UILabel!

    var facebookSelected = false
    var ownerSelected = false
    var publishSelected = false
    var profilePictureSelected = false
    var personBlackmaildSelected = false
    var over18Selected = false
    var contentRealSelected = false
    var knowPasswordSelected = false
    var phoneTypeSelected = false
    var harassesSelected = false
    var firstHarrassementSelected = false
    var sexualHarrassementSelected = false
    var doubtPersonSelected = false
    var privateSelected = false

    var selectedAssets = [TLPHAsset]()
    var isHaveCopyrights = false
    var isPermission = false
    var socialNetwork: FSocialNetworks?
    var report: FReport!
    var photosURL = [String]()
    var videosURL = [String: String]()
    var isContainsVideos = false
    var delegate: ReportTableViewControllerDelegate?
    var selectedDate = Date()
    var audioPath: URL?
    var service: FService?
    var subscription: FSubscription?
    var serviceImage: UIImage?

    // Record Audio
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var player: AVAudioPlayer?
    var playerTimeInterval: TimeInterval?
    var isFirstTimePlayAudio = true

    //MARK :- IndexPaths
    
    // COMMON ROWS
    let headerIndexPath = IndexPath(row: 0, section: 0)
    let fullNameIndexPath = IndexPath(row: 1, section: 0)
    let countryIndexPath = IndexPath(row: 2, section: 0)
    let ageIndexPath = IndexPath(row: 3, section: 0)
    let emailIndexPath = IndexPath(row: 4, section: 0)
    let phoneIndexPath = IndexPath(row: 5, section: 0)
    let commonIndexPaths = [
        IndexPath(row: 0, section: 0), //headerIndexPath
        IndexPath(row: 1, section: 0), //fullNameIndexPath
        IndexPath(row: 2, section: 0), //countryIndexPath
        IndexPath(row: 3, section: 0), //ageIndexPath
        IndexPath(row: 4, section: 0), //emailIndexPath
        IndexPath(row: 5, section: 0), //phoneIndexPath
        IndexPath(row: 18, section: 0), //descriptionMediaIndexPath
        //IndexPath(row: 19, section: 0), //descriptionAttachIndexPath
        //IndexPath(row: 20, section: 0), //attachImagesIndexPath
        //IndexPath(row: 23, section: 0), //descriptionAudioIndexPath
        //IndexPath(row: 24, section: 0), //audioIndexPath
        IndexPath(row: 34, section: 0), //descriptionRemarksIndexPath
        IndexPath(row: 35, section: 0), //remarksIndexPath
        IndexPath(row: 36, section: 0)
    ] //submitIndexPath

    // DYNAMIC ROWS
    let accountHackedDate = IndexPath(row: 6, section: 0)
    let knowPasswordIndexPath = IndexPath(row: 7, section: 0)
    let accountEmailIndexPath = IndexPath(row: 8, section: 0)
    let accountPasswordIndexPath = IndexPath(row: 9, section: 0)
    let accountLastPasswordIndexPath = IndexPath(row: 10, section: 0)
    let phoneTypeIndexPath = IndexPath(row: 11, section: 0)
    let facebookAccountIndexPath = IndexPath(row: 12, section: 0)
    let copyrightsOwnerIndexPath = IndexPath(row: 13, section: 0)
    let copyrightsLinkIndexPath = IndexPath(row: 14, section: 0)
    let accountNameIndexPath = IndexPath(row: 15, section: 0)
    let personalPhotosIndexPath = IndexPath(row: 16, section: 0)
    let permissionIndexPath = IndexPath(row: 17, section: 0)
    let descriptionMediaIndexPath = IndexPath(row: 18, section: 0)
    let descriptionAttachIndexPath = IndexPath(row: 19, section: 0)
    let attachImagesIndexPath = IndexPath(row: 20, section: 0)
    let videosIndexPath = IndexPath(row: 21, section: 0)
    let imagesIndexPath = IndexPath(row: 22, section: 0)
    let descriptionAudioIndexPath = IndexPath(row: 23, section: 0)
    let audioIndexPath = IndexPath(row: 24, section: 0)
    let extortsIndexPath = IndexPath(row: 25, section: 0)
    let harassesIndexPath = IndexPath(row: 26, section: 0)
    let over18IndexPath = IndexPath(row: 27, section: 0)
    let contentRealIndexPath = IndexPath(row: 28, section: 0)
    let fakeAccountCreationIndexPath = IndexPath(row: 29, section: 0)
    let firstHarrassementIndexPath = IndexPath(row: 30, section: 0)
    let sexualHarrassementIndexPath = IndexPath(row: 31, section: 0)
    let doubtPersonIndexPath = IndexPath(row: 32, section: 0)
    let contentIndexPath = IndexPath(row: 33, section: 0)

    let descriptionRemarksIndexPath = IndexPath(row: 34, section: 0)
    let remarksIndexPath = IndexPath(row: 35, section: 0)
    let submitIndexPath = IndexPath(row: 36, section: 0)


    override func viewDidLoad() {
        self.setupPlaceholders()
        super.viewDidLoad()
        self.setupNavBar()
        self.setupRTL()
        self.setupBanner()
        self.setupReport()
        self.setupSocialNetworkLabel()
        self.setupCollectionViewsDelegates()
        self.setFieldsDelegates()
    }


    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UserDefaults.standard.removeObject(forKey: "audioRecorded")
    }

}



extension ReportTableViewController: UITextViewDelegate {

    func textViewDidChange(_ textView: UITextView) {
        switch textView {
        case self.fullNameField:
            self.reloadFieldHeight(
                self.fullNameField,
                self.fullNameFieldHeight
            )
            break
        case self.countryField:
            self.reloadFieldHeight(
                self.countryField,
                self.countryFieldHeight
            )
            break
        case self.ageField:
            self.reloadFieldHeight(
                self.ageField,
                self.ageFieldHeight
            )
            break
        case self.emailField:
            self.reloadFieldHeight(
                self.emailField,
                self.emailFieldHeight
            )
            break

        case self.phoneNumberField:
            self.reloadFieldHeight(
                self.phoneNumberField,
                self.phoneNumberFieldHeight
            )
            break
        case self.accountEmailAddress:
            self.reloadFieldHeight(
                self.accountEmailAddress,
                self.accountEmailAddressHeight
            )
            break
        case self.accountEmailPassword:
            self.reloadFieldHeight(
                self.accountEmailPassword,
                self.accountEmailPasswordHeight
            )
            break
        case self.accountLastPassword:
            self.reloadFieldHeight(
                self.accountLastPassword,
                self.accountLastPasswordHeight
            )
            break
        case self.copyrightsField:
            self.reloadFieldHeight(
                self.copyrightsField,
                self.copyrightsFieldHeight
            )
            break
        case self.accountNameOrLink:
            self.reloadFieldHeight(
                self.accountNameOrLink,
                self.accountNameOrLinkHeight
            )
            break
        case self.remarksField:
            self.reloadFieldHeight(
                self.remarksField,
                self.remarksFieldHeight
            )
            break
        default:
            break
        }
    }


    private func reloadFieldHeight(
        _ textView: SZTextView,
        _ constraint: NSLayoutConstraint
    ) {
        self.tableView.beginUpdates()
        constraint.constant = textView.contentSize.height < 60 ? 60 :  textView.contentSize.height
        self.tableView.endUpdates()
    }

}
