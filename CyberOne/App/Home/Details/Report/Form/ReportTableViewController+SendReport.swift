//
//  ReportTableViewController+SendReport.swift
//  CyberOne
//
//  Created by Taoufik on 13/11/2022.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import Foundation
import ProgressHUD
import JustLog



extension ReportTableViewController {

    @IBAction func sendAction(_ sender: UIButton) {
        self.report == nil ? self.sendReport() : self.showSteps()
    }


    func showSteps() {
        self.navigationController?.popViewController(animated: true)
        guard let delegate = self.delegate else {
            Logger.shared.error("[ReportTableViewController+SendReport] Unable to correctly display steps")
            return
        }
        delegate.didDismiss?(
            self.report,
            self.service,
            self.socialNetwork
        )
    }


    func sendReport() {
        if !self.validationAction() {
            return
        }
        self.report = self.generateReport()
        ProgressHUD.show()
        self.report.saveInBackground { (error) in
            if let error = error {
                Logger.shared.error("[ReportTableViewController] sendReport, error: \(error.localizedDescription)")
            }
        }
        self.uploadMedia()
    }


    func generateReport() -> FReport {
        let dateFormatter = DateFormatterProvider.gmtDateFormatter

        let report = FReport(
            fullName: self.fullNameField.text ?? "",
            address: self.countryField.text ?? "",
            dateOfBirth: self.ageField.text ?? "",
            email: self.emailField.text ?? "",
            phoneNumber: self.phoneNumberField.text ?? "",
            notes: self.remarksField.text ?? "",
            hasFacebookAccount: self.facebookSelected,
            fakeAccount: false,
            usesPersonalPhotos: self.profilePictureSelected,
            fakeAccountPhoto: "",
            statusVoiceRecording: "", // object will be updated with audio afterwards
            attachments: "", // object will be updated with pictures afterwards
            accountHackedDate: self.dateButton.titleLabel?.text ?? "",
            someoneKnowsPassword: self.knowPasswordSelected,
            hackedAccountEmail: self.accountEmailAddress.text ?? "",
            hackedAccountEmailPassword: self.accountEmailPassword.text ?? "",
            hackedAccountLastPassword: self.accountLastPassword.text ?? "",
            phoneType: self.phoneTypeSelected ? "Android" : "iOS",
            isLegalCopyrightOwner: self.ownerSelected,
            copyrightsViolationLink: self.copyrightsField.text ?? "",
            copyrightsPublishedWithoutPermission: self.publishSelected,
            someoneUnknownHarassesYou: self.harassesSelected,
            doYouKnowWhoExtortsYou: self.personBlackmaildSelected,
            olderThan18: self.over18Selected,
            isContentReal: self.contentRealSelected,
            socialNetwork: self.socialNetwork?.name ?? "",
            objectId: randomString(length: 10),
            status: 0,
            userId: FUser.currentUserId() ?? "",
            serviceId: self.service?.objectId ?? "",
            videos: "", // object will be updated with videos afterwards
            isClosed: false,
            remarks: self.remarksField.text ?? "",
            creationDate: dateFormatter.string(from: Date()),
            type: self.subscription == nil ? "0" : "1",
            subscription: self.subscription?.title ?? "",
            subscriptionLogo: self.subscription?.background ?? "",
            serviceType: self.service?.id ?? 6,
            socialNetworkLogo: self.socialNetwork?.logo ?? "",
            accountNameOrLink: self.accountNameOrLink.text ?? "",
            firstHarrassement: self.firstHarrassementSelected,
            sexualHarrassement: self.sexualHarrassementSelected,
            doubtPerson: self.doubtPersonSelected,
            privateOrWork: self.privateSelected
        )
        return report
    }

}
