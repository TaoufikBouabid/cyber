//
//  ReportTableViewController+Calendar.swift
//  CyberOne
//
//  Created by Taoufik on 13/11/2022.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import Foundation
import STPopup



extension ReportTableViewController {

    @IBAction func presentCalendar(_ sender: UIButton) {
        let calendarViewController = CalendarViewController()
        calendarViewController.delegate = self
        calendarViewController.selectedDate = self.selectedDate
        let calendarPopup = STPopupController(rootViewController: calendarViewController)
        calendarPopup.containerView.layer.cornerRadius = 14
        calendarPopup.transitionStyle = .slideVertical
        calendarPopup.present(in: self)
    }
}



extension ReportTableViewController: CalendarViewControllerDelegate {
    func didSelectDate(_ date: Date) {
        let dateFormatter = DateFormatterProvider.dayMonthYearDateFormatter
        self.selectedDate = date
        self.dateButton.setTitle(
            dateFormatter.string(from: date),
            for: .normal
        )
        self.secondDateButton.setTitle(
            dateFormatter.string(from: date),
            for: .normal
        )
    }

}
