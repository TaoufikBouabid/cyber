//
//  ReportTableViewController+Country.swift
//  CyberOne
//
//  Created by Taoufik on 13/11/2022.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import Foundation
import STPopup



extension ReportTableViewController {

    @IBAction func showLocations(_ sender: UIButton) {
        if self.report != nil { return }
        let countryVC = CountrySelectorViewController(
            nibName: CountrySelectorViewController.identifier,
            bundle: nil
        )
        countryVC.delegate = self
        let countrySelector = STPopupController(rootViewController: countryVC)
        countrySelector.present(in: self)
    }
}



extension ReportTableViewController: CountrySelectorDelegate {
    func didSelectCountry(country: Country, viewController: UIViewController) {
        self.countryField.text = Languages.currentLanguage() == "ar" ? country.nameAr : country.nameEn
    }
}
