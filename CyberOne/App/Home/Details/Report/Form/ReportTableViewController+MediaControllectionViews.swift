//
//  ReportTableViewController+MediaControllectionViews.swift
//  CyberOne
//
//  Created by Taoufik on 13/11/2022.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import Foundation
import TLPhotoPicker
import Lightbox



extension ReportTableViewController {

    func setupCollectionViewsDelegates() {
        self.imagesCollectionView.delegate = self
        self.videosCollectionView.delegate = self
    }
}



extension ReportTableViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.report == nil {
            if collectionView == self.imagesCollectionView {
                return self.selectedAssets.filter { $0.type == .photo }.count
            }
            return self.selectedAssets.filter { $0.type == .video }.count
        } else {
            if collectionView == self.imagesCollectionView {
                return self.report.attachments.components(separatedBy: ",").count
            }
            return self.videosURL.count
        }
    }


    func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath
    ) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: AssetCollectionViewCell.identifier,
            for: indexPath
        ) as? AssetCollectionViewCell else {
            return UICollectionViewCell()
        }
        if self.report == nil {
            if collectionView == self.imagesCollectionView {
                let datasource = self.selectedAssets.filter { $0.type == TLPHAsset.AssetType.photo }
                cell.assetImage.image = datasource[indexPath.item].fullResolutionImage
                cell.asset = datasource[indexPath.item]
                cell.controller = self
                return cell
            }

            let datasource = self.selectedAssets.filter { $0.type == TLPHAsset.AssetType.video }
            cell.assetImage.image = datasource[indexPath.item].fullResolutionImage
            cell.asset = datasource[indexPath.item]
            if let fileName = datasource[indexPath.item].originalFileName {
                cell.videoPath = FileManager.default.temporaryDirectory.appendingPathComponent(fileName).absoluteString
            }
            cell.controller = self
        } else {
            if collectionView == self.imagesCollectionView {
                cell.assetImage.sd_setImage(with: URL(string: self.report.attachments.components(separatedBy: ",")[indexPath.item]))
                cell.removeButton.isHidden = true
                return cell
            }
            let keys = self.videosURL.compactMap { $0.key }
            if indexPath.item < keys.count {
                cell.assetImage.sd_setImage(with: URL(string: keys[indexPath.item]))
                cell.removeButton.isHidden = true
            }
        }
        return cell
    }


    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.imagesCollectionView {
            if let cell = self.imagesCollectionView.cellForItem(at: indexPath) as? AssetCollectionViewCell {
                if self.report == nil {
                    self.displayLightbox(
                        startIndex: self.selectedAssets.firstIndex(of: cell.asset) ?? 0
                    )
                } else if let url = URL(string: self.report.attachments.components(separatedBy: ",")[indexPath.item]) {
                    let controller = LightboxController(
                        images: [LightboxImage(imageURL: url)]
                    )
                    controller.dynamicBackground = true
                    self.present(controller, animated: true, completion: nil)
                }
            }
        } else if collectionView == self.videosCollectionView {
            if let cell = self.videosCollectionView.cellForItem(at: indexPath) as? AssetCollectionViewCell {
                if self.report == nil {
                    self.displayLightbox(
                        startIndex: self.selectedAssets.firstIndex(of: cell.asset) ?? 0
                    )
                } else {
                    let keys = self.videosURL.compactMap { $0.key }
                    if let videoURL = URL(string: self.videosURL[keys[indexPath.item]] ?? ""),
                       let imageURL = URL(string: keys[indexPath.item]) {
                        let controller = LightboxController(
                            images: [LightboxImage(
                                imageURL: imageURL,
                                videoURL: videoURL
                            )]
                        )
                        controller.dynamicBackground = true
                        self.present(controller, animated: true, completion: nil)
                    }
                }
           }
        }
    }


    private func displayLightbox(startIndex: Int) {
        var lighboxArray = [LightboxImage]()
        for asset in self.selectedAssets {
            guard let image = asset.fullResolutionImage else {
                continue
            }
            lighboxArray.append(
                LightboxImage(
                    image: image,
                    text: (self.service?.title ?? self.subscription?.title) ?? "",
                    videoURL: asset.type == .video ? FileManager.default.temporaryDirectory.appendingPathComponent(asset.originalFileName ?? "") : nil
                )
            )
        }
        let controller = LightboxController(
            images: lighboxArray,
            startIndex: startIndex
        )
        controller.dynamicBackground = true
        self.present(controller, animated: true, completion: nil)
    }

}
