//
//  ReportTableViewController+AudioRecorderAndPlayer.swift
//  CyberOne
//
//  Created by Taoufik on 13/11/2022.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import Foundation
import AVFAudio
import JustLog
import AVFoundation



extension ReportTableViewController {

    func setupPlayerLabelAndButton() {
        self.recordLabel.text = LISTEN_STR
        self.recordButton.addTarget(
            self,
            action: #selector(self.playAudioAction(_:)),
            for: .touchUpInside
        )
        self.sendButton.setTitle(STEPS_STR, for: .normal)
    }


    @objc private func playAudioAction(_ sender: UIButton) {
        if self.report != nil {
            if !report.statusVoiceRecording.isEmpty {
                self.recordLabel.text = LISTEN_STR
                self.playAudio()
            }
        }
    }


    func setupRecorder() {
        self.recordingSession = AVAudioSession.sharedInstance()
        do {
            try self.recordingSession.setCategory(
                AVAudioSessionCategoryPlayAndRecord,
                mode: AVAudioSessionModeDefault,
                options: []
            )
            try recordingSession.setActive(true)
            self.recordingSession.requestRecordPermission() { _ in }
        } catch {
            Logger.shared.error("[ReportTableViewController+PrepareObjs] Unable to setup the recorder, error: \(error.localizedDescription)")
        }
    }


    @objc func recordAudioAction(_ sender: UIButton) {
        if self.audioRecorder == nil  {
            // record audio
            self.recordLabel.text = RECORDING_STR
            self.startRecordingAudio()

        } else {
            // play audio
            self.recordLabel.text = LISTEN_STR
            self.finishRecording(error: nil)
            if !self.isFirstTimePlayAudio {
                self.playAudio()
            } else {
                self.isFirstTimePlayAudio = false
            }
        }
    }


    func startRecordingAudio() {
        self.audioPath = self.getDocumentsDirectory()
            .appendingPathComponent("recording.m4a")
        guard let audioPath = self.audioPath else { return }
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        do {
            self.audioRecorder = try AVAudioRecorder(
                url: audioPath,
                settings: settings
            )
            self.audioRecorder.delegate = self
            self.audioRecorder.record()
        } catch {
            self.finishRecording(error: error)
        }
    }


    private func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(
            for: .documentDirectory,
            in: .userDomainMask
        )
        return paths[0]
    }


    private func finishRecording(error: Error?) {
        self.audioRecorder.stop()
        if let error = error {
            self.recordLabel.text = RECORD_STR
            Logger.shared.error("[ReportTableViewController+AudioRecordAndPlayer] unable to record, error: \(error.localizedDescription)")
        } else {
            self.removeAudio.isHidden = false
            self.recordLabel.text = LISTEN_STR
        }
    }


    private func playAudio() {
        if self.report == nil {
            guard let url = self.audioPath else { return }
            if let player = self.player {
                if player.isPlaying {
                    self.recordLabel.text = PLAY_STR
                    player.stop()
                } else {
                    self.recordLabel.text = PAUSE_STR
                    player.play()
                }
            } else {
                do {
                    try AVAudioSession.sharedInstance().setCategory(
                        AVAudioSessionCategoryPlayback,
                        mode: AVAudioSessionModeDefault
                    )
                    try AVAudioSession.sharedInstance().setActive(true)
                    self.player = try AVAudioPlayer(
                        contentsOf: url,
                        fileTypeHint: AVFileType.m4a.rawValue
                    )
                    self.player?.delegate = self
                    guard let player = self.player else { return }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                        player.play()
                        self.recordLabel.text = PAUSE_STR
                    })
                } catch let error {
                    Logger.shared.error("\(#function) error: \(error.localizedDescription)")
                }
            }
        } else {
            guard let url = URL(string: self.report.statusVoiceRecording) else { return }
            if let player = self.player {
                if player.isPlaying {
                    self.recordLabel.text = PLAY_STR
                    player.stop()
                } else {
                    self.recordLabel.text = PAUSE_STR
                    player.play()
                }
            } else {
                URLSession(configuration: .default).dataTask(with: URLRequest(url: url)) { data, _, error in
                    if let data = data {
                        do {
                            self.player = try AVAudioPlayer(data: data)
                            guard let player = self.player else { return }
                            player.delegate = self
                            DispatchQueue.main.asyncAfter(deadline: .now()+0.3, execute: {
                                player.play()
                                self.recordLabel.text = PAUSE_STR
                            })
                        } catch {
                            Logger.shared.error("\(#function) error: \(error.localizedDescription)")
                        }
                    } else if let error = error {
                        Logger.shared.error("\(#function) error: \(error.localizedDescription)")
                    }
                }.resume()
            }
        }
    }


    @IBAction private func deleteAction(_ sender: UIButton) {
        self.showAlertView(
            isOneButton: false,
            title: REMOVE_AUDIO_RECORD_STR,
            cancelButtonTitle: CANCEL_STR,
            submitButtonTitle: OK_STR,
            cancelHandler: nil
        ) {
            if self.report == nil {
                if let audioPath = self.audioPath {
                    do{
                        try FileManager.default.removeItem(at: audioPath)
                        self.audioPath = nil
                        self.player = nil
                        self.recordLabel.text = RECORD_STR
                        self.removeAudio.isHidden = true
                        self.audioRecorder = nil
                        self.isFirstTimePlayAudio = true
                    } catch {
                        Logger.shared.error("[ReportTableViewController+AudioRecorderAndPlayer] unable to delete the audio file: \(error.localizedDescription)")
                    }
                }
            }
        }
    }
}



extension ReportTableViewController: AVAudioRecorderDelegate {
    private func audioRecorderDidFinishRecording(
        _ recorder: AVAudioRecorder,
        successfully flag: Bool
    ) {
        if !flag {
            // set a random error key
            self.finishRecording(error: NSError.description("Something went wrong when finishing the recording", code: 5432))
        }
    }
}



extension ReportTableViewController: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        self.recordLabel.text = LISTEN_STR
    }
}
