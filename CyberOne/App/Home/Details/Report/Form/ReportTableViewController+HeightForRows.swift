//
//  ReportTableViewController+HeightForRows.swift
//  CyberOne
//
//  Created by Taoufik on 13/11/2022.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import Foundation
import TLPhotoPicker



extension ReportTableViewController {

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.heightForRowAtCommonRows(tableView, indexPath: indexPath)
    }


    private func heightForRowAtCommonRows(_ tableView: UITableView, indexPath: IndexPath) -> CGFloat {

        if indexPath.row == 0 || indexPath.row == 36 {
            return super.tableView(tableView, heightForRowAt: indexPath)
        }

        if self.report == nil {
            if indexPath == self.videosIndexPath {
                if self.selectedAssets.filter({ $0.type == TLPHAsset.AssetType.video }).count == 0 {
                    return Const.zeroHeight
                } else {
                    return Const.mediaCollectionViewHeight
                }
            }
            
            if indexPath == self.imagesIndexPath {
                if self.selectedAssets.filter({ $0.type == TLPHAsset.AssetType.photo
                }).count == 0 {
                    return Const.zeroHeight
                } else {
                    return Const.mediaCollectionViewHeight
                }
            }

            if indexPath == self.attachImagesIndexPath {
                return Const.mediaCollectionViewHeight
            }
            if indexPath == self.descriptionAttachIndexPath ||
                indexPath == self.descriptionAudioIndexPath ||
                indexPath == self.audioIndexPath {
                return super.tableView(tableView, heightForRowAt: indexPath)
            }

        } else {
            if indexPath == self.descriptionAttachIndexPath {
                return Const.zeroHeight
            }
            if indexPath == self.attachImagesIndexPath {
                return Const.zeroHeight
            }
            if indexPath == self.imagesIndexPath {
                return self.report.attachments.isEmpty ? Const.zeroHeight : Const.mediaCollectionViewHeight
            }
            if indexPath == self.videosIndexPath {
                return self.report.videos.isEmpty ? Const.zeroHeight : Const.mediaCollectionViewHeight
            }
            if indexPath == self.descriptionAudioIndexPath
                || indexPath == self.audioIndexPath {
                return self.report.statusVoiceRecording.isEmpty ? Const.zeroHeight : super.tableView(tableView, heightForRowAt: indexPath)
            }
        }
        
        if self.commonIndexPaths.contains(indexPath) {
            return UITableViewAutomaticDimension// super.tableView(tableView, heightForRowAt: indexPath)
        }
        
        return self.heightWithSpecifiedService(tableView, indexPath: indexPath)
    }


    private func heightWithSpecifiedService(_ tableView: UITableView, indexPath: IndexPath) -> CGFloat {
        if self.service != nil {
            switch self.service?.id {
            case ServiceType.impersonation.rawValue :
                // Impersonation
                return self.heightForRowAtImpersonation(tableView, indexPath: indexPath)
            case ServiceType.extortion.rawValue :
                // Extortion
                return self.heightForRowAtExtortion(tableView, indexPath: indexPath)
            case ServiceType.copyrights.rawValue :
                // copyrights
                return self.heightForRowAtCopyrights(tableView, indexPath: indexPath)
            case ServiceType.harassement.rawValue :
                // Harassement
                return self.heightForRowAtHarassement(tableView, indexPath: indexPath)
            case ServiceType.hackedAccount.rawValue :
                // Hacked account
                return self.heightForRowAtHacked(tableView, indexPath: indexPath)
            case ServiceType.other.rawValue :
                // Other issues
                return self.heightForRowAtOther(tableView, indexPath: indexPath)
            case .none:
                return super.tableView(tableView, heightForRowAt: indexPath)
            case .some(_):
                return super.tableView(tableView, heightForRowAt: indexPath)
            }
        }
        return self.heightForRowAtOther(tableView, indexPath: indexPath)
    }


    private func heightForRowAtCopyrights(_ tableView: UITableView, indexPath: IndexPath) -> CGFloat {
        if !(indexPath == self.copyrightsOwnerIndexPath
             || indexPath == self.copyrightsLinkIndexPath
             || indexPath == self.permissionIndexPath
             || indexPath == self.over18IndexPath
             || indexPath == self.contentIndexPath) {
            return Const.zeroHeight
        }
        self.imageAttachementLabel.text = COPYRIGHTS_ATTACHEMENT_STR
        return UITableViewAutomaticDimension//super.tableView(tableView, heightForRowAt: indexPath)
    }


    private func heightForRowAtExtortion(_ tableView: UITableView, indexPath: IndexPath) -> CGFloat {
        if !(indexPath == self.accountNameIndexPath
             || indexPath == self.extortsIndexPath
             || indexPath == self.over18IndexPath
             || indexPath == self.contentRealIndexPath) {
            return Const.zeroHeight
        }
        self.accountNameOrLink.placeholder = EXTORTION_ACCOUNT_STR
        self.imageAttachementLabel.text = EXTORTION_ATTACHEMENT_STR
        return UITableViewAutomaticDimension//super.tableView(tableView, heightForRowAt: indexPath)
    }


    private func heightForRowAtHacked(_ tableView: UITableView, indexPath: IndexPath) -> CGFloat {
        if !(indexPath == self.accountHackedDate
             || indexPath == self.knowPasswordIndexPath
             || indexPath == self.accountEmailIndexPath
             || indexPath == self.accountPasswordIndexPath
             || indexPath == self.accountLastPasswordIndexPath
             || indexPath == self.phoneTypeIndexPath
             || indexPath == self.accountNameIndexPath) {
            return Const.zeroHeight
        }
        self.accountNameOrLink.placeholder = hacked_ACCOUNT_STR
        self.imageAttachementLabel.text = hacked_ATTACHEMENT_STR
        return UITableViewAutomaticDimension//super.tableView(tableView, heightForRowAt: indexPath)
    }


    private func heightForRowAtHarassement(_ tableView: UITableView, indexPath: IndexPath) -> CGFloat {
        if !(indexPath == self.harassesIndexPath
             || indexPath == self.over18IndexPath
             || indexPath == self.firstHarrassementIndexPath
             || indexPath == self.sexualHarrassementIndexPath
             || indexPath == self.doubtPersonIndexPath) {
            return Const.zeroHeight
        }
        self.accountNameOrLink.placeholder = HARRASSEMENT_ACCOUNT_STR
        self.imageAttachementLabel.text = EXTORTION_ATTACHEMENT_STR
        return UITableViewAutomaticDimension//super.tableView(tableView, heightForRowAt: indexPath)
    }


    private func heightForRowAtImpersonation(_ tableView: UITableView, indexPath: IndexPath) -> CGFloat {
        if !(indexPath == self.facebookAccountIndexPath
             || indexPath == self.over18IndexPath
             || indexPath == self.accountNameIndexPath
             || indexPath == self.personalPhotosIndexPath
             || indexPath == self.fakeAccountCreationIndexPath) {
            return Const.zeroHeight
        }
        self.accountNameOrLink.placeholder = IMPERSONATION_ACCOUNT_STR
        self.imageAttachementLabel.text = IMPERSONATION_ATTACHEMENT_STR
        return UITableViewAutomaticDimension// super.tableView(tableView, heightForRowAt: indexPath)
    }


    func heightForRowAtOther(_ tableView: UITableView, indexPath: IndexPath) -> CGFloat {
        return Const.zeroHeight
    }

}
