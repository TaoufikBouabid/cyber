//
//  ReportTableViewController+PrepareObjs.swift
//  CyberOne
//
//  Created by Taoufik on 13/11/2022.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import Foundation
import JustLog
import AVFAudio



extension ReportTableViewController {

    enum BannerImageType {
        case socialNetwork
        case service
        case subscription
    }


    func setupNavBar() {
        self.tableView.contentInsetAdjustmentBehavior = .never
        self.navigationController?.navigationBar.barTintColor = UIColor(hex: 0x1C61AE)
    }


    /** Setup the banner */
    func setupBanner() {
        if let socialNetwork = self.socialNetwork,
           let logo = socialNetwork.logo {
            self.setupBannerImage(
                with: URL(string: logo),
                type: .socialNetwork
            )
        } else if let background = self.service?.background {
            self.setupBannerImage(
                with: URL(string: background),
                type: .service
            )
        } else if let subscription = self.subscription,
                  let image = subscription.background {
            self.setupBannerImage(
                with: URL(string: image),
                type: .subscription
            )
        } else if let serviceImage = self.serviceImage {
            self.setupBannerImage(
                image: serviceImage,
                type: .service
            )
        }
    }


    /** Setup the image in the banner */
    private func setupBannerImage(
        with url: URL? = nil,
        image: UIImage? = nil,
        type: BannerImageType
    ) {
        self.socialNetworkBorderView.layer.borderColor = UIColor.white.cgColor
        self.socialNetworkBorderView.layer.borderWidth = 3
        self.socialNetworkBorderView.layer.cornerRadius = self.socialNetworkBorderView.frame.height / 2
        switch type {
        case .socialNetwork:
            self.logo.sd_setImage(with: url)
            self.logo.image = self.logo.image?.withRenderingMode(.alwaysTemplate)
            self.logo.tintColor = .white
            break
        case .service, .subscription:
            self.socialNetworkBg.isHidden = false
            if let image = image {
                self.subscriptionLogo.image = image
            } else {
                self.subscriptionLogo.sd_setImage(with: url)
            }
            self.subscriptionLogo.layer.cornerRadius = self.subscriptionLogo.frame.height / 2
            self.subscriptionLogo.contentMode = .scaleAspectFill
            break
        }
    }


    func setupReport() {
        if self.report == nil {
            self.prefillReportWithCurrentUserInfo()
            self.setupRecorder()
            self.recordButton.addTarget(
                self,
                action: #selector(self.recordAudioAction(_:)),
                for: .touchUpInside
            )
            self.prepareYesNoButtons()
        } else {
            self.tableView.isScrollEnabled = true
            self.setupPlayerLabelAndButton()
            self.fillDataWithReportInformations()
            self.disableInputs()
            if let subscription = self.report.subscriptionLogo {
                self.setupBannerImage(
                    with: URL(string: subscription),
                    type: .subscription
                )
            }
            self.setupHeights()
        }
    }


    func setupPlaceholders() {
        self.fullNameField.placeholder = NAME_PLACEHOLDER_STR
        self.countryField.placeholder = COUNTRY_PLACEHOLDER_STR
        self.ageField.placeholder = AGE_PLACEHOLDER_STR
        self.emailField.placeholder = EMAIL_PLACEHOLDER_STR
        self.phoneNumberField.placeholder = PHONE_NUMBER_PLACEHOLDER_STR
        self.remarksField.placeholder = REMARKS_PLACEHOLDER_STR
        self.accountNameOrLink.placeholder = ACCOUNT_NAME_OR_LINK_PLACEHOLDER_STR
        self.accountEmailAddress.placeholder = ACOUNT_EMAIL_ADDRESS_PLACEHOLDER_STR
        self.accountEmailPassword.placeholder = ACCOUNT_EMAIL_PASSWORD_PLACEHOLDER_STR
        self.accountLastPassword.placeholder = ACCOUNT_EMAIL_LAST_PASSWORD_PLACEHOLDER_STR
        self.copyrightsField.placeholder = COPYRIGHTS_PLACEHOLDER_STR
    }


    private func setupHeights() {
        self.fullNameFieldHeight.constant = self.fullNameField.contentSize.height
        self.countryFieldHeight.constant = self.countryField.contentSize.height
        self.ageFieldHeight.constant = self.ageField.contentSize.height
        self.emailFieldHeight.constant = self.emailField.contentSize.height
        self.phoneNumberFieldHeight.constant = self.phoneNumberField.contentSize.height
        self.remarksFieldHeight.constant = self.remarksField.contentSize.height
        self.accountNameOrLinkHeight.constant = self.accountNameOrLink.contentSize.height
        self.accountEmailAddressHeight.constant = self.accountEmailAddress.contentSize.height
        self.accountEmailPasswordHeight.constant = self.accountEmailPassword.contentSize.height
        self.accountLastPasswordHeight.constant = self.accountLastPassword.contentSize.height
        self.copyrightsFieldHeight.constant = self.copyrightsField.contentSize.height
    }


    private func prefillReportWithCurrentUserInfo() {
        self.fullNameField.text = FUser.currentUserName()
        self.countryField.text = FUser.currentUserCountry()
        self.ageField.text = "\(FUser.currentUserAge() ?? 0)"
        self.phoneNumberField.text = FUser.currentUserPhoneNumber() != nil ? String(FUser.currentUserPhoneNumber()!) : nil
        self.emailField.text = FUser.currentUserEmail()
        self.setupDateFields()
    }


    private func setupDateFields(_ date: Date = Date()) {
        let dateFormatter = DateFormatterProvider.dayMonthYearDateFormatter
        self.dateButton.setTitle(
            dateFormatter.string(from: date),
            for: .normal
        )
        self.secondDateButton.setTitle(
            dateFormatter.string(from: date),
            for: .normal
        )
    }


    func setButtonsStates(
        _ flag: Bool,
        _ buttonYes: UIButton,
        _ buttonNo: UIButton
    ) {
        buttonYes.alpha = flag ? 1 : 0.3
        buttonNo.alpha = flag ? 0.3 : 1
    }


    func setupSocialNetworkLabel() {
        if let socialNetwork = self.socialNetwork,
           let name = socialNetwork.name {
            self.socialNetworkLabel.text = String(
                format: SOCIAL_NETWORK_LABEL_STR,
                name
            )
        }
    }


    private func fillDataWithReportInformations() {
        let serverDateFormatter = DateFormatterProvider.dateFormatter
        let dateFormatter = DateFormatterProvider.simpleDateFormatter

        self.fullNameField.text = self.report.fullName
        self.countryField.text = self.report.address
        self.ageField.text = self.report.dateOfBirth
        self.emailField.text = self.report.email
        self.phoneNumberField.text = self.report.phoneNumber
        self.remarksField.text = self.report.notes
        self.facebookSelected = self.report.hasFacebookAccount
        if let date = serverDateFormatter.date(from: self.report.accountHackedDate) {
            self.dateButton.titleLabel?.text = dateFormatter.string(from: date)
        } else {
            self.dateButton.titleLabel?.text = nil
        }
        self.knowPasswordSelected = self.report.someoneKnowsPassword
        self.accountEmailAddress.text = self.report.hackedAccountEmail
        self.accountEmailPassword.text = self.report.hackedAccountEmailPassword
        self.accountLastPassword.text = self.report.hackedAccountLastPassword
        self.phoneTypeSelected = self.report.phoneType == Const.android
        self.ownerSelected = self.report.isLegalCopyrightOwner
        self.copyrightsField.text = self.report.copyrightsViolationLink
        self.publishSelected = self.report.copyrightsPublishedWithoutPermission
        self.harassesSelected = self.report.someoneUnknownHarassesYou
        self.personBlackmaildSelected = self.report.doYouKnowWhoExtortsYou
        self.over18Selected = self.report.olderThan18
        self.contentRealSelected = self.report.isContentReal
        self.accountNameOrLink.text = self.report.accountNameOrLink
        self.firstHarrassementSelected = self.report.firstHarrassement
        self.sexualHarrassementSelected = self.report.sexualHarrassement
        self.doubtPersonSelected = self.report.doubtPerson
        self.privateSelected = self.report.privateOrWork
        if let videos = self.report.videos, !videos.isEmpty {
            if let dict = self.report.videos.fromJSONToDictionary()?.compactMapValues({ $0 as? String }) {
                    self.videosURL = dict
                } else {
                    Logger.shared.error("[ReportTableViewController+PrepareObjs] fillDataWithReportInformations, unable to get videos and thumbnails")
                    
                }
        }

        self.tableView.beginUpdates()
        self.tableView.endUpdates()
        self.prepareYesNoButtons()
    }


    private func prepareYesNoButtons() {
        self.setButtonsStates(
            self.facebookSelected,
            self.facebookYes,
            self.facebookNo
        )
        self.setButtonsStates(
            self.ownerSelected,
            self.ownerYes,
            self.ownerNo
        )
        self.setButtonsStates(
            self.publishSelected,
            self.publishYes,
            self.publishNo
        )
        self.setButtonsStates(
            self.profilePictureSelected,
            self.profilePictureYes,
            self.profilePictureNo
        )
        self.setButtonsStates(
            self.personBlackmaildSelected,
            self.personBlackmaildYes,
            self.personBlackmaildNo
        )
        self.setButtonsStates(
            self.over18Selected,
            self.over18Yes,
            self.over18No
        )
        self.setButtonsStates(
            self.contentRealSelected,
            self.contentReal,
            self.contentFake
        )
        self.setButtonsStates(
            self.knowPasswordSelected,
            self.knowPasswordYes,
            self.knowPasswordNo
        )
        self.setButtonsStates(
            self.phoneTypeSelected,
            self.phoneTypeAndroid,
            self.phoneTypeiOS
        )
        self.setButtonsStates(
            self.harassesSelected,
            self.harassesYes,
            self.harassesNo
        )
        self.setButtonsStates(
            self.firstHarrassementSelected,
            self.firstHarrassementYes,
            self.firstHarrassementNo
        )
        self.setButtonsStates(
            self.sexualHarrassementSelected,
            self.sexualHarrassementYes,
            self.sexualHarrassementNo
        )
        self.setButtonsStates(
            self.doubtPersonSelected,
            self.doubtPersonYes,
            self.doubtPersonNo
        )
        self.setButtonsStates(
            self.privateSelected,
            self.privateBtn,
            self.workBtn
        )
    }


    private func disableInputs() {
        self.fullNameField.isUserInteractionEnabled = false
        self.countryField.isUserInteractionEnabled = false
        self.ageField.isUserInteractionEnabled = false
        self.emailField.isUserInteractionEnabled = false
        self.phoneNumberField.isUserInteractionEnabled = false
        self.remarksField.isUserInteractionEnabled = false
        self.dateButton.isEnabled = false
        self.accountEmailAddress.isUserInteractionEnabled = false
        self.accountEmailPassword.isUserInteractionEnabled = false
        self.accountLastPassword.isUserInteractionEnabled = false
        self.imageButton.isEnabled = false
        self.copyrightsField.isUserInteractionEnabled = false
        self.accountNameOrLink.isUserInteractionEnabled = false
        self.dateButton.isEnabled = false
        self.secondDateButton.isEnabled = false

        // Questions
        self.facebookYes.isEnabled = false
        self.facebookNo.isEnabled = false
        
        self.ownerYes.isEnabled = false
        self.ownerNo.isEnabled = false
        
        self.publishYes.isEnabled = false
        self.publishNo.isEnabled = false
        
        self.profilePictureYes.isEnabled = false
        self.profilePictureNo.isEnabled = false
        
        self.personBlackmaildYes.isEnabled = false
        self.personBlackmaildNo.isEnabled = false
        
        self.over18Yes.isEnabled = false
        self.over18No.isEnabled = false
        
        self.contentReal.isEnabled = false
        self.contentFake.isEnabled = false
        
        self.knowPasswordYes.isEnabled = false
        self.knowPasswordNo.isEnabled = false
        
        self.phoneTypeAndroid.isEnabled = false
        self.phoneTypeiOS.isEnabled = false
        
        self.harassesYes.isEnabled = false
        self.harassesNo.isEnabled = false
        
        self.firstHarrassementYes.isEnabled = false
        self.firstHarrassementNo.isEnabled = false
        
        self.sexualHarrassementYes.isEnabled = false
        self.sexualHarrassementNo.isEnabled = false
        
        self.doubtPersonYes.isEnabled = false
        self.doubtPersonNo.isEnabled = false
        
        self.privateBtn.isEnabled = false
        self.workBtn.isEnabled = false
    }


    func setupRTL() {
        self.fullNameField.textAlignment = Languages.isRightToLeft() ? .right : .left
        self.countryField.textAlignment = Languages.isRightToLeft() ? .right : .left
        self.ageField.textAlignment = Languages.isRightToLeft() ? .right : .left
        self.emailField.textAlignment = Languages.isRightToLeft() ? .right : .left
        self.phoneNumberField.textAlignment = Languages.isRightToLeft() ? .right : .left
        self.remarksField.textAlignment = Languages.isRightToLeft() ? .right : .left
        self.accountNameOrLink.textAlignment = Languages.isRightToLeft() ? .right : .left
        self.accountEmailAddress.textAlignment = Languages.isRightToLeft() ? .right : .left
        self.accountEmailPassword.textAlignment = Languages.isRightToLeft() ? .right : .left
        self.accountLastPassword.textAlignment = Languages.isRightToLeft() ? .right : .left
        self.copyrightsField.textAlignment = Languages.isRightToLeft() ? .right : .left
    }


    func setFieldsDelegates() {
        self.fullNameField.delegate = self
        self.ageField.delegate = self
        self.countryField.delegate = self
        self.emailField.delegate = self
        self.phoneNumberField.delegate = self
        self.accountEmailAddress.delegate = self
        self.accountEmailPassword.delegate = self
        self.accountLastPassword.delegate = self
        self.remarksField.delegate = self
        self.copyrightsField.delegate = self
        self.accountNameOrLink.delegate = self
    }

}
