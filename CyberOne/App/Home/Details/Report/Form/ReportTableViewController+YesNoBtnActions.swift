//
//  ReportTableViewController+YesNoBtnActions.swift
//  CyberOne
//
//  Created by Taoufik on 13/11/2022.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import Foundation



extension ReportTableViewController {

    @IBAction private func facebookAction(_ sender: UIButton) {
        self.facebookSelected = !self.facebookSelected
        self.setButtonsStates(
            self.facebookSelected,
            self.facebookYes,
            self.facebookNo
        )
    }
    
    @IBAction private func ownerAction(_ sender: UIButton) {
        self.ownerSelected = !self.ownerSelected
        self.setButtonsStates(
            self.ownerSelected,
            self.ownerYes,
            self.ownerNo
        )
    }
    
    @IBAction private func publishAction(_ sender: UIButton) {
        self.publishSelected = !self.publishSelected
        self.setButtonsStates(
            self.publishSelected,
            self.publishYes,
            self.publishNo
        )
    }
    
    @IBAction private func profilePictureAction(_ sender: UIButton) {
        self.profilePictureSelected = !self.profilePictureSelected
        self.setButtonsStates(
            self.profilePictureSelected,
            self.profilePictureYes,
            self.profilePictureNo
        )
    }
    
    @IBAction private func over18Action(_ sender: UIButton) {
        self.over18Selected = !self.over18Selected
        self.setButtonsStates(
            self.over18Selected,
            self.over18Yes,
            self.over18No
        )
    }
    
    @IBAction private func personBlackmailedAction(_ sender: UIButton) {
        self.personBlackmaildSelected = !self.personBlackmaildSelected
        self.setButtonsStates(
            self.personBlackmaildSelected,
            self.personBlackmaildYes,
            self.personBlackmaildNo
        )
    }
    
    @IBAction private func realFakeAction(_ sender: UIButton) {
        self.contentRealSelected = !self.contentRealSelected
        self.setButtonsStates(
            self.contentRealSelected,
            self.contentReal,
            self.contentFake
        )
    }
    
    
    @IBAction private func harassesAction(_ sender: UIButton) {
        self.harassesSelected = !self.harassesSelected
        self.setButtonsStates(
            self.harassesSelected,
            self.harassesYes,
            self.harassesNo
        )
    }
    
    @IBAction private func phoneTypeAction(_ sender: UIButton) {
        self.phoneTypeSelected = !self.phoneTypeSelected
        self.setButtonsStates(
            self.phoneTypeSelected,
            self.phoneTypeAndroid,
            self.phoneTypeiOS
        )
    }
    
    @IBAction private func knowsPasswordAction(_ sender: UIButton) {
        self.knowPasswordSelected = !self.knowPasswordSelected
        self.setButtonsStates(
            self.knowPasswordSelected,
            self.knowPasswordYes,
            self.knowPasswordNo
        )
    }

    @IBAction private func firstHarrassementAction(_ sender: UIButton) {
        self.firstHarrassementSelected = !self.firstHarrassementSelected
        self.setButtonsStates(
            self.firstHarrassementSelected,
            self.firstHarrassementYes,
            self.firstHarrassementNo
        )
    }
    
    @IBAction private func sexualHarrassementAction(_ sender: UIButton) {
        self.sexualHarrassementSelected = !self.sexualHarrassementSelected
        self.setButtonsStates(
            self.sexualHarrassementSelected,
            self.sexualHarrassementYes,
            self.sexualHarrassementNo
        )
    }
    
    @IBAction private func doubtPersonAction(_ sender: UIButton) {
        self.doubtPersonSelected = !self.doubtPersonSelected
        self.setButtonsStates(
            self.doubtPersonSelected,
            self.doubtPersonYes,
            self.doubtPersonNo
        )
    }
    
    @IBAction private func privateAction(_ sender: UIButton) {
        self.privateSelected = !self.privateSelected
        self.setButtonsStates(
            self.privateSelected,
            self.privateBtn,
            self.workBtn
        )
    }

}
