//
//  ReportTableViewContrller+ReportValidation.swift
//  CyberOne
//
//  Created by Taoufik on 13/11/2022.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import Foundation


extension ReportTableViewController {

    func validationAction() -> Bool {
        if let fullName = self.fullNameField.text, fullName.isBlank {
            self.showErrorField(
                errorMsg: EMPTY_FULLNAME_STR,
                view: self.fullNameField
            )
            return false
        } else {
            self.showNormalField(view: self.fullNameField)
        }

        if let country = self.countryField.text, country.isBlank {
            self.showErrorField(
                errorMsg: EMPTY_COUNTRY_STR,
                view: self.countryField
            )
            return false
        } else {
            self.showNormalField(view: self.countryField)
        }

        if let age = self.ageField.text, age.isBlank {
            self.showErrorField(
                errorMsg: EMPTY_AGE_STR,
                view: self.ageField
            )
            return false
        } else {
            self.showNormalField(view: self.ageField)
        }

        if let email = self.emailField.text, email.isBlank {
            self.showErrorField(
                errorMsg: EMPTY_EMAIL_STR,
                view: self.emailField
            )
            return false
        } else {
            self.showNormalField(view: self.emailField)
        }

        if let email = self.emailField.text, !email.trimmingCharacters(in: .whitespacesAndNewlines).isEmail {
            self.showErrorField(
                errorMsg: VALID_EMAIL_STR,
                view: self.emailField
            )
            return false
        } else {
            self.showNormalField(view: self.emailField)
        }

        if let phoneNumber = self.phoneNumberField.text, phoneNumber.isBlank {
            self.showErrorField(
                errorMsg: EMPTY_PHONE_STR,
                view: self.phoneNumberField
            )
            return false
        } else {
            self.showNormalField(view: self.phoneNumberField)
        }

        if let phoneNumber = self.phoneNumberField.text, !phoneNumber.isPhoneNumber {
            showErrorField(
                errorMsg: VALID_PHONE_STR,
                view: self.phoneNumberField
            )
            return false
        } else {
            showNormalField(view: self.phoneNumberField)
        }

        if let remarks = self.remarksField.text, remarks.isBlank {
            showErrorField(
                errorMsg: EMPTY_REMARKS_STR,
                view: self.remarksField
            )
            return false
        } else {
            showNormalField(view: remarksField)
        }

        switch self.service?.id {
        case ServiceType.impersonation.rawValue :
            // Impersonation
            return self.validationForImpersonation()
        case ServiceType.extortion.rawValue :
            // Extortion
            return self.validationForExtortion()
        case ServiceType.copyrights.rawValue :
            // copyrights
            return self.validationForCopyrights()
        case ServiceType.harassement.rawValue :
            // Harassement
            return self.validationForHarassement()
        case ServiceType.hackedAccount.rawValue :
            // Hacked account
            return self.validationForHacked()
        case ServiceType.other.rawValue :
            // Other issues
            if self.selectedAssets.count == 0 {
                showToast(EMPTY_ASSETS_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular)
                return false
            }
            return true
        default:
            if self.selectedAssets.count == 0 {
                showToast(EMPTY_ASSETS_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular)
                return false
            }
            return true
        }
    }


    private func validationForCopyrights() -> Bool {
        if let copyrightsText = self.copyrightsField.text, copyrightsText.isEmpty {
            self.showErrorField(
                errorMsg: EMPTY_COPYRIGHTS_STR,
                view: self.copyrightsField
            )
            return false
        } else {
            self.showNormalField(view: copyrightsField)
        }
        
        if self.selectedAssets.count == 0 {
            showToast(
                EMPTY_ASSETS_STR,
                backgroundColor: toastColor,
                textColor: .white,
                font: h2Regular
            )
            return false
        }
        
        return true
    }
    
    
    private func validationForExtortion() -> Bool {
        if let accountText = self.accountNameOrLink.text, accountText.isEmpty {
            self.showErrorField(
                errorMsg: EMPTY_ACCOUNT_NAME_LINK_STR,
                view: self.accountNameOrLink
            )
            return false
        } else {
            self.showNormalField(view: self.accountNameOrLink)
        }
        
        if self.selectedAssets.count == 0 {
            showToast(
                EMPTY_ASSETS_STR,
                backgroundColor: toastColor,
                textColor: .white,
                font: h2Regular
            )
            return false
        }
        return true
    }
    
    
    private func validationForHacked() -> Bool {
        if let accountEmailText = self.accountEmailAddress.text, accountEmailText.isEmpty {
            self.showErrorField(
                errorMsg: EMPTY_ACCOUNT_EMAIL_STR,
                view: self.accountEmailAddress
            )
            return false
        } else {
            self.showNormalField(view: self.accountEmailAddress)
        }
        
        if let accountEmailText = self.accountEmailAddress.text, !accountEmailText.isEmail {
            self.showErrorField(
                errorMsg: VALID_ACCOUNT_EMAIL_STR,
                view: self.accountEmailAddress
            )
            return false
        } else {
            self.showNormalField(view: accountEmailAddress)
        }
        
        if let accountPasswordText = self.accountEmailPassword.text, accountPasswordText.isEmpty {
            self.showErrorField(
                errorMsg: EMPTY_ACCOUNT_PASSWORD_STR,
                view: self.accountEmailPassword
            )
            return false
        } else {
            self.showNormalField(view: accountEmailPassword)
        }
        
        if let accountLastPasswordText = self.accountLastPassword.text, accountLastPasswordText.isEmpty {
            self.showErrorField(
                errorMsg: EMPTY_ACCOUNT_LAST_PASSWORD_STR,
                view: self.accountLastPassword
            )
            return false
        } else {
            self.showNormalField(view: self.accountLastPassword)
        }
        
        if self.selectedAssets.count == 0 {
            showToast(
                EMPTY_ASSETS_STR,
                backgroundColor: toastColor,
                textColor: .white,
                font: h2Regular
            )
            return false
        }
        
        return true
    }
    
    
    private func validationForHarassement() -> Bool {
        if self.selectedAssets.count == 0 {
            showToast(
                EMPTY_ASSETS_STR,
                backgroundColor: toastColor,
                textColor: .white,
                font: h2Regular
            )
            return false
        }
        
        return true
    }
    
    
    private func validationForImpersonation() -> Bool {
        if let accountText = self.accountNameOrLink.text, accountText.isEmpty {
            self.showErrorField(
                errorMsg: EMPTY_ACCOUNT_NAME_LINK_STR,
                view: self.accountNameOrLink
            )
            return false
        } else {
            self.showNormalField(view: accountNameOrLink)
        }
        
        if self.selectedAssets.count == 0 {
            showToast(
                EMPTY_ASSETS_STR,
                backgroundColor: toastColor,
                textColor: .white,
                font: h2Regular
            )
            return false
        }
        
        return true
    }


    private func showErrorField(errorMsg: String, view: UIView) {
        showToast(
            errorMsg,
            backgroundColor: toastColor,
            textColor: .white,
            font: h2Regular
        )
        view.borderWithCornder(
            raduis: 0,
            borderColor: UIColor.red.cgColor,
            borderWidth: 1
        )
    }


    private func showNormalField(view: UIView) {
        view.borderWithCornder(
            raduis: 0,
            borderColor: UIColor.clear.cgColor,
            borderWidth: 0
        )
    }

}
