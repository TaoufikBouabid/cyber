//
//  ReportTableViewController+MediaUploading.swift
//  CyberOne
//
//  Created by Taoufik on 13/11/2022.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import Foundation
import TLPhotoPicker
import ProgressHUD
import JustLog



extension ReportTableViewController {

    func uploadMedia() {
        self.uploadPhotos()
        self.uploadVideos()
        if self.audioPath != nil {
            self.uploadAudio()
        }
    }


    private func uploadPhotos() {
        let datasource = self.selectedAssets.filter{ $0.type == TLPHAsset.AssetType.photo }
        var index = 1
        for photo in datasource {
            if let data = UIImageJPEGRepresentation(photo.fullResolutionImage!, 0.2) {
                UploadManager.upload(data: data, name: "test", ext: "jpg", completion: { link, error in
                    if error == nil {
                        self.saveReportPicture(link: link!, isLast: index == datasource.count)
                        index += 1
                    } else if let error = error {
                        Logger.shared.error("[ReportTableViewController+MediaUploading] uploadPhotos - unable to upload image, error: \(error.localizedDescription)")
                        ProgressHUD.showError("Picture upload error.")
                    }
                })
            }
        }
    }


    private func uploadVideos() {
        let datasource = self.selectedAssets.filter { $0.type == TLPHAsset.AssetType.video }.reduce(into: [:]) { $0[$1.fullResolutionImage] = $1.originalFileName }

        self.isContainsVideos = datasource.count > 0
        var index = 1
        for (thumbnail, originalFileName) in datasource {
            if let url = URL(string: FileManager.default.temporaryDirectory.appendingPathComponent("\(originalFileName)").absoluteString),
            let thumbnail = thumbnail {
                guard let originalFileData = try? Data(contentsOf: url) else {
                    Logger.shared.error("[ReportTableViewController+MediaUploading] uploadVideos - unable to convert video to Data")
                    return
                }

                guard let thumbnailData = UIImagePNGRepresentation(thumbnail) else {
                    Logger.shared.error("[ReportTableViewController+MediaUploading] uploadVideos - unable to convert thumbnail video to Data")
                    return
                }

                UploadManager.upload(data: originalFileData, name: "test", ext: "mp4", completion: { videoLink, videoError in
                    if videoError == nil, let videoLink = videoLink {
                        UploadManager.upload(data: thumbnailData, name: "test", ext: "jpg", completion: { thumbnailLink, thumbnailError in
                            if thumbnailError == nil, let thumbnailLink = thumbnailLink {
                                self.saveReportVideos(
                                    thumbnailLink: thumbnailLink,
                                    videoLink: videoLink,
                                    isLast: index == datasource.count
                                )
                                index += 1
                            } else if let error = thumbnailError {
                                Logger.shared.error("[ReportTableViewController+MediaUploading] uploadPhotos - unable to upload thumbnail, error: \(error.localizedDescription)")
                                ProgressHUD.showError("Video upload error.")
                            }
                        })
                    } else if let error = videoError {
                        Logger.shared.error("[ReportTableViewController+MediaUploading] uploadPhotos - unable to upload video, error: \(error.localizedDescription)")
                        ProgressHUD.showError("Video upload error.")
                    }
                })
            }
        }
    }


    private func uploadAudio() {
        if let url = audioPath {
            let data = try! Data.init(contentsOf: url)
            UploadManager.upload(data: data, name: "test", ext: "mp3", completion: { link, error in
                if error == nil {
                    self.saveReportAudio(link: link!)
                } else if let error = error {
                    Logger.shared.error("[ReportTableViewController+MediaUploading] uploadAudio - unable to upload audio, error: \(error.localizedDescription)")
                    ProgressHUD.showError("Audio upload error.")
                }
            })
        }
    }


    private func saveReportPicture(link: String, isLast: Bool) {
        self.photosURL.append(link)
        if isLast {
            self.report[FReport.Keys.attachments.rawValue] = photosURL.joined(separator: ",")
            self.report.updateInBackground(block: { (error) in
                if error == nil {
                    if !self.isContainsVideos && isLast {
                        ProgressHUD.showSuccess()
                        if self.subscription == nil {
                            self.delegate!.didDismiss!(self.report)
                        } else {
                            self.delegate!.didDismiss?(self.report, self.service, self.socialNetwork)
                        }
                        self.navigationController?.popViewController(animated: true)
                    }
                } else if let error = error {
                    Logger.shared.error("[ReportTableViewController+MediaUploading] saveReportPicture - unable to save images, error: \(error.localizedDescription)")
                    ProgressHUD.showError("Images upload error.")
                }
            })
        }
    }


    private func saveReportVideos(
        thumbnailLink: String,
        videoLink: String,
        isLast: Bool
    ) {
        self.videosURL[thumbnailLink] = videoLink
        if isLast {
            self.report[FReport.Keys.videos.rawValue] = self.videosURL.toJson
            self.report.updateInBackground(block: { error in
                if error == nil {
                    ProgressHUD.showSuccess()
                    self.navigationController?.popViewController(animated: true)
                } else if let error = error {
                    Logger.shared.error("[ReportTableViewController+MediaUploading] saveReportVideos - unable to save videos, error: \(error.localizedDescription)")
                    ProgressHUD.showError("Videos upload error.")
                }
            })
        }
    }


    private func saveReportAudio(link: String) {
        self.report[FReport.Keys.statusVoiceRecording.rawValue] = link
        self.report.updateInBackground(block: { (error) in
            if error == nil {
                UserDefaults.standard.removeObject(forKey: "audioRecorded")
            } else if let error = error {
                Logger.shared.error("[ReportTableViewController+MediaUploading] saveReportAudio - unable to save audio, error: \(error.localizedDescription)")
                ProgressHUD.showError("Autdio upload error.")
            }
        })
    }

}
