//
//  AssetImageCollectionViewCell.swift
//  CyberOne
//
//  Created by Taoufik on 09/01/2019.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import UIKit
import TLPhotoPicker



final class AssetCollectionViewCell: UICollectionViewCell {
    static let identifier = "AssetCollectionViewCell"

    @IBOutlet weak var assetImage: UIImageView!
    @IBOutlet weak var removeButton: UIButton!
    var asset: TLPHAsset!
    var controller: ReportTableViewController!
    var videoPath: String?


    @IBAction private func removeAsset(_ sender: UIButton) {
        self.controller.selectedAssets.removeAll(
            where: { $0.originalFileName == self.asset.originalFileName }
        )
        self.controller.tableView.beginUpdates()
        self.controller.videosCollectionView.reloadData()
        self.controller.imagesCollectionView.reloadData()
        self.controller.tableView.endUpdates()
    }

}
