//
//  VideoPlayerViewController.swift
//  CyberOne
//
//  Created by Taoufik on 12/11/2022.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import UIKit



final class VideoPlayerViewController: UIViewController {

    @IBOutlet weak private var mediaView: UIView!
    var videoPath: String? = nil


    override func viewDidLoad() {
        super.viewDidLoad()

        let screenSize = UIScreen.main.bounds.size
        let popupWidth = screenSize.width - 20
        let popupHeight = screenSize.height - 120
        let popupSize = CGSize(width: popupWidth, height: popupHeight)
        self.contentSizeInPopup = popupSize
        self.popupController?.backgroundView?.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(self.dismissPopup(_:))
            )
        )

    }


    @objc private func dismissPopup(_ sender: UITapGestureRecognizer) {
        self.popupController?.dismiss()
    }
}
