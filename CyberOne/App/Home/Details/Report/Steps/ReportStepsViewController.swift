//
//  ReportStepsViewController.swift
//  CyberOne
//
//  Created by Taoufik on 10/01/2019.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import UIKit
import FirebaseDatabase
import STPopup
import JustLog



final class ReportStepsViewController: UIViewController {
    static let storyboardId = "ReportStepsViewController"

    @IBOutlet weak private var statusImage: UIImageView!
    @IBOutlet weak private var statusLabel: UILabel!

    var steps: CMSteppedProgressBar!
    var report: FReport!
    var service: FService!
    var socialNetwork: FSocialNetworks!


    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.init(hex: 0xF2F2F2)
        if FUser.currentUserType() == FUserType.admin.rawValue {
            navigationItem.rightBarButtonItems = [
                UIBarButtonItem(image: UIImage(named:"approve")?.withRenderingMode(.alwaysTemplate), style: .done, target: self, action: #selector(self.approveStep)),
                UIBarButtonItem(image: UIImage(named:"remove")?.withRenderingMode(.alwaysTemplate), style: .done, target: self, action: #selector(self.resetStep))
            ]
        }

        self.addSteps()
        self.addListener()
        self.updateView()
    }



    @objc private func approveStep() {
        if self.report.status < 4 {
            var status = self.report.status
            status += 1
            self.report[FReport.Keys.status.rawValue] = status
            self.report.updateInBackground()
            self.report.status = status
        }
    }


    @objc private func resetStep() {
        if self.report.status > 0 {
            var status = self.report.status
            status -= 1
            self.report[FReport.Keys.status.rawValue] = status
            self.report.updateInBackground()
            self.report.status = status
        }
    }


    private func addSteps() {
        self.steps = CMSteppedProgressBar(
            frame: CGRect(
                x: 0,
                y: view.frame.origin.y + 100,
                width: view.frame.size.width,
                height: 30
            )
        )
        self.steps.barColor = .lightGray
        self.steps.tintColor = UIColor(hex: 0x0040A3)
        self.steps.linesHeight = 4
        self.steps.dotsWidth = 20
        self.steps.numberOfSteps = 5
        self.steps.delegate = self
        self.view.addSubview(self.steps)
    }


    private func addListener() {
        let reference = report.databaseReference()
        reference.observe(.childChanged) { snapshot in
            self.report.values[snapshot.key] = snapshot.value
            if let step = self.report[FReport.Keys.status.rawValue] as? Int {
                self.steps.currentStep = UInt(step)
                self.updateView()
            }
        }
    }


    private func updateView() {
        guard let status = self.report[FReport.Keys.status.rawValue] as? Int else {
            Logger.shared.error("[ReportStepsViewController] updateView, unable to get status")
            return
        }
        self.steps.currentStep = UInt(status)
        switch status {
        case 0:
            self.statusLabel.text = STEP0_STR
            self.statusImage.image = #imageLiteral(resourceName: "Step_0")
            break
        case 1:
            self.statusLabel.text = STEP1_STR
            self.statusImage.image = #imageLiteral(resourceName: "Step_1")
            break
        case 2:
            self.statusLabel.text = STEP2_STR
            self.statusImage.image = #imageLiteral(resourceName: "step_2")
            break
        case 3:
            self.statusLabel.text = STEP3_STR
            self.statusImage.image = #imageLiteral(resourceName: "step_3")
            break
        case 4:
            self.statusLabel.text = STEP4_STR
            self.statusImage.image = #imageLiteral(resourceName: "step_4")
            break
        default:
            return
        }
    }
}



extension ReportStepsViewController: CMSteppedProgressBarDelegate {

    func steppedBar(_ steppedBar: CMSteppedProgressBar!, didSelect index: UInt) {
        Logger.shared.info("[ReportStepsViewController] the step is updated, current step: \(index)")
    }
}
