//
//  HomeDetailsViewController.swift
//  CyberOne
//
//  Created by Taoufik on 07/01/2019.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import UIKit
import ProgressHUD
import STPopup
import FirebaseDatabase



final class HomeDetailsViewController: UIViewController {
    static let storyboardId = "HomeDetailsViewController"
    
    @IBOutlet weak private var icon: UIImageView!
    @IBOutlet weak private var backgroundImage: UIImageView!
    @IBOutlet weak private var topView: UIView!
    @IBOutlet weak private var collectionView: UICollectionView!

    private var data = [FSocialNetworks]()
    var service: FService!
    private var reports = [FReport]()


    override func viewDidLoad() {
        super.viewDidLoad()

        self.topView.shadowWithCornder(
            raduis: 25,
            shadowColor: UIColor.lightGray.cgColor,
            shadowOffset: CGSize(width: 2, height: 2),
            shadowOpacity: 0.8,
            shadowRadius: 5
        )
        self.icon.sd_setImage(with: URL(string: service.icon))
        self.backgroundImage.sd_setImage(with: URL(string: service.background))
        self.title = service.title
        self.loadData()
    }


    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.topView.roundedCorner(
            corner: [.bottomLeft, .bottomRight],
            raduis: 25
        )
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadMyReports()
    }


    private func loadMyReports() {
        let reports = FObject(path: "Reports")
        reports.fetchInBackground { error in
            if error == nil {
                self.reports.removeAll()
                for report in reports.values {
                    if let value = report.value as? [String: Any] {
                        self.reports.append(FReport(value: value))
                    }
                }
                self.reports = self.reports.filter {
                    $0.serviceId == self.service.objectId &&
                    $0.userId == FUser.currentUserId() ?? ""
                }
                self.collectionView.reloadData()
            }
        }
    }


    private func loadData() {
        let socialNetworks = FObject(path: "SocialNetworks")
        ProgressHUD.show(nil, interaction: false)
        socialNetworks.fetchInBackground { (error) in
            if error == nil {
                self.data.removeAll()
                ProgressHUD.dismiss()
                for social in socialNetworks.values {
                    if let value = social.value as? [String: Any] {
                        self.data.append(FSocialNetworks(dict: value))
                    }
                }
                self.data.sort(by: { $0.order < $1.order })
                self.collectionView.reloadData()
            } else if let error = error {
                ProgressHUD.showError(error.localizedDescription)
            }
        }
    }
}



extension HomeDetailsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.data.count
    }


    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let value = self.data[indexPath.item]
        if value.logo == nil && value.selectedLogo == nil {
            guard let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: OtherPlatformCollectionViewCell.identifier,
                for: indexPath
            ) as? OtherPlatformCollectionViewCell else {
                return UICollectionViewCell()
            }
            cell.label.text = self.data[indexPath.item].name
            return cell
        } else {
            guard let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: HomeDetailsCollectionViewCell.identifier,
                for: indexPath
            ) as? HomeDetailsCollectionViewCell else {
                return UICollectionViewCell()
            }
            cell.image.sd_setImage(with: URL(string: self.reports.contains(where: { $0.socialNetwork == value.name }) ? value.selectedLogo : value.logo))
            return cell
        }
    }


    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  25
        let collectionViewSize = collectionView.frame.size.width - padding
        var bottomSafeArea:CGFloat = 0
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            bottomSafeArea = window?.safeAreaInsets.bottom ?? 0.0
        }
        let height = UIScreen.main.bounds.height - 220 - 15 - 75 - bottomSafeArea
        if self.data[indexPath.item].logo == nil && self.data[indexPath.item].selectedLogo == nil {
            return CGSize(width: collectionView.frame.size.width, height: height*0.2)
        } else {
            return CGSize(width: collectionViewSize/2, height: height*4/15)
        }
    }


    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 25
    }


    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 25
    }


    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.showReportForm(socialNetwork: data[indexPath.item])
    }


    func showReportForm(socialNetwork: FSocialNetworks) {
        let stb = UIStoryboard.init(name: "Report", bundle: nil)
        guard let reportVc =  stb.instantiateViewController(withIdentifier: ReportTableViewController.storyboardId) as? ReportTableViewController else {
            return
        }
        reportVc.delegate = self
        reportVc.socialNetwork = socialNetwork
        reportVc.service = self.service
        self.navigationController?.pushViewController(reportVc, animated: true)
    }


    func addChat(_ report: FReport) {
        guard let currentUserId = FUser.currentUserId(), let reportId = report.objectId() else {
            return
        }
        let objectId = currentUserId + reportId
        let dateFormatter = DateFormatterProvider.dateFormatter
        let values: [String:Any] = [
            FChat.keys.lastMessage.rawValue : "",
            FChat.keys.lastMessageDateSent.rawValue: dateFormatter.string(from: Date()),
            FChat.keys.lastMessageUserId.rawValue: "",
            FChat.keys.objectId.rawValue: objectId,
            FChat.keys.opponentKey.rawValue: reportId,
            FChat.keys.typingIndicator.rawValue: [currentUserId: false, reportId: false],
            FChat.keys.unreadMessages.rawValue: [currentUserId: 0, reportId: 0],
            FChat.keys.isClosed.rawValue: false,
            "id": self.service.id
        ]
        let object = FObject(
            path: FChat.path,
            subpath: objectId,
            dictionary: values
        )
        object.saveInBackground()
    }
}



extension HomeDetailsViewController: ReportTableViewControllerDelegate {

    func didDismiss(_ report: FReport) {
        self.addChat(report)
        guard let stepsVc = StoryboardProvider.instance.storyboard(for: .main).instantiateViewController(
            withIdentifier: ReportStepsViewController.storyboardId
        ) as? ReportStepsViewController else {
            return
        }
        stepsVc.service = self.service
        stepsVc.report = report
        if let socialNetwork = self.data.filter({ (socialNetwork) -> Bool in
            socialNetwork.name == report.socialNetwork
        }).first {
            stepsVc.socialNetwork = socialNetwork
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            self.navigationController?.pushViewController(stepsVc, animated: true)
        })
    }

}
