//
//  HomeViewController.swift
//  CyberOne
//
//  Created by Taoufik on 14/11/2018.
//  Copyright © 2018 Cyber One. All rights reserved.
//

import UIKit
import ProgressHUD
import JustLog



final class HomeViewController: UIViewController {

    @IBOutlet weak private var collectionView: UICollectionView!
    @IBOutlet weak private var topViewHeight: NSLayoutConstraint!
    
    var data = [FService]()
    var notifications = [FNotifications]()
    var selectedService: FService?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.loadData()
        self.addMiddleImage()
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getNotifications()
    }


    func addMiddleImage() {
        if let logo = UIImage(named: "logo-\(Languages.currentLanguage())") {
            let navBarImage = logo.withRenderingMode(.alwaysOriginal)
            let navBarImageView = UIImageView(image: navBarImage)
            navBarImageView.contentMode = .scaleAspectFit
            self.navigationItem.titleView = navBarImageView
        }
    }


    @objc func displayAlerts() {
        if self.notifications.count == 0 {
            self.showAlertView(
                isOneButton: true,
                title: ALERTS_STR,
                cancelButtonTitle: OK_STR,
                submitButtonTitle: OK_STR
            )
        } else {
            guard let notificationVc = StoryboardProvider.instance.storyboard(for: .main).instantiateViewController(
                withIdentifier: NotificationsViewController.storyboardId
            ) as? NotificationsViewController else {
                return
            }
            notificationVc.data = self.notifications
            self.navigationController?.pushViewController(notificationVc, animated: true)
        }
    }


    private func loadData() {
        let services = FObject(path: "Services")
        ProgressHUD.show(nil, interaction: false)
        services.fetchInBackground { (error) in
            ProgressHUD.dismiss()
            if error == nil {
                UIView.animate(withDuration: 0.3, animations: {
                    self.topViewHeight.constant = 200
                    self.view.layoutIfNeeded()
                })
                for service in services.values {
                    if let value = service.value as? [String: Any] {
                        self.data.append(FService(dict: value))
                    }
                }
                self.data = self.data.sorted(by: { $0.order < $1.order })
                self.collectionView.reloadData()
            } else if let error = error {
                Logger.shared.error("[HomeViewController] getNotifications, error: \(error.localizedDescription)")
            }
        }
    }


    private func getNotifications() {
        let notifications = FObject(path: "Notifications")
        notifications.fetchInBackground { (error) in
            if error == nil {
                self.notifications.removeAll()
                for notification in notifications.values {
                    self.notifications.append(FNotifications(dict: notification.value as! [String: Any]))
                }
                self.notifications = self.notifications.filter({ (notif) -> Bool in
                    notif.userId == FUser.currentUserId()
                })
                let number = self.notifications.filter({ (notif) -> Bool in
                    notif.status == 0
                }).count
                self.initNavItems("\(number)")
            } else if let error = error {
                Logger.shared.error("[HomeViewController] getNotifications, error: \(error.localizedDescription)")
            }
        }
    }


    private func initNavItems(_ number: String) {
        let image = UIImage(named: "alerts")
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: image?.size.width ?? 0.0, height: image?.size.height ?? 0.0)
        button.addTarget(self, action: #selector(self.displayAlerts), for: .touchDown)
        button.setBackgroundImage(image, for: .normal)

        // Make BarButton Item
        let navLeftButton = UIBarButtonItem(customView: button)
        navigationItem.leftBarButtonItem = navLeftButton
        navigationItem.leftBarButtonItem?.badgeValue = number
        navigationItem.leftBarButtonItem?.badgeBGColor = .red
    }
}



extension HomeViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeCollectionViewCell.identifier, for: indexPath) as! HomeCollectionViewCell
        let service = data[indexPath.item]
        cell.background.sd_setImage(with: URL(string: service.background))
        cell.icon.sd_setImage(with: URL(string: service.icon))
        cell.titleLabel.text = service.title
        cell.descLabel.text = service.desc
        return cell
    }


    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: self.view.frame.width-40, height: (self.view.frame.width-40)*0.5)
    }


    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedService = self.data[indexPath.item]
        if indexPath.item == self.data.count - 1 {
            guard let reportVc =  StoryboardProvider.instance.storyboard(for: .report).instantiateViewController(withIdentifier: ReportTableViewController.storyboardId) as? ReportTableViewController else {
                return
            }
            reportVc.delegate = self
            reportVc.service = self.selectedService
            self.navigationController?.pushViewController(reportVc, animated: true)
        } else {
            guard let detailsVc = StoryboardProvider.instance.storyboard(for: .main).instantiateViewController(
                withIdentifier: HomeDetailsViewController.storyboardId
            ) as? HomeDetailsViewController else {
                return
            }
            detailsVc.service = self.selectedService
            self.navigationController?.pushViewController(detailsVc, animated: true)
        }
    }
}



extension HomeViewController: ReportTableViewControllerDelegate {

    func didDismiss(_ report: FReport) {
        guard let selectedService = self.selectedService else { return }
        self.addChat(report)
        guard let stepsVc = StoryboardProvider.instance.storyboard(for: .main).instantiateViewController(withIdentifier: ReportStepsViewController.storyboardId) as? ReportStepsViewController else {
            return
        }
        stepsVc.service = selectedService
        stepsVc.report = report
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            self.navigationController?.pushViewController(stepsVc, animated: true)
        })
    }


    func addChat(_ report: FReport) {
        guard let currentUserId = FUser.currentUserId(), let reportId = report.objectId() else {
            return
        }
        let objectId = currentUserId + reportId
        let dateFormatter = DateFormatterProvider.dateFormatter
        let values: [String: Any] = [
            FChat.keys.lastMessage.rawValue: "",
            FChat.keys.lastMessageDateSent.rawValue: dateFormatter.string(from: Date()),
            FChat.keys.lastMessageUserId.rawValue: "",
            FChat.keys.objectId.rawValue: objectId,
            FChat.keys.opponentKey.rawValue: reportId,
            FChat.keys.typingIndicator.rawValue: [currentUserId: false, reportId: false],
            FChat.keys.unreadMessages.rawValue: [currentUserId: 0, reportId: 0],
            FChat.keys.isClosed.rawValue: false
        ]
        FObject(
            path: FChat.path,
            subpath: objectId,
            dictionary: values
        ).saveInBackground()
    }

}
