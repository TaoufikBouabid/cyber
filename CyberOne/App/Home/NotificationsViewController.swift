//
//  NotificationsViewController.swift
//  CyberOne
//
//  Created by Taoufik on 12/7/19.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import UIKit
import STPopup
import FirebaseDatabase



final class NotificationsViewController: UIViewController {
    static let storyboardId = "NotificationsViewController"

    @IBOutlet weak private var tableView: UITableView!

    var data = [FNotifications]()
    var reports = [FReport]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.title = ALERTS_TITLE_STR
        self.loadReports()
    }


    private func loadReports() {
        let reports = FObject(path: "Reports")
        reports.databaseReference()
            .observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
            if snapshot.exists() {
                reports.values = snapshot.value as! [String: Any]
                for report in reports.values {
                    self.reports.append(FReport.init(value: report.value as! [String: Any]))
                }
            }
        }) { error in
            print("Error while retrieving reports")
        }
    }

}


extension NotificationsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NotificationTableViewCell.identifier, for: indexPath) as! NotificationTableViewCell
        
        cell.titleLabel.text = self.data[indexPath.row].title
        cell.bodyLabel.text = self.data[indexPath.row].body
        cell.gradientView.isHidden = self.data[indexPath.row].status == 1
        if let timeResult = self.data[indexPath.row].date {
            let date = Date(timeIntervalSince1970: TimeInterval(timeResult/1000))
            let dateFormatter = DateFormatter()
            dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
            dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
            dateFormatter.timeZone = .current
            let localDate = dateFormatter.string(from: date)
            cell.hourLabel.text = localDate
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let notif = self.data[indexPath.row]
        
        if notif.type == 0 {
            if let report = self.reports.filter({ (report) -> Bool in
                report.objectId == notif.reportId
            }).first {
                let stb = UIStoryboard.init(name: "Report", bundle: nil)
                let reportVc =  stb.instantiateViewController(withIdentifier: ReportTableViewController.storyboardId) as! ReportTableViewController
                reportVc.service = FService(dict: ["id" : report.serviceType])
                reportVc.socialNetwork = FSocialNetworks(dict: ["logo" : report.socialNetworkLogo])
                reportVc.report = report
                reportVc.delegate = self
                self.navigationController?.pushViewController(reportVc, animated: true)
            }
        }

        let object = FObject(path: "Notifications", subpath: notif.objectId)
        object["notification_status"] = 1
        object.updateInBackground { (error) in
            if error == nil {
                if let cell = tableView.cellForRow(at: indexPath) as? NotificationTableViewCell {
                    cell.gradientView.isHidden = true
                }
            }
        }

    }
}


extension NotificationsViewController: ReportTableViewControllerDelegate {
    func didDismiss(_ report: FReport?, _ service: FService?, _ socialNetwork: FSocialNetworks?) {
        if let report = report, let service = service, let socialNetwork = socialNetwork {
            guard let stepsVc = StoryboardProvider.instance.storyboard(for: .main).instantiateViewController(withIdentifier: ReportStepsViewController.storyboardId) as? ReportStepsViewController else {
                return
            }
            stepsVc.service = service
            stepsVc.report = report
            stepsVc.socialNetwork = socialNetwork
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                self.navigationController?.pushViewController(stepsVc, animated: true)
            })
        }
    }
}
