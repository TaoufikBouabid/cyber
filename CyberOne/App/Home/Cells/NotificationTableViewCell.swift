//
//  NotificationTableViewCell.swift
//  CyberOne
//
//  Created by Taoufik on 12/7/19.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    static let identifier = "NotificationTableViewCell"
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var gradientView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
