//
//  HomeCollectionViewCell.swift
//  CyberOne
//
//  Created by Taoufik on 14/11/2018.
//  Copyright © 2018 Cyber One. All rights reserved.
//

import UIKit



final class HomeCollectionViewCell: UICollectionViewCell {
    static let identifier = "HomeCollectionViewCell"
    
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var background: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.layer.cornerRadius = 10
        self.contentView.layer.masksToBounds = true
        self.icon.shadowWithCornder(raduis: 0, shadowColor: UIColor.black.cgColor, shadowOffset: CGSize(width: 2, height: 2), shadowOpacity: 0.8, shadowRadius: 5)
    }

}
