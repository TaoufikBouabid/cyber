//
//  DateFormatterProvider.swift
//  CyberOne
//
//  Created by Taoufik on 05/11/2022.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import Foundation



struct DateFormatterProvider {

    static var gmtDateFormatter: DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.locale = Locale(identifier: "en_US")
        return dateFormatter
    }


    static var simpleDateFormatter: DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale(identifier: "en_US")
        return dateFormatter
    }


    static var dateFormatter: DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale(identifier: "en_US")
        return dateFormatter
    }


    static var dateFormatterFromGMT: DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone =  TimeZone(secondsFromGMT: 0)
        dateFormatter.locale = Locale(identifier: "en_US")
        return dateFormatter
    }


    func dateFormat(date: Date) -> String{
        let dateFormatter = DateFormatterProvider.dateFormatter
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.string(from: date)
    }


    func dateFormat(date: String) -> Date? {
        let dateFormatter = DateFormatterProvider.dateFormatter
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.date(from: date)
    }


    func displayTimeInMessages(dateSent: String) -> String? {
        let dateFormatter = DateFormatterProvider.dateFormatterFromGMT
        guard let date = dateFormatter.date(from: dateSent) else {
            return nil
        }

        let dateFormatterHour = DateFormatter()
        dateFormatterHour.dateFormat = "HH:mm"
        return dateFormatterHour.string(from: date)
    }


    static var dayMonthYearDateFormatter: DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale(identifier: "en_US")
        return dateFormatter
    }
}
