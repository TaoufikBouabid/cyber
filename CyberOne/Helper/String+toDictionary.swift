//
//  String+toDictionary.swift
//  CyberOne
//
//  Created by Taoufik on 13/11/2022.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import Foundation
import JustLog



extension String {


    /** Convert a json to dictionary */
    func fromJSONToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                Logger.shared.error("[String] toDictionary: cannot deserialize object, error=\(error)")
            }
        }
        return nil
    }

}
