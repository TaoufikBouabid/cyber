//
//  Dictionary+toJson.swift
//  CyberOne
//
//  Created by Taoufik on 13/11/2022.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import Foundation
import JustLog



extension Dictionary {

    /// Converts a dictionary to the json format
    var toJson: String? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return String(bytes: jsonData, encoding: .utf8)
        } catch let error {
            Logger.shared.error("[Dictionary+toJSON], error: \(error.localizedDescription)")
            return nil
        }
    }

}
