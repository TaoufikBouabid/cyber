//
//  String+HTML.swift
//  CyberOne
//
//  Created by Taoufik on 12/11/2022.
//  Copyright © 2022 Cyber One. All rights reserved.
//

import Foundation


extension String{

    func convertHtml() -> NSAttributedString{
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(
                data: data,
                options: [.documentType: NSAttributedString.DocumentType.html],
                documentAttributes: nil)
            
        } catch {
            return NSAttributedString()
        }
    }
}
