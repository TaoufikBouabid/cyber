//
//  CalendarViewController.swift
//  Shoot
//
//  Created by macbook on 04/09/2018.
//  Copyright © 2018 Karizma. All rights reserved.
//

import UIKit
import FSCalendar



@objc protocol CalendarViewControllerDelegate {
    @objc func didSelectDate(_ date: Date)
}



final class CalendarViewController: UIViewController {
    static let storyboardId = "CalendarViewController"
    
    @IBOutlet weak var calendarView: FSCalendar!
    @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!

    
    private lazy var headerDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE d MMMM"
        formatter.locale = Locale.init(identifier: "en_US")
        return formatter
    }()
    
    var delegate: CalendarViewControllerDelegate!
    var selectedDate: Date!
    var minimumDate: Date?


    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.contentSizeInPopup = CGSize(width: UIScreen.main.bounds.width-30, height: 470)
        self.popupController?.navigationBarHidden = true
        self.popupController?.containerView.layer.cornerRadius = 10
        self.popupController?.containerView.clipsToBounds = true
        self.popupController?.hidesCloseButton = true
        self.popupController?.navigationBarHidden = true
        popupController?.backgroundView?.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(self.dismissPopup)))

        self.calendarView.locale = Locale.init(identifier: "en_US")
        self.calendarView.firstWeekday = 2
        self.calendarView.appearance.weekdayTextColor = .white
        self.calendarView.appearance.titleDefaultColor = .white
        self.calendarView.headerHeight = 0
        self.calendarView.scrollEnabled = true
        self.calendarView.scrollDirection = .horizontal
        self.calendarView.appearance.selectionColor = .white
        self.calendarView.appearance.titleSelectionColor = .darkGray
        self.calendarView.appearance.todayColor = UIColor(hex: 0xFFFF01)
        self.calendarView.appearance.titleTodayColor = .darkGray
        self.calendarView.appearance.borderRadius = 10
        self.calendarView.delegate = self
        self.calendarView.scope = .month
        self.calendarView.sendSubview(toBack: self.view)
        if self.selectedDate != nil {
            self.calendarView.select(selectedDate)
            self.calendarView.reloadData()
            self.calendarView.adjustMonthPosition()
            self.yearLabel.text = "\(Calendar.current.component(.year, from: selectedDate))"
            self.dateLabel.text = headerDateFormatter.string(from: selectedDate).capitalizingFirstLetter()
        } else {
            self.yearLabel.text = "\(Calendar.current.component(.year, from: Date()))"
            self.dateLabel.text = headerDateFormatter.string(from: Date()).capitalizingFirstLetter()
        }
        self.submitBtn.borderWithCornder(raduis: 15, borderColor: UIColor.white.withAlphaComponent(0.3).cgColor, borderWidth: 1)
        self.cancelBtn.borderWithCornder(raduis: 15, borderColor: UIColor.white.withAlphaComponent(0.3).cgColor, borderWidth: 1)
    }


    @objc private func dismissPopup(){
        self.popupController?.dismiss()
    }


    @IBAction private func cancelAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }


    @IBAction private func submitAction(_ sender: UIButton) {
        self.delegate.didSelectDate(selectedDate)
        self.dismiss(animated: true, completion: nil)
    }
}



extension CalendarViewController: FSCalendarDataSource, FSCalendarDelegate{
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        print("did select date \(DateFormatterProvider.dateFormatter.string(from: date))")
        let selectedDates = calendar.selectedDates.map({DateFormatterProvider.dateFormatter.string(from: $0)})
        print("selected dates is \(selectedDates)")
        if monthPosition == .next || monthPosition == .previous {
            calendar.setCurrentPage(date, animated: true)
        }
        
        self.selectedDate = date
        self.yearLabel.text = "\(Calendar.current.component(.year, from: date))"
        self.dateLabel.text = headerDateFormatter.string(from: date).capitalizingFirstLetter()
    }


    private func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        print("\(DateFormatterProvider.dateFormatter.string(from: calendar.currentPage))")
    }


    private func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.calendarHeightConstraint.constant = bounds.height
        self.view.layoutIfNeeded()
    }

}
